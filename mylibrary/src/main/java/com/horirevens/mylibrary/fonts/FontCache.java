package com.horirevens.mylibrary.fonts;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.reflect.Type;
import java.util.Hashtable;

public class FontCache {

    private static Hashtable<String, Typeface> fontCache = new Hashtable<String, Typeface>();

    public static Typeface get(String name, Context context) {
        Typeface typeface = fontCache.get(name);
        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), name);
            } catch (Exception e) {
                return null;
            }

            fontCache.put(name, typeface);
        }

        return typeface;
    }
}
