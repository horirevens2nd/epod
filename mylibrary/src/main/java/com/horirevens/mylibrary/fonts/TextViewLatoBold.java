package com.horirevens.mylibrary.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class TextViewLatoBold extends AppCompatTextView {
    public TextViewLatoBold(Context context) {
        super(context);
        //init();
    }

    public TextViewLatoBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFontTextView(this, context, attrs);
        //init();
    }

    public TextViewLatoBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFontTextView(this, context, attrs);
        //init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato_bold.ttf");
            setTypeface(typeface);
        }
    }
}
