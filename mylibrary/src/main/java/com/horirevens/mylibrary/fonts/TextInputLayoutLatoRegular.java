package com.horirevens.mylibrary.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

public class TextInputLayoutLatoRegular extends TextInputLayout {
    public TextInputLayoutLatoRegular(Context context) {
        super(context);
        //init();
    }

    public TextInputLayoutLatoRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFontTextInputLayout(this, context, attrs);
        //init();
    }

    public TextInputLayoutLatoRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFontTextInputLayout(this, context, attrs);
        //init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato_regular.ttf");
            setTypeface(typeface);
        }
    }
}
