package com.horirevens.mylibrary.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class TextViewLatoItalic extends AppCompatTextView {
    public TextViewLatoItalic(Context context) {
        super(context);
        //init();
    }

    public TextViewLatoItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFontTextView(this, context, attrs);
        //init();
    }

    public TextViewLatoItalic(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFontTextView(this, context, attrs);
        //init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato_italic.ttf");
            setTypeface(typeface);
        }
    }
}
