package com.horirevens.mylibrary.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

public class TextInputEditTextLatoRegular extends TextInputEditText {
    public TextInputEditTextLatoRegular(Context context) {
        super(context);
        //init();
    }

    public TextInputEditTextLatoRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFontTextInputEditText(this, context, attrs);
        //init();
    }

    public TextInputEditTextLatoRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFontTextInputEditText(this, context, attrs);
        //init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato_regular.ttf");
            setTypeface(typeface);
        }
    }
}
