package com.horirevens.mylibrary.fonts;


import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;

public class SpinnerLatoRegular extends AppCompatSpinner {
    public SpinnerLatoRegular(Context context) {
        super(context);
    }

    public SpinnerLatoRegular(Context context, int mode) {
        super(context, mode);
    }

    public SpinnerLatoRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerLatoRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SpinnerLatoRegular(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public SpinnerLatoRegular(Context context, AttributeSet attrs, int defStyleAttr, int mode, Resources.Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode, popupTheme);
    }
}
