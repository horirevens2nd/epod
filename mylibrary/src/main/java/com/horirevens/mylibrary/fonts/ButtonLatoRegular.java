package com.horirevens.mylibrary.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

public class ButtonLatoRegular extends AppCompatButton {
    public ButtonLatoRegular(Context context) {
        super(context);
        //init();
    }

    public ButtonLatoRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFontButton(this, context, attrs);
        //init();
    }

    public ButtonLatoRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFontButton(this, context, attrs);
        //init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato_regular.ttf");
            setTypeface(typeface);
        }
    }
}
