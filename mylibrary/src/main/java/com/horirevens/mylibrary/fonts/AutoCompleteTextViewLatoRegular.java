package com.horirevens.mylibrary.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;

public class AutoCompleteTextViewLatoRegular extends AppCompatAutoCompleteTextView {
    public AutoCompleteTextViewLatoRegular(Context context) {
        super(context);
        //init();
    }

    public AutoCompleteTextViewLatoRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFontAutoCompleteTextView(this, context, attrs);
        //init();
    }

    public AutoCompleteTextViewLatoRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFontAutoCompleteTextView(this, context, attrs);
        //init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato_regular.ttf");
            setTypeface(typeface);
        }
    }
}
