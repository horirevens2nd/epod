package com.horirevens.mylibrary.fonts;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.horirevens.mylibrary.R;

public class CustomFontHelper {

    public static void setCustomFontTextView(TextView textView, Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFont);
        String font = typedArray.getString(R.styleable.CustomFont_myFont);
        setCustomFontTextView(textView, font, context);
        typedArray.recycle();
    }

    private static void setCustomFontTextView(TextView textView, String font, Context context) {
        if (font == null) {
            return;
        }

        Typeface typeface = FontCache.get(font, context);
        if (typeface != null) {
            textView.setTypeface(typeface);
        }
    }

    public static void setCustomFontTextInputLayout(TextInputLayout textInputLayout, Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFont);
        String font = typedArray.getString(R.styleable.CustomFont_myFont);
        setCustomFontTextInputLayout(textInputLayout, font, context);
        typedArray.recycle();
    }

    private static void setCustomFontTextInputLayout(TextInputLayout textInputLayout, String font, Context context) {
        if (font == null) {
            return;
        }

        Typeface typeface = FontCache.get(font, context);
        if (typeface != null) {
            textInputLayout.setTypeface(typeface);
        }
    }

    public static void setCustomFontTextInputEditText(TextInputEditText textInputEditText, Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFont);
        String font = typedArray.getString(R.styleable.CustomFont_myFont);
        setCustomFontTextInputEditText(textInputEditText, font, context);
        typedArray.recycle();
    }

    private static void setCustomFontTextInputEditText(TextInputEditText textInputEditText, String font, Context context) {
        if (font == null) {
            return;
        }

        Typeface typeface = FontCache.get(font, context);
        if (typeface != null) {
            textInputEditText.setTypeface(typeface);
        }
    }

    public static void setCustomFontEditText(EditText editText, Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFont);
        String font = typedArray.getString(R.styleable.CustomFont_myFont);
        setCustomFontEditText(editText, font, context);
        typedArray.recycle();
    }

    private static void setCustomFontEditText(EditText editText, String font, Context context) {
        if (font == null) {
            return;
        }

        Typeface typeface = FontCache.get(font, context);
        if (typeface != null) {
            editText.setTypeface(typeface);
        }
    }

    public static void setCustomFontButton(Button button, Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFont);
        String font = typedArray.getString(R.styleable.CustomFont_myFont);
        setCustomFontButton(button, font, context);
        typedArray.recycle();
    }

    private static void setCustomFontButton(Button button, String font, Context context) {
        if (font == null) {
            return;
        }

        Typeface typeface = FontCache.get(font, context);
        if (typeface != null) {
            button.setTypeface(typeface);
        }
    }

    public static void setCustomFontAutoCompleteTextView(AutoCompleteTextView autoCompleteTextView, Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFont);
        String font = typedArray.getString(R.styleable.CustomFont_myFont);
        setCustomFontAutoCompleteTextView(autoCompleteTextView, font, context);
        typedArray.recycle();
    }

    private static void setCustomFontAutoCompleteTextView(AutoCompleteTextView autoCompleteTextView, String font, Context context) {
        if (font == null) {
            return;
        }

        Typeface typeface = FontCache.get(font, context);
        if (typeface != null) {
            autoCompleteTextView.setTypeface(typeface);
        }
    }
}
