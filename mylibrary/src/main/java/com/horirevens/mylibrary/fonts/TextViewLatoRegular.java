package com.horirevens.mylibrary.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class TextViewLatoRegular extends AppCompatTextView {
    public TextViewLatoRegular(Context context) {
        super(context);
        //init();
    }

    public TextViewLatoRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFontTextView(this, context, attrs);
        //init();
    }

    public TextViewLatoRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFontTextView(this, context, attrs);
        //init();
    }

    private void init(Context context, AttributeSet attrs) {
        if (!isInEditMode()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato_regular.ttf");
            setTypeface(typeface);
        }
    }
}
