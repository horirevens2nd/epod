### Versi 4.4.0
* Penambahan fitur alamat baru dan catatan pada menu berhasil (opsional)
* Penambahan fitur data penerima dan produk pada tab Delivery Order
* Penambahan fitur telp/WA jika terdapat no telp/WA pada data penerima
* Perbaikan tampilan aplikasi (minor)

### Versi 4.3.2
* Perubahan arah Load Balancer

### Versi 4.3.1
* Perbaikan bug :
    * Item duplikat
    * Data tanggal kemarin masih muncul
    * Data tidak tampil pada tab DO setelah tutup DO
    * Loading tanpa henti
    * Gagal upload data pending
* Perbaikan entri DO untuk resi pengganti (item dengan barcode 9 digit)
* Perbaikan penanganan berkas yang gagal terunggah
* Perbaikan menu File Upload Usage
* Mendukung untuk Android Pie 9.0

### Versi 4.3.0
* Hapus berkas foto/tanda tangan secara otomatis setelah sukses mengunggah ke server dan jika terjadi kegagalan pada saat unggah berkas maka akan diunggah ulang ketika membuka aplikasi namun dengan syarat berkas foto/tanda tangan masih ada pada perangkat
* Mendukung untuk memindai QR Code dan barcode pengganti (9 digit dan inisial BN)
* Penambahan menu Utilities (Antaran dan Kode Status Update) untuk mengunduh data dari server tanpa perlu melakukan logout
* Penambahan pengecekan GPS sebelum melakukan update status
* Perbaikan sistem terutama untuk Android 8.x (Oreo)
* Perbaikan performa pada tampilan (lag pada saat scroll dan swipe)
* Perbaikan beberapa bug pada versi sebelumnya (4.1.0)
* Full Cloud Based
