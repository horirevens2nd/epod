package horirevens.com.epod;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class VersionNotificationSession {
    private static final String TAG = VersionNotificationSession.class.getName();
    private static final String VERSION = "version";
    private static final String TYPE_VERSION = "typeVersion";
    private static final String DATE = "date";
    private static final String PREF_NAME = "VersionNotificationPreferences";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public VersionNotificationSession(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setSession(String version, String typeVersion, String date) {
        editor.putString(VERSION, version);
        editor.putString(TYPE_VERSION, typeVersion);
        editor.putString(DATE, date);
        editor.commit();
    }

    public HashMap<String, String> getSession() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(VERSION, preferences.getString(VERSION, null));
        hashMap.put(TYPE_VERSION, preferences.getString(TYPE_VERSION, null));
        hashMap.put(DATE, preferences.getString(DATE, null));
        return hashMap;
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }
}
