package horirevens.com.epod.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.ImageViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.*;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.horirevens.mylibrary.fonts.ButtonLatoBold;
import com.horirevens.mylibrary.fonts.TextInputEditTextLatoRegular;
import com.horirevens.mylibrary.fonts.TextViewLatoBold;
import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import horirevens.com.epod.libraries.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

import es.dmoral.toasty.Toasty;
import horirevens.com.epod.AppConfig;
import horirevens.com.epod.DateSession;
import horirevens.com.epod.LoginSession;
import horirevens.com.epod.MainApplication;
import horirevens.com.epod.R;
import horirevens.com.epod.activities.AnyOrientation;
import horirevens.com.epod.activities.CameraActivity;
import horirevens.com.epod.activities.CollectiveActivity;
import horirevens.com.epod.adapters.MainFragmentTab1Adapter;
import horirevens.com.epod.adapters.StatusAdapter;
import horirevens.com.epod.helpers.DatabaseHelper;
import horirevens.com.epod.models.Antaran;
import horirevens.com.epod.models.FileUploadError;
import horirevens.com.epod.models.FileUploadUsage;
import horirevens.com.epod.models.Status;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainFragmentTab1 extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener,
        MainFragmentTab1Adapter.MainFragmentTab1Listener,
        AlertDialogConfirmation.AlertDialogConfirmationListener,
        AlertDialogInformation.AlertDialogInformationListener {
    private static final String TAG = MainFragmentTab1.class.getName();
    private static final int REQUEST_CODE_CAMERA = 101;
    private static final int REQUEST_CODE_COLLECTIVE_ACTIVITY = 102;

    private ArrayList<Antaran> arrayList = new ArrayList<>();
    private ArrayList<Antaran> arrayListByDO = new ArrayList<>();
    private DatabaseHelper db;
    private MyFile myFile;
    private MyUploadFile myUploadFile;
    private MainFragmentTab1Adapter adapter;
    private AlertDialogConfirmation alertDialogConfirmation;
    private AlertDialogInformation alertDialogInformation;
    private AlertDialogLoading alertDialogLoading;
    private AlertDialog adUpdateStatus, adStatus, adRecipient, adSignature, adValidation;
    private SharedPreferences locationPreferences;
    private OkHttpClient client;

    private TextViewLatoBold tvPendingBerhasil, tvPendingGagal;
    private TextViewLatoRegular tvInfoWarning;
    private TextInputEditTextLatoRegular etIdDeliveryOrder;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ConstraintLayout infoContainer, warningContainer, pendingContainer;
    private SearchView searchView;

    private String idPengantar, idKantor, baseUrl, uid, idItem, idDeliveryOrder, keteranganBerhasil, keteranganGagal;
    private String signatureFileName, imageFileName, remotePath;
    private String idStatus, keterangan, tindakan, kodeLn, kodeDn, kodeStatusGagal;
    private String dateFromSession;
    private String alamatBaru, catatan;
    private File signatureFile, compressedImage;
    private boolean isBerhasil, isWithPhoto, isUpdateByDO;
    private Double latitude, longitude;

    public MainFragmentTab1() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        db = new DatabaseHelper(getContext());
        alertDialogConfirmation = new AlertDialogConfirmation(getContext(), this);
        alertDialogInformation = new AlertDialogInformation(getContext(), this);
        alertDialogLoading = new AlertDialogLoading(getContext());
        myFile = new MyFile(getContext());
        myUploadFile = new MyUploadFile(getContext());
        client = MainApplication.getUnsafeOkHttpClient();
        locationPreferences = getContext().getSharedPreferences("LocationPreferences", Context.MODE_PRIVATE);
        isBerhasil = false;
        isWithPhoto = false;
        isUpdateByDO = false;
        latitude = 0.000000;
        longitude = 0.000000;
        alamatBaru = "";
        catatan = "";

        LoginSession loginSession = new LoginSession(getContext());
        HashMap<String, String> hashMap = loginSession.getSession();
        idPengantar = hashMap.get(AppConfig.ID);
        idKantor = hashMap.get(AppConfig.ID_KANTOR);
        baseUrl = hashMap.get(AppConfig.BASE_URL);

        DateSession dateSession = new DateSession(getContext());
        HashMap<String, String> hashMap2 = dateSession.getSession();
        dateFromSession = hashMap2.get("date");

        View view = inflater.inflate(R.layout.content_main_fragment, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        infoContainer = (ConstraintLayout) view.findViewById(R.id.info_container);
        infoContainer.setVisibility(View.GONE);
        warningContainer = (ConstraintLayout) view.findViewById(R.id.warning_container);
        warningContainer.setVisibility(View.GONE);
        pendingContainer = (ConstraintLayout) view.findViewById(R.id.pending_container);
        pendingContainer.setVisibility(View.GONE);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setVisibility(View.GONE);

        tvPendingBerhasil = (TextViewLatoBold) view.findViewById(R.id.tv_pending_berhasil);
        tvPendingGagal = (TextViewLatoBold) view.findViewById(R.id.tv_pending_gagal);
        tvInfoWarning = (TextViewLatoRegular) view.findViewById(R.id.tv_info_warning);

        storeData();

        return view;
    }

    private void storeData() {
        Log.i(TAG, "storeData tab1");
        Date date = new Date(System.currentTimeMillis());
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);

        arrayList.clear();
        arrayList = db.getAllAntaranProses(currentDate);

        adapter = new MainFragmentTab1Adapter(getContext(), arrayList, this);
        adapter.notifyDataSetChanged();

        if (adapter.getItemCount() > 0) {
            infoContainer.setVisibility(View.GONE);
            warningContainer.setVisibility(View.GONE);
            pendingContainer.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.setAdapter(adapter);
        } else {
            int countItemUnclosed = db.getCountAntaranUnclosed(currentDate);
            if (countItemUnclosed > 0) {
                infoContainer.setVisibility(View.GONE);
                warningContainer.setVisibility(View.VISIBLE);
                pendingContainer.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);

                String message =
                        "Masih ada " + countItemUnclosed + " item yang belum ditutup pada menu Delivery Order. " +
                        "Lakukan tutup terlebih dahulu atau hapus semua item agar tidak ada data yang terbuka";
                tvInfoWarning.setText(message);
            } else {
                String message =
                        "Masih ada data dengan status pending pada antaran Anda. " +
                        "Lakukan unggah data dengan cara klik tombol unggah yang ada " +
                        "pada pojok kanan atas Tab Berhasil/Gagal untuk menaikkan data";
                tvInfoWarning.setText(message);

                ArrayList<Antaran> pendingBerhasil = new ArrayList<>();
                pendingBerhasil.clear();
                pendingBerhasil = db.getAllAntaranBerhasilPending(currentDate);

                ArrayList<Antaran> pendingGagal = new ArrayList<>();
                pendingGagal.clear();
                pendingGagal = db.getAllAntaranGagalPending(currentDate);

                if (pendingBerhasil.size() > 0 || pendingGagal.size() > 0) {
                    infoContainer.setVisibility(View.GONE);
                    warningContainer.setVisibility(View.VISIBLE);
                    pendingContainer.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

                    if (pendingBerhasil.size() > 0) {
                        tvPendingBerhasil.setText("Berhasil Antar : " + String.valueOf(pendingBerhasil.size() + " item"));
                    } else {
                        tvPendingBerhasil.setVisibility(View.GONE);
                    }

                    if (pendingGagal.size() > 0) {
                        tvPendingGagal.setText("Gagal Antar : " + String.valueOf(pendingGagal.size() + " item"));
                    } else {
                        tvPendingGagal.setVisibility(View.GONE);
                    }

                    //int pendingItem = pendingBerhasil.size() + pendingGagal.size();
                    //scheduleNotification("pending", pendingItem);
                } else {
                    infoContainer.setVisibility(View.VISIBLE);
                    warningContainer.setVisibility(View.GONE);
                    pendingContainer.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
        }

        setCountItemOnTab1();
        setCountItemOnTab2(currentDate);
        setCountItemOnTab3(currentDate);
    }

    private void setCountItemOnTab1() {
        Log.d(TAG, "setCountItemOnTab1");
        int countAll = adapter.getItemCount();

        FragmentManager fragmentManager = getParentFragment().getFragmentManager();
        MainFragment mainFragment = (MainFragment) fragmentManager.findFragmentById(R.id.content_main);
        TextViewLatoBold tvCount = mainFragment.getTvCountItemOnTab1();

        if (countAll > 0) {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(countAll));
        } else {
            tvCount.setVisibility(View.INVISIBLE);
        }
    }

    private void setCountItemOnTab2(String currentDate) {
        Log.d(TAG, "setCountItemOnTab2");
        int countAll = db.getCountAntaranBerhasil(currentDate);
        int countSync = db.getCountAntaranBerhasilNonPending(currentDate);

        FragmentManager fragmentManager = getParentFragment().getFragmentManager();
        MainFragment mainFragment = (MainFragment) fragmentManager.findFragmentById(R.id.content_main);
        TextViewLatoBold tvCount = mainFragment.getTvCountItemOnTab2();

        if (countAll > 0) {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(countAll) + " (" + String.valueOf(countSync) + ")");
        } else {
            tvCount.setVisibility(View.INVISIBLE);
        }
    }

    private void setCountItemOnTab3(String currentDate) {
        Log.d(TAG, "setCountItemOnTab3");
        int countAll = db.getCountAntaranGagal(currentDate);
        int countSync = db.getCountAntaranGagalNonPending(currentDate);

        FragmentManager fragmentManager = getParentFragment().getFragmentManager();
        MainFragment mainFragment = (MainFragment) fragmentManager.findFragmentById(R.id.content_main);
        TextViewLatoBold tvCount = mainFragment.getTvCountItemOnTab3();

        if (countAll > 0) {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(countAll) + " (" + String.valueOf(countSync) + ")");
        } else {
            tvCount.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (this.isVisible()) {
            storeData();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();
        inflater.inflate(R.menu.main_fragment_tab1, menu);
        searchView = (SearchView) menu.findItem(R.id.nav_search).getActionView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_search:
                searchIdItem(menuItem);
                return true;
            case R.id.nav_scan_camera:
                IntentIntegrator.forSupportFragment(this)
                        .setCaptureActivity(AnyOrientation.class)
                        .setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES)
                        .setPrompt("Letakkan nomor item tepat pada garis merah")
                        .setOrientationLocked(true)
                        .setBeepEnabled(true)
                        .initiateScan();
                return true;
            case R.id.nav_delivery_order:
                showDialogEntryDeliveryOrder();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void validateIdItemFromScanner(String nIdItem) {
        Log.i(TAG, "validateIdItemFromScanner");
        String mUid,mIdItem,  mIdStatus, mIdDeliveryOrder;

        mIdDeliveryOrder = db.getIdDeliveryOrderFromAntaran(nIdItem);
        mIdItem = db.getIdItemFromAntaran(nIdItem, mIdDeliveryOrder);
        mUid = db.getUidFromAntaran(nIdItem, mIdDeliveryOrder);
        mIdStatus = db.getIdStatusFromAntaran(nIdItem);

        if (mIdDeliveryOrder.isEmpty() || mIdDeliveryOrder.length() == 0) {
            Toasty.error(getContext(), getResources().getString(R.string.id_item_not_found), 3000, false).show();
        } else if (!mIdItem.equalsIgnoreCase(nIdItem)) {
            Toasty.error(getContext(), getResources().getString(R.string.id_item_not_found), 3000, false).show();
        } else if (!mIdStatus.equalsIgnoreCase("P01")) {
            Toasty.error(getContext(), getResources().getString(R.string.id_item_status_success), 3000, false).show();
        } else {
            uid = mUid;
            idItem = mIdItem;
            idDeliveryOrder = mIdDeliveryOrder;

            showDialogUpdateStatus();
        }
    }

    private void searchIdItem(MenuItem menuItem) {
        final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (menuItem != null) {
            MenuItemCompat.setOnActionExpandListener(menuItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem menuItem) {
                    if (searchView != null) {
                        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
                        searchView.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                        searchView.setIconified(false);
                        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                            @Override
                            public boolean onQueryTextSubmit(String query) {
                                return false;
                            }

                            @Override
                            public boolean onQueryTextChange(String newText) {
                                adapter.search(newText);
                                recyclerView.invalidate();
                                return false;
                            }
                        });
                    }
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                    return true;
                }
            });

            searchView = (SearchView) menuItem.getActionView();
        }
    }

    @Override
    public void onItemClick(Antaran antaran) {
        uid = antaran.getUid();
        idItem = antaran.getIdItem();
        idDeliveryOrder = antaran.getIdDeliveryOrder();

        showDialogUpdateStatus();
    }

    @Override
    public void onItemLongClick(Antaran antaran) {
        Intent intent = new Intent(getActivity(), CollectiveActivity.class);
        startActivityForResult(intent, REQUEST_CODE_COLLECTIVE_ACTIVITY);
    }

    @Override
    public void onPhoneIconClick(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @Override
    public void onWhatsappIconClick(String phoneNumber) {
        // Send whatsapp message unsaved number
        PackageManager packageManager = getActivity().getPackageManager();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        try {
            String url = "https://api.whatsapp.com/send?phone="+ phoneNumber +"&text=" + URLEncoder.encode("", "UTF-8");
            intent.setPackage("com.whatsapp");
            intent.setData(Uri.parse(url));
            if (intent.resolveActivity(packageManager) != null) {
                getActivity().startActivity(intent);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showDialogUpdateStatus() {
        Log.i(TAG, "showDialogUpdateStatus");
        View view;

        if (isUpdateByDO) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.alert_dialog_update_status_collective, null, false);
            TextViewLatoRegular tvTotalItem = (TextViewLatoRegular) view.findViewById(R.id.tv_total_item);
            tvTotalItem.setText(String.valueOf(arrayListByDO.size()));
        } else {
            ArrayList<Antaran> arrayList = new ArrayList<>();
            arrayList = db.getAllAntaranByIdItem(idItem);

            view = LayoutInflater.from(getContext()).inflate(R.layout.alert_dialog_update_status2, null, false);
            TextViewLatoRegular tvIdItem = (TextViewLatoRegular) view.findViewById(R.id.tv_id_item);
            tvIdItem.setText(idItem);
            TextViewLatoRegular tvNamaPenerima = (TextViewLatoRegular) view.findViewById(R.id.tv_nama_penerima);
            tvNamaPenerima.setText(arrayList.get(0).getNamaPenerima());
            TextViewLatoRegular tvAlamatPenerima = (TextViewLatoRegular) view.findViewById(R.id.tv_alamat_penerima);
            tvAlamatPenerima.setText(StringUtils.uppercaseSetences(arrayList.get(0).getAlamatPenerima()));

            TextViewLatoRegular tvLabelBsu = (TextViewLatoRegular) view.findViewById(R.id.tv_label_bsu);
            TextViewLatoRegular tvBsu = (TextViewLatoRegular) view.findViewById(R.id.tv_bsu);
            TextViewLatoRegular tvHpPenerima = (TextViewLatoRegular) view.findViewById(R.id.tv_hp_penerima);
            AppCompatImageView ivPhone = (AppCompatImageView) view.findViewById(R.id.iv_phone);
            AppCompatImageView ivRupiah = (AppCompatImageView) view.findViewById(R.id.iv_rupiah);

            String hpPenerima = arrayList.get(0).getHpPenerima();
            if (hpPenerima.equalsIgnoreCase("0") || hpPenerima.equalsIgnoreCase("")
                || hpPenerima.length() < 11) {
                ivPhone.setVisibility(View.GONE);
                tvHpPenerima.setVisibility(View.GONE);
            } else {
                if (hpPenerima.substring(0, 1).equalsIgnoreCase("6")) { //628140513878
                    hpPenerima = "+" + hpPenerima; // +6282140513878
                } else if (hpPenerima.substring(0, 1).equalsIgnoreCase("0")) { // 082140513878
                    String countryCode = hpPenerima.substring(0, 1).replace("0", "+62"); // 0 > +62
                    String phoneNumber = hpPenerima.substring(1); // 082140513878 > 821405138787
                    hpPenerima = countryCode + phoneNumber; // +6282140513878
                }

                ivPhone.setVisibility(View.VISIBLE);
                tvHpPenerima.setVisibility(View.VISIBLE);
                tvHpPenerima.setText(hpPenerima);
            }

            String tipe = arrayList.get(0).getTipe();
            if (tipe.equalsIgnoreCase("COD")) {
                String bsuCod = String.format(Locale.getDefault(), "%,.0f", arrayList.get(0).getBsuCod());
                tvLabelBsu.setText("BSU COD");
                tvBsu.setText(bsuCod);
            } else if (tipe.equalsIgnoreCase("BLB")) {
                if (arrayList.get(0).getBsuBlb() != 0 && arrayList.get(0).getBsuTax() != 0) {
                    double bsuBlbTax = arrayList.get(0).getBsuBlb() + arrayList.get(0).getBsuTax();
                    String bsuBlb = String.format(Locale.getDefault(), "%,.0f", bsuBlbTax);
                    tvLabelBsu.setText("BSU BLB+PAJAK");
                    tvBsu.setText(bsuBlb);
                } else if (arrayList.get(0).getBsuBlb() != 0 && arrayList.get(0).getBsuTax() == 0){
                    String bsuBlb = String.format(Locale.getDefault(), "%,.0f", arrayList.get(0).getBsuBlb());
                    tvLabelBsu.setText("BSU BLB");
                    tvBsu.setText(bsuBlb);
                }
            } else {
                ivRupiah.setVisibility(View.GONE);
                tvLabelBsu.setVisibility(View.GONE);
                tvBsu.setVisibility(View.GONE);
            }
        }

        ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBerhasil = true;
                showDialogStatus();
                adUpdateStatus.dismiss();
            }
        });

        ButtonLatoBold btnNegative = (ButtonLatoBold) view.findViewById(R.id.btn_negative);
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBerhasil = false;
                showDialogStatus();
                adUpdateStatus.dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setView(view);
        adUpdateStatus = builder.create();
        adUpdateStatus.setCanceledOnTouchOutside(true);
        adUpdateStatus.show();
    }

    private void showDialogStatus() {
        Log.i(TAG, "showDialogStatus");
        String title;
        ArrayList<Status> arrayList = new ArrayList<>();
        arrayList.clear();

        if (isBerhasil) {
            arrayList = db.getAllStatusBerhasil();
            title = getResources().getString(R.string.title_status_success);
        } else {
            arrayList = db.getAllStatusGagal();
            title = getResources().getString(R.string.title_status_failed);
        }

        StatusAdapter adapter = new StatusAdapter(getContext(), arrayList, new StatusAdapter.StatusListener() {
            @Override
            public void onItemClicked(Status status) {
                idStatus = status.getId();
                keterangan = status.getKeterangan();
                tindakan = status.getTindakan();
                kodeLn = status.getKodeLn();
                kodeDn = status.getKodeDn();

                if (idStatus.equalsIgnoreCase("B01") || idStatus.equalsIgnoreCase("B02") ||
                    idStatus.equalsIgnoreCase("B03") || idStatus.equalsIgnoreCase("B04") ||
                    idStatus.equalsIgnoreCase("B05") || idStatus.equalsIgnoreCase("B06") ||
                    idStatus.equalsIgnoreCase("B07") || idStatus.equalsIgnoreCase("B08") ||
                    idStatus.equalsIgnoreCase("B09") || idStatus.equalsIgnoreCase("B10") ||
                    idStatus.equalsIgnoreCase("B11") || idStatus.equalsIgnoreCase("B12"))
                {
                    if (status.getTerakhir().equalsIgnoreCase("1")) {
                        adStatus.dismiss();
                        showDialogRecipient();
                    }
                } else {
                    if (idStatus.equalsIgnoreCase("G01")) {
                        kodeStatusGagal = "15";
                    } else if (idStatus.equalsIgnoreCase("G02") || idStatus.equalsIgnoreCase("G03") ||
                            idStatus.equalsIgnoreCase("G04")) {
                        kodeStatusGagal = "14";
                    } else if (idStatus.equalsIgnoreCase("G05")) {
                        kodeStatusGagal = "17";
                    }

                    adStatus.dismiss();
                    showDialogStatusLevel2(idStatus);
                }
            }
        });

        View view = LayoutInflater.from(getContext()).inflate(R.layout.alert_dialog_status, null, false);
        TextViewLatoBold tvTitle = (TextViewLatoBold) view.findViewById(R.id.tv_title);
        tvTitle.setText(title);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setView(view);
        adStatus = builder.create();
        adStatus.setCanceledOnTouchOutside(true);
        adStatus.show();
    }

    private void showDialogStatusLevel2(String id) {
        Log.i(TAG, "showDialogStatusLevel2");
        ArrayList<Status> arrayList = new ArrayList<>();
        arrayList.clear();
        arrayList = db.getAllStatusGagalLevel2(id);

        StatusAdapter adapter = new StatusAdapter(getContext(), arrayList, new StatusAdapter.StatusListener() {
            @Override
            public void onItemClicked(Status status) {
                idStatus = status.getId();
                keterangan = status.getKeterangan();
                tindakan = status.getTindakan();
                kodeLn = status.getKodeLn();
                kodeDn = status.getKodeDn();

                if (status.getTerakhir().equalsIgnoreCase("1")) {
                    adStatus.dismiss();
                    startCameraActivity();
                }
            }
        });

        View view = LayoutInflater.from(getContext()).inflate(R.layout.alert_dialog_status, null, false);
        TextViewLatoBold tvTitle = (TextViewLatoBold) view.findViewById(R.id.tv_title);
        tvTitle.setText(getResources().getString(R.string.title_status_failed));

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setView(view);
        adStatus = builder.create();
        adStatus.setCanceledOnTouchOutside(true);
        adStatus.show();
    }

    private void showDialogRecipient() {
        Log.i(TAG, "showDialogRecipient");
        View view = LayoutInflater.from(getContext()).inflate(R.layout.alert_dialog_entry_recipient, null, false);
        TextViewLatoBold tvTitle = (TextViewLatoBold) view.findViewById(R.id.tv_title);
        tvTitle.setText(getResources().getString(R.string.title_entry_recipient));

        TextViewLatoRegular tvNamaPenerima = (TextViewLatoRegular) view.findViewById(R.id.tv_nama_penerima);
        tvNamaPenerima.setText(db.getNamaPenerimaFromAntaran(idItem));
        TextViewLatoRegular tvAlamatPenerima = (TextViewLatoRegular) view.findViewById(R.id.tv_alamat_penerima);
        tvAlamatPenerima.setText(StringUtils.uppercaseSetences(db.getAlamatPenerimaFromAntaran(idItem)));

        TextInputEditTextLatoRegular etAlamatBaru = (TextInputEditTextLatoRegular) view.findViewById(R.id.et_alamat_baru);
        TextInputEditTextLatoRegular etCatatan = (TextInputEditTextLatoRegular) view.findViewById(R.id.et_catatan);

        final TextInputEditTextLatoRegular etKeterangan = (TextInputEditTextLatoRegular) view.findViewById(R.id.et_keterangan);
        etKeterangan.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateName(etKeterangan)) {
                        alamatBaru = etAlamatBaru.getText().toString().trim().toUpperCase();
                        catatan = etCatatan.getText().toString().trim().toUpperCase();

                        adRecipient.dismiss();
                        showDialogSignature();
                    }
                }

                return false;
            }
        });

        ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateName(etKeterangan)) {
                    alamatBaru = etAlamatBaru.getText().toString().trim().toUpperCase();
                    catatan = etCatatan.getText().toString().trim().toUpperCase();

                    adRecipient.dismiss();
                    showDialogSignature();
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setView(view);
        adRecipient = builder.create();
        adRecipient.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        adRecipient.setCanceledOnTouchOutside(true);
        adRecipient.show();
    }

    private boolean validateName(TextInputEditTextLatoRegular etKeterangan) {
        Log.i(TAG, "validateName");
        keteranganBerhasil = etKeterangan.getText().toString().trim().toUpperCase();

        if (keteranganBerhasil.isEmpty() || keteranganBerhasil.length() == 0) {
            Toasty.error(getContext(), getString(R.string.name_empty), 3000, false).show();
            return false;
        } else if (!keteranganBerhasil.matches(AppConfig.NAME_PATTERN)) {
            Toasty.error(getContext(), getString(R.string.name_invalid_format), 3000, false).show();
            return false;
        }

        return true;
    }
    
    private void showDialogSignature() {
        Log.i(TAG, "showDialogSignature");
        View view = LayoutInflater.from(getContext()).inflate(R.layout.alert_dialog_signature, null, false);

        final ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        btnPositive.setEnabled(false);
        btnPositive.setTextColor(ContextCompat.getColor(getContext(), R.color.disableText));

        final ButtonLatoBold btnNegative = (ButtonLatoBold) view.findViewById(R.id.btn_negative);
        btnNegative.setEnabled(false);
        btnNegative.setTextColor(ContextCompat.getColor(getContext(), R.color.disableText));

        final SignaturePad signaturePad = (SignaturePad) view.findViewById(R.id.signature_pad);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                btnNegative.setEnabled(true);
                btnPositive.setEnabled(true);

                btnNegative.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                btnPositive.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            }

            @Override
            public void onClear() {
                btnNegative.setEnabled(false);
                btnPositive.setEnabled(false);

                btnNegative.setTextColor(ContextCompat.getColor(getContext(), R.color.disableText));
                btnPositive.setTextColor(ContextCompat.getColor(getContext(), R.color.disableText));
            }
        });

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = signaturePad.getSignatureBitmap();

                if (addFileIntoStorage(bitmap)) {
                    adSignature.dismiss();

                    String message = "Apakah Anda ingin menambahkan foto?";
                    alertDialogConfirmation
                            .setMessage(message)
                            .setActionPositive("startCameraActivity")
                            .setActionNegative("showDialogValidation")
                            .showDialog();
                } else {
                    String message = "Simpan tanda tangan gagal. Pastikan perizinan \"STORAGE\" telah di-allow";
                    Toasty.error(getContext(), message, 3000, false).show();
                }
            }
        });

        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setView(view);
        adSignature = builder.create();
        adSignature.setCanceledOnTouchOutside(true);
        adSignature.show();
    }

    private void convertToJpg(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat. JPEG, 80, stream);
        stream.close();
    }

    private void scanMediaFile(File fileName) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(fileName);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private boolean addFileIntoStorage(Bitmap bitmap) {
        boolean result = false;
        try {
            if (isUpdateByDO) {
                signatureFileName = "SIGN_" + arrayListByDO.get(0).getIdItem() + ".jpg";
            } else {
                signatureFileName = "SIGN_" + idItem + ".jpg";
            }

            signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(getContext(), AppConfig.FOLDER_SIGNATURE), signatureFileName);

            convertToJpg(bitmap, signatureFile);
            scanMediaFile(signatureFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void startCameraActivity() {
        Log.i(TAG, "startCameraActivity");
        Intent intent = new Intent(getActivity(), CameraActivity.class);
        if (isUpdateByDO) {
            intent.putExtra("idItem", arrayListByDO.get(0).getIdItem());
        } else {
            intent.putExtra("idItem", idItem);
        }
        startActivityForResult(intent, REQUEST_CODE_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CAMERA) {
            String action = data.getStringExtra("action");

            if (action.equalsIgnoreCase("save")) {
                File imageFile;

                if (isBerhasil) {
                    try {
                        isWithPhoto = true;
                        imageFile = new File(data.getStringExtra("imageFile"));
                        compressedImage = new ImageCompressor(getContext())
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(80)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setAlbumName(AppConfig.FOLDER_IMAGE)
                                .compressToFile(imageFile);

                        imageFileName = compressedImage.getName();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    showDialogValidation();
                } else {
                    try {
                        isWithPhoto = true;
                        imageFile = new File(data.getStringExtra("imageFile"));
                        compressedImage = new ImageCompressor(getContext())
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(80)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setAlbumName(AppConfig.FOLDER_IMAGE)
                                .compressToFile(imageFile);

                        imageFileName = compressedImage.getName();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    showDialogValidation();
                }
            }

            if (action.equalsIgnoreCase("cancel")) {
                // Do nothing
            }
        }  else if (requestCode == REQUEST_CODE_COLLECTIVE_ACTIVITY) {
            String action = data.getStringExtra("action");

            if (action.equalsIgnoreCase("success")) {
                storeData();
            }

            if (action.equalsIgnoreCase("cancel")) {
                // Do nothing
            }
        } else {
            IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (intentResult != null) {
                if (resultCode == getActivity().RESULT_OK) {
                    String mIdItem = intentResult.getContents();

                    validateIdItemFromScanner(mIdItem);
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
    
    private void showDialogValidation() {
        Log.i(TAG, "showDialogValidation");
        View view;

        if (isUpdateByDO) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.alert_dialog_validation_collective, null, false);
            TextViewLatoRegular tvTotalItem = (TextViewLatoRegular) view.findViewById(R.id.tv_total_item);
            tvTotalItem.setText(String.valueOf(arrayListByDO.size()));
        } else {
            view = LayoutInflater.from(getContext()).inflate(R.layout.alert_dialog_validation, null, false);
            TextViewLatoRegular tvIdItem = (TextViewLatoRegular) view.findViewById(R.id.tv_id_item);
            tvIdItem.setText(idItem);
        }

        TextViewLatoRegular tvLabel2 = (TextViewLatoRegular) view.findViewById(R.id.tv_label2);
        TextViewLatoRegular tvKeterangan = (TextViewLatoRegular) view.findViewById(R.id.tv_keterangan); 
        TextViewLatoRegular tvTandaTangan = (TextViewLatoRegular) view.findViewById(R.id.tv_tanda_tangan);
        TextViewLatoRegular tvFoto = (TextViewLatoRegular) view.findViewById(R.id.tv_foto);
        TextViewLatoRegular textView = (TextViewLatoRegular) view.findViewById(R.id.textview);
        ImageView ivTandaTangan = (ImageView) view.findViewById(R.id.iv_tanda_tangan); 
        ImageView ivFoto = (ImageView) view.findViewById(R.id.iv_foto);
        
        if (isBerhasil) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            Bitmap signatureBitmap = BitmapFactory.decodeFile(String.valueOf(signatureFile), options);
            ivTandaTangan.setImageBitmap(signatureBitmap);
            Bitmap imageBitmap = BitmapFactory.decodeFile(String.valueOf(compressedImage), options);
            ivFoto.setImageBitmap(imageBitmap);
            
            tvLabel2.setText("Penerima");
            tvKeterangan.setText(keterangan + " [" + keteranganBerhasil + "]");

            if (!isWithPhoto) {
                imageFileName = "";
                textView.setVisibility(View.GONE);
                tvFoto.setVisibility(View.GONE);
                ivFoto.setVisibility(View.GONE);
            }
        } else {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            Bitmap imageBitmap = BitmapFactory.decodeFile(String.valueOf(compressedImage), options);
            ivFoto.setImageBitmap(imageBitmap);

            String id = idStatus.substring(0, 3);
            if (id.equals("G01")) {
                tvLabel2.setText("Alasan Retur");
            } else if (id.equals("G02")) {
                tvLabel2.setText("Alasan Dipanggil");
            } else if (id.equals("G03")) {
                tvLabel2.setText("Alasan Ditahan");
            } else if (id.equals("G04")) {
                tvLabel2.setText("Alasan Diteruskan");
            } else if (id.equals("G05")) {
                tvLabel2.setText("Alasan Antar Ulang");
            } else {
                tvLabel2.setText("Alasan Gagal");
            }

            keteranganGagal = kodeLn + " " + keterangan + " - " + tindakan;
            tvKeterangan.setText(keterangan);

            signatureFileName = "";
            tvTandaTangan.setVisibility(View.GONE);
            ivTandaTangan.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);
        }

        ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    adValidation.dismiss();
                    updateAntaranIntoServer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        ButtonLatoBold btnNegative = (ButtonLatoBold) view.findViewById(R.id.btn_negative);
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adValidation.dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setView(view);
        adValidation = builder.create();
        adValidation.setCanceledOnTouchOutside(true);
        adValidation.show();
    }

    private void updateAntaranIntoServer() throws IOException {
        Log.i(TAG, "updateAntaranIntoServer");
        alertDialogLoading.setCancelAble(false).showDialog();

        Date date = new Date(System.currentTimeMillis());
        final String currentDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(date);
        final String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        String mLatitude = locationPreferences.getString("latitude", null);
        String mLongitude = locationPreferences.getString("longitude", null);
        final String keteranganUpdate;

        if (mLatitude == null || mLongitude == null) {
            mLatitude = "0.000000";
            mLongitude = "0.000000";

            latitude = Double.parseDouble(mLatitude);
            longitude = Double.parseDouble(mLongitude);
        } else {
            latitude = Double.parseDouble(mLatitude);
            longitude = Double.parseDouble(mLongitude);
        }

        if (isBerhasil) {
            keteranganUpdate = keteranganBerhasil;
        } else {
            keteranganUpdate = keteranganGagal;
        }

        if (isUpdateByDO) {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            try {
                for (int i=0; i<arrayListByDO.size(); i++) {
                    JSONObject jsonObject2 = new JSONObject();
                    jsonObject2.put(AppConfig.UID, arrayListByDO.get(i).getUid());
                    jsonObject2.put(AppConfig.ID_ITEM, arrayListByDO.get(i).getIdItem());
                    jsonObject2.put(AppConfig.ID_DELIVERY_ORDER, arrayListByDO.get(i).getIdDeliveryOrder());
                    jsonObject2.put(AppConfig.ID_STATUS, idStatus);
                    jsonObject2.put(AppConfig.WAKTU_UPDATE, currentDateTime);
                    jsonObject2.put(AppConfig.GARIS_LINTANG, latitude);
                    jsonObject2.put(AppConfig.GARIS_BUJUR, longitude);
                    jsonObject2.put(AppConfig.TANDA_TANGAN, signatureFileName);
                    jsonObject2.put(AppConfig.FOTO, imageFileName);
                    jsonObject2.put(AppConfig.ID_PENGANTAR, idPengantar);
                    jsonObject2.put(AppConfig.ID_KANTOR, idKantor);
                    jsonObject2.put(AppConfig.KODE_DN, kodeDn);
                    jsonObject2.put(AppConfig.TANGGAL_PERANGKAT, currentDate);
                    jsonObject2.put(AppConfig.ALAMAT_BARU, alamatBaru);
                    jsonObject2.put(AppConfig.CATATAN, catatan);

                    if (isBerhasil) {
                        jsonObject2.put(AppConfig.KETERANGAN, keteranganBerhasil);
                    } else {
                        jsonObject2.put(AppConfig.KETERANGAN, keteranganGagal);
                    }

                    jsonArray.put(jsonObject2);
                }

                jsonObject.put("action", "update_collective");
                jsonObject.put("data", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(AppConfig.MEDIA_TYPE_JSON, jsonObject.toString());

            Request request = new Request.Builder()
                    .url(baseUrl + "antaran_collective.php")
                    .post(body)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(final Call call, IOException e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentDate.equals(dateFromSession)) {
                                int j = 0;
                                for (int i=0; i<arrayListByDO.size(); i++) {
                                    String uid = arrayListByDO.get(i).getUid();

                                    db.updateIntoAntaran(
                                        new Antaran(
                                            uid, idStatus, keteranganUpdate, currentDateTime, latitude, longitude,
                                            signatureFileName, imageFileName, alamatBaru, catatan, "0"
                                        )
                                    );
                                    j++;

                                    if (j == arrayListByDO.size()) {
                                        alertDialogLoading.dismissDialog();

                                        String message = "Update status item berhasil (pending) dengan jumlah " + arrayListByDO.size() + " item";
                                        Toasty.info(getContext(), message, 3000, false).show();

                                        storeData();
                                        resetAllValue();
                                    }
                                }
                            } else {
                                String errorMessage =
                                        "Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server" +
                                        "\n\nTanggal Perangkat : " + MyDate.setDateTimeIndo(currentDate) + "\nTanggal Server       : " +
                                        MyDate.setDateTimeIndo(dateFromSession);

                                alertDialogLoading.dismissDialog();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        }
                    });

                    call.cancel();
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try(final ResponseBody responseBody = response.body()) {
                        if (!response.isSuccessful()) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (currentDate.equals(dateFromSession)) {
                                        int j = 0;
                                        for (int i=0; i<arrayListByDO.size(); i++) {
                                            String uid = arrayListByDO.get(i).getUid();

                                            db.updateIntoAntaran(
                                                new Antaran(
                                                    uid, idStatus, keteranganUpdate, currentDateTime, latitude, longitude,
                                                    signatureFileName, imageFileName, alamatBaru, catatan, "0"
                                                )
                                            );
                                            j++;

                                            if (j == arrayListByDO.size()) {
                                                alertDialogLoading.dismissDialog();

                                                String message = "Update status item berhasil (pending) dengan jumlah " + arrayListByDO.size() + " item";
                                                Toasty.info(getContext(), message, 3000, false).show();

                                                storeData();
                                                resetAllValue();
                                            }
                                        }
                                    } else {
                                        String errorMessage =
                                                "Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server" +
                                                "\n\nTanggal Perangkat : " + MyDate.setDateTimeIndo(currentDate) + "\nTanggal Server       : " +
                                                MyDate.setDateTimeIndo(dateFromSession);

                                        alertDialogLoading.dismissDialog();
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    }
                                }
                            });
                        } else {
                            final String responseData = responseBody.string();

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        JSONObject jsonObject = new JSONObject(responseData);
                                        boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                        if (isError) {
                                            String errorType = jsonObject.getString(AppConfig.ERROR_TYPE);
                                            String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);

                                            if (errorType.equalsIgnoreCase("alert")) {
                                                alertDialogLoading.dismissDialog();
                                                alertDialogInformation
                                                        .setMessage(errorMessage)
                                                        .showDialog();
                                            } else {
                                                int j = 0;
                                                for (int i=0; i<arrayListByDO.size(); i++) {
                                                    String uid = arrayListByDO.get(i).getUid();

                                                    db.updateIntoAntaran(
                                                        new Antaran(
                                                            uid, idStatus, keteranganUpdate, currentDateTime, latitude,
                                                            longitude, signatureFileName, imageFileName, alamatBaru, catatan,
                                                            "0"
                                                        )
                                                    );
                                                    j++;

                                                    if (j == arrayListByDO.size()) {
                                                        alertDialogLoading.dismissDialog();

                                                        String message = "Update status item berhasil (pending) dengan jumlah " + arrayListByDO.size() + " item";
                                                        Toasty.info(getContext(), message, 3000, false).show();

                                                        storeData();
                                                        resetAllValue();
                                                    }
                                                }
                                            }
                                        } else {
                                            String message = jsonObject.getString(AppConfig.MESSAGE);
                                            JSONArray antaran = jsonObject.getJSONArray("antaran");
                                            int countItem = antaran.length();
                                            int j = 0;

                                            for (int i=0; i<countItem; i++) {
                                                JSONObject object = antaran.getJSONObject(i);
                                                String uid = object.getString(AppConfig.UID);
                                                String signatureFileName = object.getString(AppConfig.TANDA_TANGAN);
                                                String imageFileName = object.getString(AppConfig.FOTO);

                                                db.updateIntoAntaran(
                                                    new Antaran(
                                                        uid, idStatus, keteranganUpdate, currentDateTime, latitude,
                                                        longitude, signatureFileName, imageFileName, alamatBaru, catatan,
                                                        "1"
                                                    )
                                                );
                                                j++;

                                                if (j == countItem) {
                                                    alertDialogLoading.dismissDialog();
                                                    Toasty.success(getContext(), message, 3000, false).show();

                                                    uploadHTTPByDO(getContext(), signatureFileName, imageFileName);
                                                    storeData();
                                                    resetAllValue();
                                                }
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }
            });
        } else {
            final RequestBody formBody = new FormBody.Builder()
                    .add("action", "update")
                    .add(AppConfig.UID, uid)
                    .add(AppConfig.ID_STATUS, idStatus)
                    .add(AppConfig.KETERANGAN, keteranganUpdate)
                    .add(AppConfig.WAKTU_UPDATE, currentDateTime)
                    .add(AppConfig.GARIS_LINTANG, String.valueOf(latitude))
                    .add(AppConfig.GARIS_BUJUR, String.valueOf(longitude))
                    .add(AppConfig.TANDA_TANGAN, signatureFileName)
                    .add(AppConfig.FOTO, imageFileName)
                    .add(AppConfig.ID_ITEM, idItem)
                    .add(AppConfig.ID_DELIVERY_ORDER, idDeliveryOrder)
                    .add(AppConfig.ID_PENGANTAR, idPengantar)
                    .add(AppConfig.ID_KANTOR, idKantor)
                    .add(AppConfig.KODE_DN, kodeDn)
                    .add(AppConfig.TANGGAL_PERANGKAT, currentDate)
                    .add(AppConfig.ALAMAT_BARU, alamatBaru)
                    .add(AppConfig.CATATAN, catatan)
                    .build();

            Request request = new Request.Builder()
                    //.url("https://httpbin.org/delay/10")
                    .url(baseUrl + "antaran.php")
                    .post(formBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentDate.equals(dateFromSession)) {
                                boolean isUpdated = db.updateIntoAntaran(
                                    new Antaran(
                                        uid, idStatus, keteranganUpdate, currentDateTime, latitude, longitude,
                                        signatureFileName, imageFileName, alamatBaru, catatan, "0"
                                    )
                                );

                                if (isUpdated) {
                                    alertDialogLoading.dismissDialog();
                                    Toasty.info(getContext(), getResources().getString(R.string.update_status_pending_success), 3000, false).show();

                                    storeData();
                                    resetAllValue();
                                }
                            } else {
                                String errorMessage =
                                        "Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server" +
                                        "\n\nTanggal Perangkat : " + MyDate.setDateTimeIndo(currentDate) + "\nTanggal Server       : " +
                                        MyDate.setDateTimeIndo(dateFromSession);

                                alertDialogLoading.dismissDialog();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        }
                    });

                    call.cancel();
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try(ResponseBody responseBody = response.body()) {
                        if (!response.isSuccessful()) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (currentDate.equals(dateFromSession)) {
                                        boolean isUpdated = db.updateIntoAntaran(
                                            new Antaran(
                                                uid, idStatus, keteranganUpdate, currentDateTime, latitude, longitude,
                                                 signatureFileName, imageFileName, alamatBaru, catatan, "0"
                                            )
                                        );

                                        if (isUpdated) {
                                            alertDialogLoading.dismissDialog();
                                            Toasty.info(getContext(), getResources().getString(R.string.update_status_pending_success), 3000, false).show();

                                            storeData();
                                            resetAllValue();
                                        }
                                    } else {
                                        String errorMessage =
                                                "Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server" +
                                                "\n\nTanggal Perangkat : " + MyDate.setDateTimeIndo(currentDate) + "\nTanggal Server       : " +
                                                MyDate.setDateTimeIndo(dateFromSession);

                                        alertDialogLoading.dismissDialog();
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    }
                                }
                            });
                        } else {
                            final String responseData = responseBody.string();

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        JSONObject jsonObject = new JSONObject(responseData);
                                        boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                        if (isError) {
                                            String errorType = jsonObject.getString(AppConfig.ERROR_TYPE);
                                            String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);

                                            if (errorType.equalsIgnoreCase("alert")) {
                                                alertDialogLoading.dismissDialog();
                                                alertDialogInformation
                                                        .setMessage(errorMessage)
                                                        .showDialog();
                                            } else {
                                                boolean isUpdated = db.updateIntoAntaran(
                                                    new Antaran(
                                                        uid, idStatus, keteranganUpdate, currentDateTime, latitude,
                                                        longitude, signatureFileName, imageFileName, alamatBaru, catatan,
                                                        "0"
                                                    )
                                                );

                                                if (isUpdated) {
                                                    alertDialogLoading.dismissDialog();
                                                    Toasty.info(getContext(), getResources().getString(R.string.update_status_pending_success), 3000, false).show();

                                                    storeData();
                                                    resetAllValue();
                                                }
                                            }
                                        } else {
                                            boolean isUpdated = db.updateIntoAntaran(
                                                new Antaran(
                                                    uid, idStatus, keteranganUpdate, currentDateTime, latitude, longitude,
                                                    signatureFileName, imageFileName, alamatBaru, catatan, "1"
                                                )
                                            );

                                            if (isUpdated) {
                                                alertDialogLoading.dismissDialog();
                                                Toasty.success(getContext(), getResources().getString(R.string.update_status_success), 3000, false).show();

                                                uploadHTTP();
                                                storeData();
                                                resetAllValue();
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }
            });
        }
    }

    private void resetAllValue() {
        Log.i(TAG, "resetAllValue");
        uid = "";
        idItem = "";
        idStatus ="";
        keterangan ="";
        keteranganBerhasil = "";
        keteranganGagal = "";
        latitude = 0.000000;
        longitude = 0.000000;
        signatureFileName = "";
        imageFileName = "";
        alamatBaru = "";
        catatan = "";
        isBerhasil = false;
        isWithPhoto = false;
        isUpdateByDO = false;
    }

    private void calculateFileSize(String date, double signatureSize, double imageSize) {
        Log.i(TAG, "calculateFileSize");
        int signatureFile = 0;
        if (signatureSize > 0) {
            signatureFile = 1;
        }

        int imageFile = 0;
        if (imageSize > 0) {
            imageFile = 1;
        }

        int totalFile = imageFile + signatureFile;
        double totalSize = signatureSize + imageSize;

        int countRow = db.getCountFileUploadUsageByDate(date);
        if (countRow > 0) {
            String statusUpdate = db.getStatusUpdateFromFileUploadUsage(date);
            if (statusUpdate.equalsIgnoreCase("1")) {
                try {
                    submitFileUploadUsageIntoServer(totalFile, imageFile, signatureFile, totalSize, imageSize, signatureSize, "1");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                ArrayList<FileUploadUsage> arrayList;
                arrayList = db.getAllFileUploadUsageByDate(date);

                int jmlBerkasLama = arrayList.get(0).getJmlBerkas();
                int jmlBerkasFotoLama = arrayList.get(0).getJmlBerkasFoto();
                int jmlBerkasTtdLama = arrayList.get(0).getJmlBerkasTtd();
                double jmlUkuranLama = arrayList.get(0).getJmlUkuran();
                double jmlUkuranFotoLama = arrayList.get(0).getJmlUkuranFoto();
                double jmlUkuranTtdLama = arrayList.get(0).getJmlUkuranTtd();

                int jmlBerkasBaru = jmlBerkasLama + totalFile;
                int jmlBerkasFotoBaru = jmlBerkasFotoLama + imageFile;
                int jmlBerkasTtdBaru = jmlBerkasTtdLama + signatureFile;
                double jmlUkuranBaru = jmlUkuranLama + totalSize;
                double jmlUkuranFotoBaru = jmlUkuranFotoLama + imageSize;
                double jmlUkuranTtdBaru = jmlUkuranTtdLama + signatureSize;

                try {
                    submitFileUploadUsageIntoServer(jmlBerkasBaru, jmlBerkasFotoBaru, jmlBerkasTtdBaru, jmlUkuranBaru, jmlUkuranFotoBaru, jmlUkuranTtdBaru, "0");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                submitFileUploadUsageIntoServer(totalFile, imageFile, signatureFile, totalSize, imageSize, signatureSize, "1");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onFailureSubmitFileUploadUsage(
            String date, String uid, int jmlBerkas, int jmlBerkasFoto, int jmlBerkasTtd, double jmlUkuran,
            double jmlUkuranFoto, double jmlUkuranTtd
    ) {
        Log.i(TAG, "onFailureSubmitFileUploadUsage");
        int countRow = db.getCountFileUploadUsageByDate(date);
        if (countRow > 0) {
            ArrayList<FileUploadUsage> arrayList;
            arrayList = db.getAllFileUploadUsageByDate(date);

            String uidLama = arrayList.get(0).getUid();
            int jmlBerkasLama = arrayList.get(0).getJmlBerkas();
            int jmlBerkasFotoLama = arrayList.get(0).getJmlBerkasFoto();
            int jmlBerkasTtdLama = arrayList.get(0).getJmlBerkasTtd();
            double jmlUkuranLama = arrayList.get(0).getJmlUkuran();
            double jmlUkuranFotoLama = arrayList.get(0).getJmlUkuranFoto();
            double jmlUkuranTtdLama = arrayList.get(0).getJmlUkuranTtd();

            String statusUpdate = db.getStatusUpdateFromFileUploadUsage(date);
            if (statusUpdate.equalsIgnoreCase("1")) {
                int jmlBerkasBaru = jmlBerkasLama + jmlBerkas;
                int jmlBerkasFotoBaru = jmlBerkasFotoLama + jmlBerkasFoto;
                int jmlBerkasTtdBaru = jmlBerkasTtdLama + jmlBerkasTtd;
                double jmlUkuranBaru = jmlUkuranLama + jmlUkuran;
                double jmlUkuranFotoBaru = jmlUkuranFotoLama + jmlUkuranFoto;
                double jmlUkuranTtdBaru = jmlUkuranTtdLama + jmlUkuranTtd;

                db.updateIntoFileUploadUsage(
                    new FileUploadUsage(
                        uidLama, jmlBerkasBaru, jmlBerkasFotoBaru, jmlBerkasTtdBaru, jmlUkuranBaru, jmlUkuranFotoBaru,
                        jmlUkuranTtdBaru, "0"
                    )
                );
            } else {
                db.updateIntoFileUploadUsage(
                    new FileUploadUsage(
                        uidLama, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd,
                        "0"
                    )
                );
            }
        } else {
            db.insertIntoFileUploadUsage(
                new FileUploadUsage(
                    uid, date, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran,
                    jmlUkuranFoto, jmlUkuranTtd, "0", idPengantar
                )
            );
        }
    }

    private void submitFileUploadUsageIntoServer(
            final int jmlBerkas, final int jmlBerkasFoto, final int jmlBerkasTtd, final double jmlUkuran,
            final double jmlUkuranFoto, final double jmlUkuranTtd, String statusUpdate
    ) throws IOException {
        Log.i(TAG, "submitFileUploadUsageIntoServer");
        Date date = new Date(System.currentTimeMillis());
        final String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        final String uid = UUID.randomUUID().toString().replace("-",".").substring(0,20);

        RequestBody formBody = new FormBody.Builder()
                .add("action", "submit")
                .add(AppConfig.UID, uid)
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .add(AppConfig.TANGGAL, currentDate)
                .add(AppConfig.JML_BERKAS, String.valueOf(jmlBerkas))
                .add(AppConfig.JML_BERKAS_FOTO, String.valueOf(jmlBerkasFoto))
                .add(AppConfig.JML_BERKAS_TTD, String.valueOf(jmlBerkasTtd))
                .add(AppConfig.JML_UKURAN, String.valueOf(jmlUkuran))
                .add(AppConfig.JML_UKURAN_FOTO, String.valueOf(jmlUkuranFoto))
                .add(AppConfig.JML_UKURAN_TTD, String.valueOf(jmlUkuranTtd))
                .add(AppConfig.STATUS_UPDATE, statusUpdate)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "file_upload_usage.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFailureSubmitFileUploadUsage(currentDate, uid, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd);

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        //throw new IOException("Unexpected code " + response.code() + " : " + response.message());
                        onFailureSubmitFileUploadUsage(currentDate, uid, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd);
                    } else {
                        final String responseData = responseBody.string();

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (!isError) {
                                        JSONObject fileUploadUsage = jsonObject.getJSONObject("file_upload_usage");
                                        String action = fileUploadUsage.getString("action");
                                        String uidServer = fileUploadUsage.getString(AppConfig.UID);
                                        String tanggalServer = fileUploadUsage.getString(AppConfig.TANGGAL);
                                        int jmlBerkasServer = fileUploadUsage.getInt(AppConfig.JML_BERKAS);
                                        int jmlBerkasFotoServer = fileUploadUsage.getInt(AppConfig.JML_BERKAS_FOTO);
                                        int jmlBerkasTtdServer = fileUploadUsage.getInt(AppConfig.JML_BERKAS_TTD);
                                        double jmlUkuranServer = fileUploadUsage.getDouble(AppConfig.JML_UKURAN);
                                        double jmlUkuranFotoServer = fileUploadUsage.getDouble(AppConfig.JML_UKURAN_FOTO);
                                        double jmlUkuranTtdServer = fileUploadUsage.getDouble(AppConfig.JML_UKURAN_TTD);

                                        if (action.equalsIgnoreCase("insert")) {
                                            db.insertIntoFileUploadUsage(
                                                new FileUploadUsage(
                                                    uidServer, tanggalServer, jmlBerkasServer, jmlBerkasFotoServer, jmlBerkasTtdServer,
                                                    jmlUkuranServer, jmlUkuranFotoServer, jmlUkuranTtdServer, "1", idPengantar
                                                )
                                            );
                                        } else {
                                            db.updateIntoFileUploadUsage(
                                                new FileUploadUsage(
                                                    uidServer, jmlBerkasServer, jmlBerkasFotoServer, jmlBerkasTtdServer, jmlUkuranServer,
                                                    jmlUkuranFotoServer, jmlUkuranTtdServer, "1"
                                                )
                                            );
                                        }
                                    } else {
                                        onFailureSubmitFileUploadUsage(currentDate, uid, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void uploadHTTP() {
        Log.i(TAG, "uploadHTTP");
        Date date = new Date(System.currentTimeMillis());
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        String year = currentDate.substring(0, 4);
        String month = currentDate.substring(5, 7);
        String day = currentDate.substring(8, 10);
        remotePath = "/" + year + month + "/" + day + "/";

        if (isBerhasil) {
            if (isWithPhoto) {
                double signatureSize = (double) signatureFile.length() / 1024;
                double imageSize = (double) compressedImage.length() / 1024;
                calculateFileSize(currentDate, signatureSize, imageSize);

                uploadHTTPSignatureFile(signatureFile);
                uploadHTTPImageFile(compressedImage);
            } else {
                double signatureSize = (double) signatureFile.length() / 1024;
                calculateFileSize(currentDate, signatureSize, 0);

                uploadHTTPSignatureFile(signatureFile);
            }
        } else {
            double imageSize = (double) compressedImage.length() / 1024;
            calculateFileSize(currentDate,0, imageSize);

            uploadHTTPImageFile(compressedImage);
        }
    }

    private void uploadHTTPSignatureFile(File fileName) {
        Log.i(TAG, "uploadHTTPSignatureFile");
        String signatureUploadID = myUploadFile.UploadHTTP(
                getContext(),
                String.valueOf(fileName),
                remotePath
        );

        db.insertIntoFileUploadError(new FileUploadError(signatureUploadID, signatureFileName, remotePath, idPengantar));
    }

    private void uploadHTTPImageFile(File fileName) {
        Log.i(TAG, "uploadHTTPImageFile");
        String imageUploadID = myUploadFile.UploadHTTP(
                getContext(),
                String.valueOf(fileName),
                remotePath
        );

        db.insertIntoFileUploadError(new FileUploadError(imageUploadID, imageFileName, remotePath, idPengantar));
    }

    private void uploadHTTPByDO(Context context, String signatureFileName, String imageFileName) {
        Log.i(TAG, "uploadHTTPByDO");
        Date date = new Date(System.currentTimeMillis());
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        String year = currentDate.substring(0, 4);
        String month = currentDate.substring(5, 7);
        String day = currentDate.substring(8, 10);
        remotePath = "/" + year + month + "/" + day + "/";

        MyFile myFile = new MyFile(context);
        File signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_SIGNATURE), signatureFileName);
        File imageFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_IMAGE), imageFileName);

        if (isBerhasil) {
            if (isWithPhoto) {
                double signatureSize = (double) signatureFile.length() / 1024;
                double imageSize = (double) imageFile.length() / 1024;
                calculateFileSize(currentDate, signatureSize, imageSize);

                uploadHTTPSignatureFile(signatureFile);
                uploadHTTPImageFile(imageFile);
            } else {
                double signatureSize = (double) signatureFile.length() / 1024;
                calculateFileSize(currentDate, signatureSize, 0);

                uploadHTTPSignatureFile(signatureFile);
            }
        } else {
            double imageSize = (double) imageFile.length() / 1024;
            calculateFileSize(currentDate, 0, imageSize);

            uploadHTTPImageFile(imageFile);
        }
    }

    /*private void uploadFTP() {
        Log.i(TAG, "uploadFTP");
        Date currentDate = new Date();
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(currentDate);
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);
        remotePath = "/" + year + month + "/" + day + "/";

        if (isBerhasil) {
            if (isWithPhoto) {
                double signatureSize = (double) signatureFile.length() / 1024;
                double imageSize = (double) compressedImage.length() / 1024;
                calculateFileSize(date, signatureSize, imageSize);

                uploadFTPSignatureFile(signatureFile);
                uploadFTPImageFile(compressedImage);
            } else {
                double signatureSize = (double) signatureFile.length() / 1024;
                calculateFileSize(date, signatureSize, 0);

                uploadFTPSignatureFile(signatureFile);
            }
        } else {
            double imageSize = (double) compressedImage.length() / 1024;
            calculateFileSize(date,0, imageSize);

            uploadFTPImageFile(compressedImage);
        }
    }

    private void uploadFTPSignatureFile(File fileName) {
        Log.i(TAG, "uploadFTPSignatureFile");
        String signatureUploadID = myUploadFile.uploadFTP(
                getContext(),
                AppConfig.FTP_HOST,
                Integer.parseInt(AppConfig.FTP_PORT),
                AppConfig.FTP_USERNAME,
                AppConfig.FTP_PASSWORD,
                String.valueOf(fileName),
                remotePath
        );

        db.insertIntoFileUploadError(new FileUploadError(signatureUploadID, signatureFileName, remotePath, idPengantar));
    }

    private void uploadFTPImageFile(File fileName) {
        Log.i(TAG, "uploadFTPImageFile");
        String imageUploadID = myUploadFile.uploadFTP(
                getContext(),
                AppConfig.FTP_HOST,
                Integer.parseInt(AppConfig.FTP_PORT),
                AppConfig.FTP_USERNAME,
                AppConfig.FTP_PASSWORD,
                String.valueOf(fileName),
                remotePath
        );

        db.insertIntoFileUploadError(new FileUploadError(imageUploadID, imageFileName, remotePath, idPengantar));
    }

    private void uploadFTPByDO(Context context, String signatureFileName, String imageFileName) {
        Log.i(TAG, "uploadFTPByDO");
        Date currentDate = new Date();
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(currentDate);
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);
        remotePath = "/" + year + month + "/" + day + "/";

        MyFile myFile = new MyFile(context);
        File signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_SIGNATURE), signatureFileName);
        File imageFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_IMAGE), imageFileName);

        if (isBerhasil) {
            if (isWithPhoto) {
                double signatureSize = (double) signatureFile.length() / 1024;
                double imageSize = (double) imageFile.length() / 1024;
                calculateFileSize(date, signatureSize, imageSize);

                uploadFTPSignatureFile(signatureFile);
                uploadFTPImageFile(imageFile);
            } else {
                double signatureSize = (double) signatureFile.length() / 1024;
                calculateFileSize(date, signatureSize, 0);

                uploadFTPSignatureFile(signatureFile);
            }
        } else {
            double imageSize = (double) imageFile.length() / 1024;
            calculateFileSize(date, 0, imageSize);

            uploadFTPImageFile(imageFile);
        }
    }*/

    private void showDialogEntryDeliveryOrder() {
        Log.i(TAG, "showDialogEntryDeliveryOrder");
        View view = LayoutInflater.from(getContext()).inflate(R.layout.alert_dialog_entry_delivery_order, null);
        TextViewLatoBold tvTitle = (TextViewLatoBold) view.findViewById(R.id.tv_title);
        tvTitle.setText(getResources().getString(R.string.title_entry_delivery_order));

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setView(view);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertDialog.show();

        etIdDeliveryOrder = (TextInputEditTextLatoRegular) view.findViewById(R.id.et_id_delivery_order);
        etIdDeliveryOrder.addTextChangedListener(new PreventZeroOnFirstType(etIdDeliveryOrder));
        etIdDeliveryOrder.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateIdDeliveryOrder()) {
                        alertDialog.dismiss();
                        alertDialogLoading.setCancelAble(false).showDialog();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogLoading.dismissDialog();
                                showDialogUpdateStatus();
                            }
                        }, 1000);
                    }
                }

                return false;
            }
        });

        ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateIdDeliveryOrder()) {
                    alertDialog.dismiss();
                    alertDialogLoading.setCancelAble(false).showDialog();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            alertDialogLoading.dismissDialog();
                            showDialogUpdateStatus();
                        }
                    }, 1000);
                }
            }
        });
    }

    private boolean validateIdDeliveryOrder() {
        Log.d(TAG, "validateIdDeliveryOrder");
        Date date = new Date(System.currentTimeMillis());
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        idDeliveryOrder = etIdDeliveryOrder.getText().toString().trim();

        if (idDeliveryOrder.isEmpty()) {
            Toasty.error(getContext(), getResources().getString(R.string.id_delivery_order_empty), 3000, false)
                    .show();
            return false;
        } else if (idDeliveryOrder.length() < 16) {
            Toasty.error(getContext(), getResources().getString(R.string.id_delivery_order_min_length), 3000, false)
                    .show();
            return false;
        } else if (!idDeliveryOrder.matches(AppConfig.ID_ITEM_PATTERN)) {
            Toasty.error(getContext(), getResources().getString(R.string.id_delivery_order_invalid_format), 3000, false)
                    .show();
            return false;
        } else {
            String mIdDeliveryOrder = db.getIdDeliveryOrderFromAntaranByDO(idDeliveryOrder);
            if (mIdDeliveryOrder.isEmpty()) {
                Toasty.error(getContext(), getResources().getString(R.string.id_delivery_order_not_found), 3000, false)
                        .show();
                return false;
            } else {
                arrayListByDO.clear();
                arrayListByDO = db.getAllAntaranProsesByDO(currentDate, idDeliveryOrder);

                if (arrayListByDO.size() == 0) {
                    Toasty.error(getContext(), getResources().getString(R.string.id_delivery_order_updated), 3000, false)
                            .show();
                    return false;
                } else {
                    isUpdateByDO = true;
                }
            }
        }

        return true;
    }

    @Override
    public void positiveButtonClickedOnAlertDialogConfirmation(String action) {
        if (action.equalsIgnoreCase("startCameraActivity")) {
            startCameraActivity();
        }
    }

    @Override
    public void negativeButtonClickedOnAlertDialogConfirmation(String action) {
        if (action.equalsIgnoreCase("showDialogValidation")) {
            showDialogValidation();
        }
    }

    @Override
    public void positiveButtonClickedOnAlertDialogInformation(String action) {

    }
}
