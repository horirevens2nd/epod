package horirevens.com.epod.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horirevens.mylibrary.fonts.TextViewLatoBold;

import horirevens.com.epod.R;
import horirevens.com.epod.adapters.MainPagerAdapter;

/**
 * Created by horirevens on 3/21/18.
 */

public class MainFragment extends Fragment {
    private static final String TAG = MainFragment.class.getName();

    private Context context;
    private ViewPager viewPager;
    private MainPagerAdapter mainPagerAdapter;
    private TabLayout tabLayout;
    private TextViewLatoBold tvCountItemOnTab1, tvCountItemOnTab2, tvCountItemOnTab3;

    public MainFragment() {

    }

    public TextViewLatoBold getTvCountItemOnTab1() {
        return tvCountItemOnTab1;
    }
    public void setTvCountItemOnTab(TextViewLatoBold tvCountItemOnTab1) {
        this.tvCountItemOnTab1 = tvCountItemOnTab1;
    }

    public TextViewLatoBold getTvCountItemOnTab2() {
        return tvCountItemOnTab2;
    }
    public void setTvCountItemOnTab2(TextViewLatoBold tvCountItemOnTab2) {
        this.tvCountItemOnTab2 = tvCountItemOnTab2;
    }

    public TextViewLatoBold getTvCountItemOnTab3() {
        return tvCountItemOnTab3;
    }
    public void setTvCountItemOnTab3(TextViewLatoBold tvCountItemOnTab3) {
        this.tvCountItemOnTab3 = tvCountItemOnTab3;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        context = getContext();

        View view = inflater.inflate(R.layout.tablayout_main_fragment, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);

        setupTabText();
        setupViewPager();

        return view;
    }

    private void setupTabText() {
        Log.d(TAG, "setupTabText");
        View view1 = getActivity().getLayoutInflater().inflate(R.layout.tabview_main_fragment, null);
        TextViewLatoBold tvTitleTab1 = (TextViewLatoBold) view1.findViewById(R.id.tv_title);
        tvCountItemOnTab1 = (TextViewLatoBold) view1.findViewById(R.id.tv_count_item);
        setTvCountItemOnTab(tvCountItemOnTab1);
        tvTitleTab1.setText(getResources().getString(R.string.tab_delivery_order));
        tabLayout.addTab(tabLayout.newTab().setCustomView(view1));

        View view2 = getActivity().getLayoutInflater().inflate(R.layout.tabview_main_fragment, null);
        TextViewLatoBold tvTitleTab2 = (TextViewLatoBold) view2.findViewById(R.id.tv_title);
        tvCountItemOnTab2 = (TextViewLatoBold) view2.findViewById(R.id.tv_count_item);
        setTvCountItemOnTab2(tvCountItemOnTab2);
        tvTitleTab2.setText(getResources().getString(R.string.tab_success));
        tabLayout.addTab(tabLayout.newTab().setCustomView(view2));

        View view3 = getActivity().getLayoutInflater().inflate(R.layout.tabview_main_fragment, null);
        TextViewLatoBold tvTitleTab3 = (TextViewLatoBold) view3.findViewById(R.id.tv_title);
        tvCountItemOnTab3 = (TextViewLatoBold) view3.findViewById(R.id.tv_count_item);
        setTvCountItemOnTab3(tvCountItemOnTab3);
        tvTitleTab3.setText(getResources().getString(R.string.tab_failed));
        tabLayout.addTab(tabLayout.newTab().setCustomView(view3));
    }

    private void setupViewPager() {
        Log.d(TAG, "setupViewPager");
        mainPagerAdapter = new MainPagerAdapter(
                //getActivity().getSupportFragmentManager(), tabLayout.getTabCount()
                getChildFragmentManager(), tabLayout.getTabCount()
        );

        //viewPager.setOffscreenPageLimit(6);
        viewPager.setAdapter(mainPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
