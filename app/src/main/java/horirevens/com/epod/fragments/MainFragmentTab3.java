package horirevens.com.epod.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.horirevens.mylibrary.fonts.TextViewLatoBold;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

import es.dmoral.toasty.Toasty;
import horirevens.com.epod.AppConfig;
import horirevens.com.epod.LoginSession;
import horirevens.com.epod.MainApplication;
import horirevens.com.epod.R;
import horirevens.com.epod.activities.MainActivity;
import horirevens.com.epod.adapters.MainFragmentTab3Adapter;
import horirevens.com.epod.helpers.DatabaseHelper;
import horirevens.com.epod.libraries.AlertDialogInformation;
import horirevens.com.epod.libraries.AlertDialogLoading;
import horirevens.com.epod.libraries.MyFile;
import horirevens.com.epod.libraries.MyUploadFile;
import horirevens.com.epod.models.Antaran;
import horirevens.com.epod.models.FileUploadError;
import horirevens.com.epod.models.FileUploadUsage;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by horirevens on 3/21/18.
 */

public class MainFragmentTab3 extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener,
        AlertDialogInformation.AlertDialogInformationListener {
    private static final String TAG = MainFragmentTab3.class.getName();

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ConstraintLayout infoContainer, warningContainer;
    private ArrayList<Antaran> arrayList = new ArrayList<>();
    private ArrayList<Antaran> arrayListPending = new ArrayList<>();
    private DatabaseHelper db;
    private MainFragmentTab3Adapter adapter;
    private OkHttpClient client;
    private AlertDialogInformation alertDialogInformation;
    private AlertDialogLoading alertDialogLoading;

    private String idPengantar, idKantor, baseUrl, server;

    private SearchView searchView;

    public MainFragmentTab3() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        db = new DatabaseHelper(getContext());
        client = MainApplication.getUnsafeOkHttpClient();
        alertDialogInformation = new AlertDialogInformation(getContext(), this);
        alertDialogLoading = new AlertDialogLoading(getContext());

        LoginSession loginSession = new LoginSession(getContext());
        HashMap<String, String> hashMap = loginSession.getSession();
        idPengantar = hashMap.get(AppConfig.ID);
        idKantor = hashMap.get(AppConfig.ID_KANTOR);
        baseUrl = hashMap.get(AppConfig.BASE_URL);
        server = hashMap.get(AppConfig.SERVER);

        View view = inflater.inflate(R.layout.content_main_fragment, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        infoContainer = (ConstraintLayout) view.findViewById(R.id.info_container);
        infoContainer.setVisibility(View.GONE);
        warningContainer = (ConstraintLayout) view.findViewById(R.id.warning_container);
        warningContainer.setVisibility(View.GONE);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setVisibility(View.GONE);

        storeData();

        return view;
    }

    private void storeData() {
        Log.i(TAG, "storeData tab3");
        Date date = new Date(System.currentTimeMillis());
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);

        arrayList.clear();
        arrayList = db.getAllAntaranGagal(currentDate);

        arrayListPending.clear();
        arrayListPending = db.getAllAntaranGagalPending(currentDate);

        adapter = new MainFragmentTab3Adapter(getContext(), arrayList);
        adapter.notifyDataSetChanged();

        if (adapter.getItemCount() > 0) {
            infoContainer.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.setAdapter(adapter);
        } else {
            infoContainer.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        setCountItemOnTab3(currentDate);
    }

    private void setCountItemOnTab3(String currentDate) {
        Log.d(TAG, "setCountItemOnTab3");
        int countAll = adapter.getItemCount();
        int countSync = db.getCountAntaranGagalNonPending(currentDate);

        FragmentManager fragmentManager = getParentFragment().getFragmentManager();
        MainFragment mainFragment = (MainFragment) fragmentManager.findFragmentById(R.id.content_main);
        TextViewLatoBold tvCount = mainFragment.getTvCountItemOnTab3();

        if (countAll > 0) {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(countAll) + " (" + String.valueOf(countSync) + ")");
        } else {
            tvCount.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (arrayListPending.size() == 0) {
                    Toasty.error(getContext(), getResources().getString(R.string.message_no_pending_data), 3000, false).show();
                } else {
                    try {
                        updateAntaranIntoServer();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                swipeRefreshLayout.setRefreshing(false);
            }
        }, 3000);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (this.isVisible()) {
            storeData();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();
        inflater.inflate(R.menu.main_fragment_tab3, menu);
        searchView = (SearchView) menu.findItem(R.id.nav_search).getActionView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_search:
                searchIdItem(menuItem);
                return true;
            case R.id.nav_upload:
                if (arrayListPending.size() == 0) {
                    Toasty.error(getContext(), getResources().getString(R.string.message_no_pending_data), 3000, false).show();
                } else {
                    try {
                        updateAntaranIntoServer();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void searchIdItem(MenuItem menuItem) {
        final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (menuItem != null) {
            MenuItemCompat.setOnActionExpandListener(menuItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem menuItem) {
                    if (searchView != null) {
                        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
                        searchView.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                        searchView.setIconified(false);
                        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                            @Override
                            public boolean onQueryTextSubmit(String query) {
                                return false;
                            }

                            @Override
                            public boolean onQueryTextChange(String newText) {
                                adapter.search(newText);
                                recyclerView.invalidate();
                                return false;
                            }
                        });
                    }
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                    return true;
                }
            });

            searchView = (SearchView) menuItem.getActionView();
        }
    }

    private void updateAntaranIntoServer() throws IOException {
        Log.i(TAG, "updateAntaranIntoServer");
        alertDialogLoading.setCancelAble(false).showDialog();

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();

        try {
            for (Antaran antaran : arrayListPending) {
                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put(AppConfig.UID, antaran.getUid());
                jsonObject2.put(AppConfig.ID_ITEM, antaran.getIdItem());
                jsonObject2.put(AppConfig.ID_DELIVERY_ORDER, antaran.getIdDeliveryOrder());
                jsonObject2.put(AppConfig.ID_STATUS, antaran.getIdStatus());
                jsonObject2.put(AppConfig.KETERANGAN, antaran.getKeterangan());
                jsonObject2.put(AppConfig.WAKTU_UPDATE, antaran.getWaktuUpdate());
                jsonObject2.put(AppConfig.GARIS_LINTANG, antaran.getGarisLintang());
                jsonObject2.put(AppConfig.GARIS_BUJUR, antaran.getGarisBujur());
                jsonObject2.put(AppConfig.TANDA_TANGAN, antaran.getTandaTangan());
                jsonObject2.put(AppConfig.FOTO, antaran.getFoto());
                jsonObject2.put(AppConfig.ID_PENGANTAR, idPengantar);
                jsonObject2.put(AppConfig.ID_KANTOR, idKantor);
                jsonObject2.put(AppConfig.KODE_DN, db.getKodeDnFromStatus(antaran.getIdStatus()));
                jsonObject2.put(AppConfig.ALAMAT_BARU, antaran.getAlamatBaru());
                jsonObject2.put(AppConfig.CATATAN, antaran.getCatatan());

                jsonArray.put(jsonObject2);
            }

            jsonObject.put("action", "update_collective");
            jsonObject.put("data", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(AppConfig.MEDIA_TYPE_JSON, jsonObject.toString());

        Request request = new Request.Builder()
                .url(baseUrl + "antaran_collective_pending.php")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(final Call call, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogLoading.dismissDialog();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(final ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogLoading.dismissDialog();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (isError) {
                                        alertDialogLoading.dismissDialog();

                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    } else {
                                        MyFile myFile = new MyFile(getContext());
                                        String message = jsonObject.getString(AppConfig.MESSAGE);
                                        JSONArray antaran = jsonObject.getJSONArray("antaran");
                                        int j = 0;
                                        int totalImageFile = 0;
                                        int totalSignatureFile = 0;
                                        double totalImageSize = 0;
                                        double totalSignatureSize = 0;

                                        for (int i=0; i<antaran.length(); i++) {
                                            JSONObject object = antaran.getJSONObject(i);
                                            String uid = object.getString(AppConfig.UID);
                                            String signatureFileName = object.getString(AppConfig.TANDA_TANGAN);
                                            String imageFileName = object.getString(AppConfig.FOTO);

                                            if (!signatureFileName.isEmpty()) {
                                                File signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(getContext(), AppConfig.FOLDER_SIGNATURE), signatureFileName);
                                                double signatureSize = (double) signatureFile.length() / 1024;
                                                totalSignatureSize = totalSignatureSize + signatureSize;
                                                totalSignatureFile = totalSignatureFile + 1;
                                            }

                                            if (!imageFileName.isEmpty()) {
                                                File imageFile = new File(myFile.getFolderPicturesOnStorageDirectory(getContext(), AppConfig.FOLDER_IMAGE), imageFileName);
                                                double imageSize = (double) imageFile.length() / 1024;
                                                totalImageSize = totalImageSize + imageSize;
                                                totalImageFile = totalImageFile + 1;
                                            }

                                            boolean isUpdated = db.updateStatusUpdateIntoAntaran(uid);

                                            if (isUpdated) {
                                                uploadHTTP(getContext(), signatureFileName, imageFileName);

                                                j++;
                                                if (j == antaran.length()) {
                                                    alertDialogLoading.dismissDialog();
                                                    Toasty.success(getContext(), message, 3000, false).show();
                                                    storeData();

                                                    int totalFile = totalImageFile + totalSignatureFile;
                                                    double totalSize = totalImageSize + totalSignatureSize;
                                                    calculateFileSize(totalFile, totalImageFile, totalSignatureFile, totalSize, totalImageSize, totalSignatureSize);
                                                }
                                            }
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void calculateFileSize(
            int totalFile, int totalImageFile, int totalSignatureFile, double totalSize, double totalImageSize, double totalSignatureSize
    ) {
        Log.i(TAG, "calculateFileSize");
        Date date = new Date(System.currentTimeMillis());
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);

        int countRow = db.getCountFileUploadUsageByDate(currentDate);
        if (countRow > 0) {
            String statusUpdate = db.getStatusUpdateFromFileUploadUsage(currentDate);
            if (statusUpdate.equalsIgnoreCase("1")) {
                try {
                    submitFileUploadUsageIntoServer(totalFile, totalImageFile, totalSignatureFile, totalSize, totalImageSize, totalSignatureSize, "1");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                ArrayList<FileUploadUsage> arrayList;
                arrayList = db.getAllFileUploadUsageByDate(currentDate);

                int jmlBerkasLama = arrayList.get(0).getJmlBerkas();
                int jmlBerkasFotoLama = arrayList.get(0).getJmlBerkasFoto();
                int jmlBerkasTtdLama = arrayList.get(0).getJmlBerkasTtd();
                double jmlUkuranLama = arrayList.get(0).getJmlUkuran();
                double jmlUkuranFotoLama = arrayList.get(0).getJmlUkuranFoto();
                double jmlUkuranTtdLama = arrayList.get(0).getJmlUkuranTtd();

                int jmlBerkasBaru = jmlBerkasLama + totalFile;
                int jmlBerkasFotoBaru = jmlBerkasFotoLama + totalImageFile;
                int jmlBerkasTtdBaru = jmlBerkasTtdLama + totalSignatureFile;
                double jmlUkuranBaru = jmlUkuranLama + totalSize;
                double jmlUkuranFotoBaru = jmlUkuranFotoLama + totalImageSize;
                double jmlUkuranTtdBaru = jmlUkuranTtdLama + totalSignatureSize;

                try {
                    submitFileUploadUsageIntoServer(jmlBerkasBaru, jmlBerkasFotoBaru, jmlBerkasTtdBaru, jmlUkuranBaru, jmlUkuranFotoBaru, jmlUkuranTtdBaru, "0");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                submitFileUploadUsageIntoServer(totalFile, totalImageFile, totalSignatureFile, totalSize, totalImageSize, totalSignatureSize, "1");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onFailureSubmitFileUploadUsage(
            String date, String uid, int jmlBerkas, int jmlBerkasFoto, int jmlBerkasTtd, double jmlUkuran,
            double jmlUkuranFoto, double jmlUkuranTtd
    ) {
        Log.i(TAG, "onFailureSubmitFileUploadUsage");
        int countRow = db.getCountFileUploadUsageByDate(date);
        if (countRow > 0) {
            ArrayList<FileUploadUsage> arrayList;
            arrayList = db.getAllFileUploadUsageByDate(date);

            String uidLama = arrayList.get(0).getUid();
            int jmlBerkasLama = arrayList.get(0).getJmlBerkas();
            int jmlBerkasFotoLama = arrayList.get(0).getJmlBerkasFoto();
            int jmlBerkasTtdLama = arrayList.get(0).getJmlBerkasTtd();
            double jmlUkuranLama = arrayList.get(0).getJmlUkuran();
            double jmlUkuranFotoLama = arrayList.get(0).getJmlUkuranFoto();
            double jmlUkuranTtdLama = arrayList.get(0).getJmlUkuranTtd();

            String statusUpdate = db.getStatusUpdateFromFileUploadUsage(date);
            if (statusUpdate.equalsIgnoreCase("1")) {
                int jmlBerkasBaru = jmlBerkasLama + jmlBerkas;
                int jmlBerkasFotoBaru = jmlBerkasFotoLama + jmlBerkasFoto;
                int jmlBerkasTtdBaru = jmlBerkasTtdLama + jmlBerkasTtd;
                double jmlUkuranBaru = jmlUkuranLama + jmlUkuran;
                double jmlUkuranFotoBaru = jmlUkuranFotoLama + jmlUkuranFoto;
                double jmlUkuranTtdBaru = jmlUkuranTtdLama + jmlUkuranTtd;

                db.updateIntoFileUploadUsage(
                    new FileUploadUsage(
                        uidLama, jmlBerkasBaru, jmlBerkasFotoBaru, jmlBerkasTtdBaru, jmlUkuranBaru, jmlUkuranFotoBaru,
                        jmlUkuranTtdBaru, "0"
                    )
                );
            } else {
                db.updateIntoFileUploadUsage(
                    new FileUploadUsage(
                        uidLama, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd,
                        "0"
                    )
                );
            }
        } else {
            db.insertIntoFileUploadUsage(
                new FileUploadUsage(
                    uid, date, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran,
                    jmlUkuranFoto, jmlUkuranTtd, "0", idPengantar
                )
            );
        }
    }

    private void submitFileUploadUsageIntoServer(
            final int jmlBerkas, final int jmlBerkasFoto, final int jmlBerkasTtd, final double jmlUkuran, final double jmlUkuranFoto,
            final double jmlUkuranTtd, String statusUpdate
    ) throws IOException {
        Log.i(TAG, "submitFileUploadUsageIntoServer");
        Date date = new Date(System.currentTimeMillis());
        final String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        final String uid = UUID.randomUUID().toString().replace("-",".").substring(0,20);

        RequestBody formBody = new FormBody.Builder()
                .add("action", "submit")
                .add(AppConfig.UID, uid)
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .add(AppConfig.TANGGAL, currentDate)
                .add(AppConfig.JML_BERKAS, String.valueOf(jmlBerkas))
                .add(AppConfig.JML_BERKAS_FOTO, String.valueOf(jmlBerkasFoto))
                .add(AppConfig.JML_BERKAS_TTD, String.valueOf(jmlBerkasTtd))
                .add(AppConfig.JML_UKURAN, String.valueOf(jmlUkuran))
                .add(AppConfig.JML_UKURAN_FOTO, String.valueOf(jmlUkuranFoto))
                .add(AppConfig.JML_UKURAN_TTD, String.valueOf(jmlUkuranTtd))
                .add(AppConfig.STATUS_UPDATE, statusUpdate)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "file_upload_usage.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFailureSubmitFileUploadUsage(currentDate, uid, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd);

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        //throw new IOException("Unexpected code " + response.code() + " : " + response.message());
                        onFailureSubmitFileUploadUsage(currentDate, uid, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd);
                    } else {
                        final String responseData = responseBody.string();

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (!isError) {
                                        JSONObject fileUploadUsage = jsonObject.getJSONObject("file_upload_usage");
                                        String action = fileUploadUsage.getString("action");
                                        String uidServer = fileUploadUsage.getString(AppConfig.UID);
                                        String tanggalServer = fileUploadUsage.getString(AppConfig.TANGGAL);
                                        int jmlBerkasServer = fileUploadUsage.getInt(AppConfig.JML_BERKAS);
                                        int jmlBerkasFotoServer = fileUploadUsage.getInt(AppConfig.JML_BERKAS_FOTO);
                                        int jmlBerkasTtdServer = fileUploadUsage.getInt(AppConfig.JML_BERKAS_TTD);
                                        double jmlUkuranServer = fileUploadUsage.getDouble(AppConfig.JML_UKURAN);
                                        double jmlUkuranFotoServer = fileUploadUsage.getDouble(AppConfig.JML_UKURAN_FOTO);
                                        double jmlUkuranTtdServer = fileUploadUsage.getDouble(AppConfig.JML_UKURAN_TTD);

                                        if (action.equalsIgnoreCase("insert")) {
                                            db.insertIntoFileUploadUsage(
                                                new FileUploadUsage(
                                                    uidServer, tanggalServer, jmlBerkasServer, jmlBerkasFotoServer, jmlBerkasTtdServer,
                                                    jmlUkuranServer, jmlUkuranFotoServer, jmlUkuranTtdServer, "1", idPengantar
                                                )
                                            );
                                        } else {
                                            db.updateIntoFileUploadUsage(
                                                new FileUploadUsage(
                                                    uidServer, jmlBerkasServer, jmlBerkasFotoServer, jmlBerkasTtdServer, jmlUkuranServer,
                                                    jmlUkuranFotoServer, jmlUkuranTtdServer, "1"
                                                )
                                            );
                                        }
                                    } else {
                                        onFailureSubmitFileUploadUsage(currentDate, uid, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    /*private void uploadFTP(Context context, String signatureFileName, String imageFileName) {
        Log.i(TAG, "uploadFTP");
        MyUploadFile myUploadFile = new MyUploadFile(context);
        MyFile myFile = new MyFile(context);
        File signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_SIGNATURE), signatureFileName);
        File imageFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_IMAGE), imageFileName);

        Date currentDate = new Date();
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(currentDate);
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);
        String remotePath = "/" + year + month + "/" + day + "/";

        if (!signatureFileName.isEmpty()) {
            String signatureUploadID = myUploadFile.uploadFTP(
                    context,
                    AppConfig.FTP_HOST,
                    Integer.parseInt(AppConfig.FTP_PORT),
                    AppConfig.FTP_USERNAME,
                    AppConfig.FTP_PASSWORD,
                    String.valueOf(signatureFile),
                    remotePath
            );

            db.insertIntoFileUploadError(new FileUploadError(signatureUploadID, signatureFileName, remotePath, idPengantar));
        }

        if (!imageFileName.isEmpty()) {
            String imageUploadID = myUploadFile.uploadFTP(
                    context,
                    AppConfig.FTP_HOST,
                    Integer.parseInt(AppConfig.FTP_PORT),
                    AppConfig.FTP_USERNAME,
                    AppConfig.FTP_PASSWORD,
                    String.valueOf(imageFile),
                    remotePath
            );

            db.insertIntoFileUploadError(new FileUploadError(imageUploadID, imageFileName, remotePath, idPengantar));
        }
    }*/

    private void uploadHTTP(Context context, String signatureFileName, String imageFileName) {
        Log.i(TAG, "uploadFTP");
        MyUploadFile myUploadFile = new MyUploadFile(context);
        MyFile myFile = new MyFile(context);

        Date date = new Date(System.currentTimeMillis());
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        String year = currentDate.substring(0, 4);
        String month = currentDate.substring(5, 7);
        String day = currentDate.substring(8, 10);
        String remotePath = "/" + year + month + "/" + day + "/";

        if (!signatureFileName.isEmpty()) {
            File signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_SIGNATURE), signatureFileName);
            String signatureUploadID = myUploadFile.UploadHTTP(
                    getContext(),
                    String.valueOf(signatureFile),
                    remotePath
            );

            db.insertIntoFileUploadError(new FileUploadError(signatureUploadID, signatureFileName, remotePath, idPengantar));
        }

        if (!imageFileName.isEmpty()) {
            File imageFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_IMAGE), imageFileName);
            String imageUploadID = myUploadFile.UploadHTTP(
                    getContext(),
                    String.valueOf(imageFile),
                    remotePath
            );

            db.insertIntoFileUploadError(new FileUploadError(imageUploadID, imageFileName, remotePath, idPengantar));
        }
    }

    @Override
    public void positiveButtonClickedOnAlertDialogInformation(String action) {

    }
}
