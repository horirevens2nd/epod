package horirevens.com.epod;

import android.app.Application;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;

import net.gotev.uploadservice.UploadService;

import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import es.dmoral.toasty.Toasty;
import horirevens.com.epod.libraries.CustomFont;
import horirevens.com.epod.receivers.NetworkReceiver;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;

/**
 * Created by horirevens on 3/4/18.
 */

public class MainApplication extends Application {

    private static MainApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        AllCertificatesAndHostsTruster.apply();
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;

        CustomFont.setDefaultFont(this, "MONOSPACE", "fonts/lato_regular.ttf");

        Toasty.Config.getInstance()
                .setErrorColor(ContextCompat.getColor(this, R.color.red))
                .setInfoColor(ContextCompat.getColor(this, R.color.gray))
                .setSuccessColor(ContextCompat.getColor(this, R.color.green))
                .setWarningColor(ContextCompat.getColor(this, R.color.yellow))
                .setTextColor(ContextCompat.getColor(this, R.color.white))
                .setToastTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/lato_regular.ttf"))
                .setTextSize(12)
                .apply();
    }

    public static synchronized MainApplication getInstance() {
        return instance;
    }

    public void setNetworkListener(NetworkReceiver.NetworkReceiverListener listener) {
        NetworkReceiver.networkReceiverListener = listener;
    }

    public static OkHttpClient getUnsafeOkHttpClient() {
        try {
            final TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
            };

            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            builder.callTimeout(15, TimeUnit.SECONDS);
            builder.connectTimeout(15, TimeUnit.SECONDS);
            builder.readTimeout(15, TimeUnit.SECONDS);
            builder.writeTimeout(15, TimeUnit.SECONDS);
            builder.retryOnConnectionFailure(false);

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
