package horirevens.com.epod.helpers;

import android.content.Context;
import android.content.Intent;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import horirevens.com.epod.activities.CrashReportActivity;


/**
 * Created by horirevens on 3/8/18.
 */

public class ExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {

    private Context context;

    public ExceptionHandler(Context context) {
        this.context = context;
    }

    public void uncaughtException(Thread t, Throwable e) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);

        /*result.append("\n*** INFORMASI PERANGKAT ***\n");
        result.append("Brand: ");
        result.append(Build.BRAND);
        result.append("\n");
        result.append("Device: ");
        result.append(Build.DEVICE);
        result.append("\n");
        result.append("Model: ");
        result.append(Build.MODEL);
        result.append("\n");
        result.append("Id: ");
        result.append(Build.ID);
        result.append("\n");
        result.append("Product: ");
        result.append(Build.PRODUCT);
        result.append("\n");*/

        Intent intent = new Intent(context, CrashReportActivity.class);
        intent.putExtra("crashMessage", result.toString());
        context.startActivity(intent);

        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(10);
    }

}
