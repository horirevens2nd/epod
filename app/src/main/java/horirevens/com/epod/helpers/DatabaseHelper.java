package horirevens.com.epod.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import horirevens.com.epod.AppConfig;
import horirevens.com.epod.models.Antaran;
import horirevens.com.epod.models.FileUploadError;
import horirevens.com.epod.models.FileUploadUsage;
import horirevens.com.epod.models.ItemDeliveryOrder;
import horirevens.com.epod.models.Status;

/**
 * Created by horirevens on 3/19/18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getName();

    private static final int DB_VERSION = 18;
    private static final String DB_NAME = "epod";
    private static final String TBL_ANTARAN = "antaran";
    private static final String TBL_STATUS = "status";
    private static final String TBL_FILE_UPLOAD_USAGE = "file_upload_usage";
    private static final String TBL_FILE_UPLOAD_ERROR = "file_upload_error";

    private static final String CREATE_TABLE_ANTARAN =
            "CREATE TABLE " + TBL_ANTARAN + " ("
            + AppConfig.UID + " TEXT NOT NULL PRIMARY KEY, "
            + AppConfig.ID_ITEM + " TEXT NOT NULL, "
            + AppConfig.ID_DELIVERY_ORDER + " TEXT NOT NULL, "
            + AppConfig.ID_STATUS +   " TEXT NOT NULL, "
            + AppConfig.KETERANGAN + " TEXT NULL, "
            + AppConfig.WAKTU_ENTRI + " TEXT NOT NULL, "
            + AppConfig.WAKTU_UPDATE + " TEXT NULL, "
            + AppConfig.GARIS_LINTANG + " REAL NULL, "
            + AppConfig.GARIS_BUJUR + " REAL NULL, "
            + AppConfig.TANDA_TANGAN + " TEXT NULL, "
            + AppConfig.FOTO + " TEXT NULL, "
            + AppConfig.NAMA_PENGIRIM + " TEXT NULL, "
            + AppConfig.ALAMAT_PENGIRIM + " TEXT NULL, "
            + AppConfig.HP_PENGIRIM + " TEXT NULL, "
            + AppConfig.NAMA_PENERIMA + " TEXT NULL, "
            + AppConfig.ALAMAT_PENERIMA + " TEXT NULL, "
            + AppConfig.HP_PENERIMA + " TEXT NULL, "
            + AppConfig.ID_PRODUK + " TEXT NULL, "
            + AppConfig.BERAT + " REAL NULL, "
            + AppConfig.BSU_COD + " REAL NULL, "
            + AppConfig.BSU_BLB + " REAL NULL, "
            + AppConfig.BSU_TAX + " REAL NULL, "
            + AppConfig.KANTOR_ASAL + " TEXT NULL, "
            + AppConfig.KANTOR_TUJUAN + " TEXT NULL, "
            + AppConfig.ID_CUSTOMER + " TEXT NULL, "
            + AppConfig.ID_EXTERNAL + " TEXT NULL, "
            + AppConfig.TGL_POSTING + " TEXT NULL, "
            + AppConfig.TIPE + " TEXT NULL, "
            + AppConfig.ALAMAT_BARU + " TEXT NULL, "
            + AppConfig.CATATAN + " TEXT NULL, "
            + AppConfig.STATUS_TUTUP + " TEXT NOT NULL, "
            + AppConfig.STATUS_UPDATE + " TEXT NOT NULL, "
            + AppConfig.ID_PENGANTAR + " TEXT NOT NULL, "
            + " UNIQUE (" + AppConfig.ID_ITEM + "," + AppConfig.ID_DELIVERY_ORDER + "))";

    private static final String CREATE_TABLE_STATUS =
            "CREATE TABLE " + TBL_STATUS + "("
            + AppConfig.ID + " TEXT NOT NULL PRIMARY KEY, "
            + AppConfig.KETERANGAN + " TEXT NOT NULL, "
            + AppConfig.TINDAKAN + " TEXT NOT NULL, "
            + AppConfig.KODE_LN + " TEXT NOT NULL, "
            + AppConfig.KODE_DN + " TEXT NOT NULL, "
            + AppConfig.URUTAN + " TEXT NOT NULL, "
            + AppConfig.LEVEL + " TEXT NOT NULL, "
            + AppConfig.TERAKHIR + " TEXT NOT NULL, "
            + AppConfig.ID_STATUS_INDUK + " TEXT NOT NULL)";

    private static final String CREATE_TABLE_FILE_UPLOAD_USAGE =
            "CREATE TABLE " + TBL_FILE_UPLOAD_USAGE + "("
            + AppConfig.UID + " TEXT NOT NULL PRIMARY KEY, "
            + AppConfig.TANGGAL + " TEXT NOT NULL, "
            + AppConfig.JML_BERKAS + " INTEGER NOT NULL, "
            + AppConfig.JML_BERKAS_FOTO + " INTEGER NOT NULL, "
            + AppConfig.JML_BERKAS_TTD + " INTEGER NOT NULL, "
            + AppConfig.JML_UKURAN + " REAL NOT NULL, "
            + AppConfig.JML_UKURAN_FOTO + " REAL NOT NULL, "
            + AppConfig.JML_UKURAN_TTD + " REAL NOT NULL, "
            + AppConfig.STATUS_UPDATE + " TEXT NOT NULL, "
            + AppConfig.ID_PENGANTAR + " TEXT NOT NULL)";

    private static final String CREATE_TABLE_FILE_UPLOAD_ERROR =
            "CREATE TABLE " + TBL_FILE_UPLOAD_ERROR + "("
            + AppConfig.UID + " TEXT NOT NULL PRIMARY KEY, "
            + AppConfig.NAMA_FILE + " TEXT NOT NULL, "
            + AppConfig.DIREKTORI + " TEXT NOT NULL, "
            + AppConfig.ID_PENGANTAR + " TEXT NOT NULL)";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ANTARAN);
        db.execSQL(CREATE_TABLE_STATUS);
        db.execSQL(CREATE_TABLE_FILE_UPLOAD_USAGE);
        db.execSQL(CREATE_TABLE_FILE_UPLOAD_ERROR);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TBL_ANTARAN);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_STATUS);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_FILE_UPLOAD_USAGE);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_FILE_UPLOAD_ERROR);

        onCreate(db);
    }

    public boolean insertIntoFileUploadError(FileUploadError fileUploadError) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AppConfig.UID, fileUploadError.getUid());
        values.put(AppConfig.NAMA_FILE, fileUploadError.getNamaFile());
        values.put(AppConfig.DIREKTORI, fileUploadError.getDirektori());
        values.put(AppConfig.ID_PENGANTAR, fileUploadError.getIdPengantar());

        if (db.insert(TBL_FILE_UPLOAD_ERROR, null, values) > 0) {
            result = true;
        }

        return result;
    }

    public boolean insertIntoFileUploadUsage(FileUploadUsage fileUploadUsage) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AppConfig.UID, fileUploadUsage.getUid());
        values.put(AppConfig.TANGGAL, fileUploadUsage.getTanggal());
        values.put(AppConfig.JML_BERKAS, fileUploadUsage.getJmlBerkas());
        values.put(AppConfig.JML_BERKAS_FOTO, fileUploadUsage.getJmlBerkasFoto());
        values.put(AppConfig.JML_BERKAS_TTD, fileUploadUsage.getJmlBerkasTtd());
        values.put(AppConfig.JML_UKURAN, fileUploadUsage.getJmlUkuran());
        values.put(AppConfig.JML_UKURAN_FOTO, fileUploadUsage.getJmlUkuranFoto());
        values.put(AppConfig.JML_UKURAN_TTD, fileUploadUsage.getJmlUkuranTtd());
        values.put(AppConfig.STATUS_UPDATE, fileUploadUsage.getStatusUpdate());
        values.put(AppConfig.ID_PENGANTAR, fileUploadUsage.getIdPengantar());

        if (db.insert(TBL_FILE_UPLOAD_USAGE, null, values) > 0) {
            result = true;
        }

        return result;
    }

    public boolean insertIntoStatus(Status status) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AppConfig.ID, status.getId());
        values.put(AppConfig.KETERANGAN, status.getKeterangan());
        values.put(AppConfig.TINDAKAN, status.getTindakan());
        values.put(AppConfig.KODE_LN, status.getKodeLn());
        values.put(AppConfig.KODE_DN, status.getKodeDn());
        values.put(AppConfig.URUTAN, status.getUrutan());
        values.put(AppConfig.LEVEL, status.getLevel());
        values.put(AppConfig.TERAKHIR, status.getTerakhir());
        values.put(AppConfig.ID_STATUS_INDUK, status.getIdStatusInduk());

        if (db.insert(TBL_STATUS, null, values) > 0) {
            result = true;
        }

        return result;
    }

    public boolean insertIntoAntaran(Antaran antaran) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AppConfig.UID, antaran.getUid());
        values.put(AppConfig.ID_ITEM, antaran.getIdItem());
        values.put(AppConfig.ID_DELIVERY_ORDER, antaran.getIdDeliveryOrder());
        values.put(AppConfig.ID_STATUS, antaran.getIdStatus());
        values.put(AppConfig.KETERANGAN, antaran.getKeterangan());
        values.put(AppConfig.WAKTU_ENTRI, antaran.getWaktuEntri());
        values.put(AppConfig.WAKTU_UPDATE, antaran.getWaktuUpdate());
        values.put(AppConfig.GARIS_LINTANG, antaran.getGarisLintang());
        values.put(AppConfig.GARIS_BUJUR, antaran.getGarisBujur());
        values.put(AppConfig.TANDA_TANGAN, antaran.getTandaTangan());
        values.put(AppConfig.FOTO, antaran.getFoto());
        values.put(AppConfig.NAMA_PENGIRIM, antaran.getNamaPengirim());
        values.put(AppConfig.ALAMAT_PENGIRIM, antaran.getAlamatPengirim());
        values.put(AppConfig.HP_PENGIRIM, antaran.getHpPengirim());
        values.put(AppConfig.NAMA_PENERIMA, antaran.getNamaPenerima());
        values.put(AppConfig.ALAMAT_PENERIMA, antaran.getAlamatPenerima());
        values.put(AppConfig.HP_PENERIMA, antaran.getHpPenerima());
        values.put(AppConfig.ID_PRODUK, antaran.getIdProduk());
        values.put(AppConfig.BERAT, antaran.getBerat());
        values.put(AppConfig.BSU_COD, antaran.getBsuCod());
        values.put(AppConfig.BSU_BLB, antaran.getBsuBlb());
        values.put(AppConfig.BSU_TAX, antaran.getBsuTax());
        values.put(AppConfig.KANTOR_ASAL, antaran.getKantorAsal());
        values.put(AppConfig.KANTOR_TUJUAN, antaran.getKantorTujuan());
        values.put(AppConfig.ID_CUSTOMER, antaran.getIdCustomer());
        values.put(AppConfig.ID_EXTERNAL, antaran.getIdExternal());
        values.put(AppConfig.TGL_POSTING, antaran.getTglPosting());
        values.put(AppConfig.TIPE, antaran.getTipe());
        values.put(AppConfig.ALAMAT_BARU, antaran.getAlamatBaru());
        values.put(AppConfig.CATATAN, antaran.getCatatan());
        values.put(AppConfig.STATUS_TUTUP, antaran.getStatusTutup());
        values.put(AppConfig.STATUS_UPDATE, antaran.getStatusUpdate());
        values.put(AppConfig.ID_PENGANTAR, antaran.getIdPengantar());

        if (db.insert(TBL_ANTARAN, null, values) > 0) {
            result = true;
        }

        return result;
    }

    public boolean updateUidIntoFileUploadError(String newUid, String oldUid) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = AppConfig.UID + "=?";
        String[] whereArgs = {oldUid};

        ContentValues values = new ContentValues();
        values.put(AppConfig.UID, newUid);

        if (db.update(TBL_FILE_UPLOAD_ERROR, values, whereClause, whereArgs) > 0) {
            result = true;
        }

        return result;
    }

    public int updateStatusTutupIntoAntaran(String idDeliveryOrder) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = AppConfig.ID_DELIVERY_ORDER + "=?";
        String[] whereArgs = {idDeliveryOrder};

        ContentValues values = new ContentValues();
        values.put(AppConfig.STATUS_TUTUP, "1");

        return db.update(TBL_ANTARAN, values, whereClause, whereArgs);

    }

    public boolean updateStatusUpdateIntoAntaran(String uid) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = AppConfig.UID + "=?";
        String[] whereArgs = {uid};

        ContentValues values = new ContentValues();
        values.put(AppConfig.STATUS_UPDATE, "1");

        if (db.update(TBL_ANTARAN, values, whereClause, whereArgs) > 0) {
            result = true;
        }

        return result;
    }

    public boolean updateIntoFileUploadUsage(FileUploadUsage fileUploadUsage) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = AppConfig.UID + "=?";
        String[] whereArgs = {fileUploadUsage.getUid()};

        ContentValues values = new ContentValues();
        values.put(AppConfig.JML_BERKAS, fileUploadUsage.getJmlBerkas());
        values.put(AppConfig.JML_BERKAS_FOTO, fileUploadUsage.getJmlBerkasFoto());
        values.put(AppConfig.JML_BERKAS_TTD, fileUploadUsage.getJmlBerkasTtd());
        values.put(AppConfig.JML_UKURAN, fileUploadUsage.getJmlUkuran());
        values.put(AppConfig.JML_UKURAN_FOTO, fileUploadUsage.getJmlUkuranFoto());
        values.put(AppConfig.JML_UKURAN_TTD, fileUploadUsage.getJmlUkuranTtd());
        values.put(AppConfig.STATUS_UPDATE, fileUploadUsage.getStatusUpdate());

        if (db.update(TBL_FILE_UPLOAD_USAGE, values, whereClause, whereArgs) > 0) {
            result = true;
        }

        return result;
    }

    public boolean updateIntoStatus(Status status) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = AppConfig.ID + "=?";
        String[] whereArgs = {status.getId()};

        ContentValues values = new ContentValues();
        values.put(AppConfig.KETERANGAN, status.getKeterangan());
        values.put(AppConfig.TINDAKAN, status.getTindakan());
        values.put(AppConfig.KODE_LN, status.getKodeLn());
        values.put(AppConfig.KODE_DN, status.getKodeDn());
        values.put(AppConfig.URUTAN, status.getUrutan());
        values.put(AppConfig.LEVEL, status.getLevel());
        values.put(AppConfig.TERAKHIR, status.getTerakhir());
        values.put(AppConfig.ID_STATUS_INDUK, status.getIdStatusInduk());

        if (db.update(TBL_STATUS, values, whereClause, whereArgs) > 0) {
            result = true;
        }

        return result;
    }

    public boolean updateIntoAntaran(Antaran antaran) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = AppConfig.UID + "=?";
        String[] whereArgs = {antaran.getUid()};

        ContentValues values = new ContentValues();
        values.put(AppConfig.ID_STATUS, antaran.getIdStatus());
        values.put(AppConfig.KETERANGAN, antaran.getKeterangan());
        values.put(AppConfig.WAKTU_UPDATE, antaran.getWaktuUpdate());
        values.put(AppConfig.GARIS_LINTANG, antaran.getGarisLintang());
        values.put(AppConfig.GARIS_BUJUR, antaran.getGarisBujur());
        values.put(AppConfig.TANDA_TANGAN, antaran.getTandaTangan());
        values.put(AppConfig.FOTO, antaran.getFoto());
        values.put(AppConfig.ALAMAT_BARU, antaran.getAlamatBaru());
        values.put(AppConfig.CATATAN, antaran.getCatatan());
        values.put(AppConfig.STATUS_UPDATE, antaran.getStatusUpdate());

        if (db.update(TBL_ANTARAN, values, whereClause, whereArgs) > 0) {
            result = true;
        }

        return result;
    }

    /*public boolean updateDetailAlamatIntoAntaran(Antaran antaran) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = AppConfig.UID + "=?";
        String[] whereArgs = {antaran.getUid()};

        ContentValues values = new ContentValues();
        values.put(AppConfig.ID_PRODUK, antaran.getIdProduk());
        values.put(AppConfig.ID_PENERIMA, antaran.getIdPenerima());
        values.put(AppConfig.AL_PENERIMA, antaran.getAlPenerima());
        values.put(AppConfig.ID_PENGIRIM, antaran.getIdPengirim());
        values.put(AppConfig.SWP, antaran.getSwp());

        if (db.update(TBL_ANTARAN, values, whereClause, whereArgs) > 0) {
            result = true;
        }

        return result;
    }*/

    public ArrayList<Status> getAllStatusBerhasil() {
        ArrayList<Status> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_STATUS
                + " WHERE " + AppConfig.ID + " LIKE 'B%'"
                + " AND " + AppConfig.LEVEL + " = '1'"
                + " ORDER BY " + AppConfig.URUTAN + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Status status = new Status();
                status.setId(cursor.getString(0));
                status.setKeterangan(cursor.getString(1));
                status.setTindakan(cursor.getString(2));
                status.setKodeLn(cursor.getString(3));
                status.setKodeDn(cursor.getString(4));
                status.setUrutan(cursor.getString(5));
                status.setLevel(cursor.getString(6));
                status.setTerakhir(cursor.getString(7));
                status.setIdStatusInduk(cursor.getString(8));
                arrayList.add(status);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<Status> getAllStatusGagal() {
        ArrayList<Status> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_STATUS
                + " WHERE " + AppConfig.ID + " LIKE 'G%'"
                + " AND " + AppConfig.LEVEL + " = '1'"
                + " ORDER BY " + AppConfig.URUTAN + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Status status = new Status();
                status.setId(cursor.getString(0));
                status.setKeterangan(cursor.getString(1));
                status.setTindakan(cursor.getString(2));
                status.setKodeLn(cursor.getString(3));
                status.setKodeDn(cursor.getString(4));
                status.setUrutan(cursor.getString(5));
                status.setLevel(cursor.getString(6));
                status.setTerakhir(cursor.getString(7));
                status.setIdStatusInduk(cursor.getString(8));
                arrayList.add(status);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<Status> getAllStatusGagalLevel2(String id) {
        ArrayList<Status> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_STATUS
                + " WHERE " + AppConfig.ID_STATUS_INDUK + " = '" + id + "'"
                + " ORDER BY " + AppConfig.URUTAN + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Status status = new Status();
                status.setId(cursor.getString(0));
                status.setKeterangan(cursor.getString(1));
                status.setTindakan(cursor.getString(2));
                status.setKodeLn(cursor.getString(3));
                status.setKodeDn(cursor.getString(4));
                status.setUrutan(cursor.getString(5));
                status.setLevel(cursor.getString(6));
                status.setTerakhir(cursor.getString(7));
                status.setIdStatusInduk(cursor.getString(8));
                arrayList.add(status);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<FileUploadUsage> getAllFileUploadUsage(String currentDate) {
        String year = currentDate.substring(0, 4);
        ArrayList<FileUploadUsage> arrayList = new ArrayList<>();
        String query =
                "SELECT "
                + AppConfig.TANGGAL + ","
                + " SUM(" + AppConfig.JML_BERKAS + "),"
                + " SUM(" + AppConfig.JML_BERKAS_FOTO + "),"
                + " SUM(" + AppConfig.JML_BERKAS_TTD + "),"
                + " ROUND(SUM(" + AppConfig.JML_UKURAN + "), 2),"
                + " ROUND(SUM(" + AppConfig.JML_UKURAN_FOTO + "), 2),"
                + " ROUND(SUM(" + AppConfig.JML_UKURAN_TTD + "), 2)"
                + " FROM " + TBL_FILE_UPLOAD_USAGE
                + " WHERE " + AppConfig.TANGGAL + " LIKE '" + year + "%'"
                + " GROUP BY SUBSTR(" + AppConfig.TANGGAL + ", 6, 2) "
                + " ORDER BY SUBSTR(" + AppConfig.TANGGAL + ", 6, 2) ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                FileUploadUsage fileUploadUsage = new FileUploadUsage();
                fileUploadUsage.setTanggal(cursor.getString(0));
                fileUploadUsage.setJmlBerkas(cursor.getInt(1));
                fileUploadUsage.setJmlBerkasFoto(cursor.getInt(2));
                fileUploadUsage.setJmlBerkasTtd(cursor.getInt(3));
                fileUploadUsage.setJmlUkuran(cursor.getDouble(4));
                fileUploadUsage.setJmlUkuranFoto(cursor.getDouble(5));
                fileUploadUsage.setJmlUkuranTtd(cursor.getDouble(6));
                arrayList.add(fileUploadUsage);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<FileUploadUsage> getAllFileUploadUsageByDate(String currentDate) {
        ArrayList<FileUploadUsage> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_FILE_UPLOAD_USAGE
                + " WHERE " + AppConfig.TANGGAL + "='" + currentDate + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                FileUploadUsage fileUploadUsage = new FileUploadUsage();
                fileUploadUsage.setUid(cursor.getString(0));
                fileUploadUsage.setTanggal(cursor.getString(1));
                fileUploadUsage.setJmlBerkas(cursor.getInt(2));
                fileUploadUsage.setJmlBerkasFoto(cursor.getInt(3));
                fileUploadUsage.setJmlBerkasTtd(cursor.getInt(4));
                fileUploadUsage.setJmlUkuran(cursor.getDouble(5));
                fileUploadUsage.setJmlUkuranFoto(cursor.getDouble(6));
                fileUploadUsage.setJmlUkuranTtd(cursor.getDouble(7));
                fileUploadUsage.setStatusUpdate(cursor.getString(8));
                arrayList.add(fileUploadUsage);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<FileUploadError> getAllFileUploadError() {
        ArrayList<FileUploadError> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_FILE_UPLOAD_ERROR;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                FileUploadError fileUploadError = new FileUploadError();
                fileUploadError.setUid(cursor.getString(0));
                fileUploadError.setNamaFile(cursor.getString(1));
                fileUploadError.setDirektori(cursor.getString(2));
                arrayList.add(fileUploadError);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<Antaran> getAllAntaranProses(String currentDate) {
        ArrayList<Antaran> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_ENTRI + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " = 'P01'"
                + " AND " + AppConfig.STATUS_TUTUP + " = '1'"
                + " ORDER BY " + AppConfig.WAKTU_ENTRI + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Antaran antaran = new Antaran();
                antaran.setUid(cursor.getString(0));
                antaran.setIdItem(cursor.getString(1));
                antaran.setIdDeliveryOrder(cursor.getString(2));
                antaran.setIdStatus(cursor.getString(3));
                antaran.setKeterangan(cursor.getString(4));
                antaran.setWaktuEntri(cursor.getString(5));
                antaran.setWaktuUpdate(cursor.getString(6));
                antaran.setGarisLintang(Double.parseDouble(cursor.getString(7)));
                antaran.setGarisBujur(Double.parseDouble(cursor.getString(8)));
                antaran.setTandaTangan(cursor.getString(9));
                antaran.setFoto(cursor.getString(10));
                antaran.setNamaPengirim(cursor.getString(11));
                antaran.setAlamatPengirim(cursor.getString(12));
                antaran.setHpPengirim(cursor.getString(13));
                antaran.setNamaPenerima(cursor.getString(14));
                antaran.setAlamatPenerima(cursor.getString(15));
                antaran.setHpPenerima(cursor.getString(16));
                antaran.setIdProduk(cursor.getString(17));
                antaran.setBerat(cursor.getDouble(18));
                antaran.setBsuCod(cursor.getDouble(19));
                antaran.setBsuBlb(cursor.getDouble(20));
                antaran.setBsuTax(cursor.getDouble(21));
                antaran.setKantorAsal(cursor.getString(22));
                antaran.setKantorTujuan(cursor.getString(23));
                antaran.setIdCustomer(cursor.getString(24));
                antaran.setIdExternal(cursor.getString(25));
                antaran.setTglPosting(cursor.getString(26));
                antaran.setTipe(cursor.getString(27));
                antaran.setAlamatBaru(cursor.getString(28));
                antaran.setCatatan(cursor.getString(29));
                antaran.setStatusTutup(cursor.getString(30));
                antaran.setStatusUpdate(cursor.getString(31));
                arrayList.add(antaran);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<Antaran> getAllAntaranProsesByDO(String currentDate, String idDeliveryOrder) {
        ArrayList<Antaran> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_ENTRI + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " = 'P01'"
                + " AND " + AppConfig.STATUS_TUTUP + " = '1'"
                + " AND " + AppConfig.ID_DELIVERY_ORDER + " = '" + idDeliveryOrder + "'"
                + " ORDER BY " + AppConfig.WAKTU_ENTRI + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Antaran antaran = new Antaran();
                antaran.setUid(cursor.getString(0));
                antaran.setIdItem(cursor.getString(1));
                antaran.setIdDeliveryOrder(cursor.getString(2));
                antaran.setIdStatus(cursor.getString(3));
                antaran.setKeterangan(cursor.getString(4));
                antaran.setWaktuEntri(cursor.getString(5));
                antaran.setWaktuUpdate(cursor.getString(6));
                antaran.setGarisLintang(Double.parseDouble(cursor.getString(7)));
                antaran.setGarisBujur(Double.parseDouble(cursor.getString(8)));
                antaran.setTandaTangan(cursor.getString(9));
                antaran.setFoto(cursor.getString(10));
                antaran.setNamaPengirim(cursor.getString(11));
                antaran.setAlamatPengirim(cursor.getString(12));
                antaran.setHpPengirim(cursor.getString(13));
                antaran.setNamaPenerima(cursor.getString(14));
                antaran.setAlamatPenerima(cursor.getString(15));
                antaran.setHpPenerima(cursor.getString(16));
                antaran.setIdProduk(cursor.getString(17));
                antaran.setBerat(cursor.getDouble(18));
                antaran.setBsuCod(cursor.getDouble(19));
                antaran.setBsuBlb(cursor.getDouble(20));
                antaran.setBsuTax(cursor.getDouble(21));
                antaran.setKantorAsal(cursor.getString(22));
                antaran.setKantorTujuan(cursor.getString(23));
                antaran.setIdCustomer(cursor.getString(24));
                antaran.setIdExternal(cursor.getString(25));
                antaran.setTglPosting(cursor.getString(26));
                antaran.setTipe(cursor.getString(27));
                antaran.setAlamatBaru(cursor.getString(28));
                antaran.setCatatan(cursor.getString(29));
                antaran.setStatusTutup(cursor.getString(30));
                antaran.setStatusUpdate(cursor.getString(31));
                arrayList.add(antaran);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<Antaran> getAllAntaranBerhasil(String currentDate) {
        ArrayList<Antaran> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_UPDATE + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " LIKE 'B%'"
                + " ORDER BY " + AppConfig.WAKTU_UPDATE + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Antaran antaran = new Antaran();
                antaran.setUid(cursor.getString(0));
                antaran.setIdItem(cursor.getString(1));
                antaran.setIdDeliveryOrder(cursor.getString(2));
                antaran.setIdStatus(cursor.getString(3));
                antaran.setKeterangan(cursor.getString(4));
                antaran.setWaktuEntri(cursor.getString(5));
                antaran.setWaktuUpdate(cursor.getString(6));
                antaran.setGarisLintang(Double.parseDouble(cursor.getString(7)));
                antaran.setGarisBujur(Double.parseDouble(cursor.getString(8)));
                antaran.setTandaTangan(cursor.getString(9));
                antaran.setFoto(cursor.getString(10));
                antaran.setNamaPengirim(cursor.getString(11));
                antaran.setAlamatPengirim(cursor.getString(12));
                antaran.setHpPengirim(cursor.getString(13));
                antaran.setNamaPenerima(cursor.getString(14));
                antaran.setAlamatPenerima(cursor.getString(15));
                antaran.setHpPenerima(cursor.getString(16));
                antaran.setIdProduk(cursor.getString(17));
                antaran.setBerat(cursor.getDouble(18));
                antaran.setBsuCod(cursor.getDouble(19));
                antaran.setBsuBlb(cursor.getDouble(20));
                antaran.setBsuTax(cursor.getDouble(21));
                antaran.setKantorAsal(cursor.getString(22));
                antaran.setKantorTujuan(cursor.getString(23));
                antaran.setIdCustomer(cursor.getString(24));
                antaran.setIdExternal(cursor.getString(25));
                antaran.setTglPosting(cursor.getString(26));
                antaran.setTipe(cursor.getString(27));
                antaran.setAlamatBaru(cursor.getString(28));
                antaran.setCatatan(cursor.getString(29));
                antaran.setStatusTutup(cursor.getString(30));
                antaran.setStatusUpdate(cursor.getString(31));
                arrayList.add(antaran);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<Antaran> getAllAntaranBerhasilPending(String currentDate) {
        ArrayList<Antaran> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_UPDATE + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " LIKE 'B%'"
                + " AND " + AppConfig.STATUS_UPDATE + " = '0'"
                + " ORDER BY " + AppConfig.WAKTU_UPDATE + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Antaran antaran = new Antaran();
                antaran.setUid(cursor.getString(0));
                antaran.setIdItem(cursor.getString(1));
                antaran.setIdDeliveryOrder(cursor.getString(2));
                antaran.setIdStatus(cursor.getString(3));
                antaran.setKeterangan(cursor.getString(4));
                antaran.setWaktuEntri(cursor.getString(5));
                antaran.setWaktuUpdate(cursor.getString(6));
                antaran.setGarisLintang(Double.parseDouble(cursor.getString(7)));
                antaran.setGarisBujur(Double.parseDouble(cursor.getString(8)));
                antaran.setTandaTangan(cursor.getString(9));
                antaran.setFoto(cursor.getString(10));
                antaran.setNamaPengirim(cursor.getString(11));
                antaran.setAlamatPengirim(cursor.getString(12));
                antaran.setHpPengirim(cursor.getString(13));
                antaran.setNamaPenerima(cursor.getString(14));
                antaran.setAlamatPenerima(cursor.getString(15));
                antaran.setHpPenerima(cursor.getString(16));
                antaran.setIdProduk(cursor.getString(17));
                antaran.setBerat(cursor.getDouble(18));
                antaran.setBsuCod(cursor.getDouble(19));
                antaran.setBsuBlb(cursor.getDouble(20));
                antaran.setBsuTax(cursor.getDouble(21));
                antaran.setKantorAsal(cursor.getString(22));
                antaran.setKantorTujuan(cursor.getString(23));
                antaran.setIdCustomer(cursor.getString(24));
                antaran.setIdExternal(cursor.getString(25));
                antaran.setTglPosting(cursor.getString(26));
                antaran.setTipe(cursor.getString(27));
                antaran.setAlamatBaru(cursor.getString(28));
                antaran.setCatatan(cursor.getString(29));
                antaran.setStatusTutup(cursor.getString(30));
                antaran.setStatusUpdate(cursor.getString(31));
                arrayList.add(antaran);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<Antaran> getAllAntaranGagal(String currentDate) {
        ArrayList<Antaran> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_UPDATE + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " LIKE 'G%'"
                + " ORDER BY " + AppConfig.WAKTU_UPDATE + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Antaran antaran = new Antaran();
                antaran.setUid(cursor.getString(0));
                antaran.setIdItem(cursor.getString(1));
                antaran.setIdDeliveryOrder(cursor.getString(2));
                antaran.setIdStatus(cursor.getString(3));
                antaran.setKeterangan(cursor.getString(4));
                antaran.setWaktuEntri(cursor.getString(5));
                antaran.setWaktuUpdate(cursor.getString(6));
                antaran.setGarisLintang(Double.parseDouble(cursor.getString(7)));
                antaran.setGarisBujur(Double.parseDouble(cursor.getString(8)));
                antaran.setTandaTangan(cursor.getString(9));
                antaran.setFoto(cursor.getString(10));
                antaran.setNamaPengirim(cursor.getString(11));
                antaran.setAlamatPengirim(cursor.getString(12));
                antaran.setHpPengirim(cursor.getString(13));
                antaran.setNamaPenerima(cursor.getString(14));
                antaran.setAlamatPenerima(cursor.getString(15));
                antaran.setHpPenerima(cursor.getString(16));
                antaran.setIdProduk(cursor.getString(17));
                antaran.setBerat(cursor.getDouble(18));
                antaran.setBsuCod(cursor.getDouble(19));
                antaran.setBsuBlb(cursor.getDouble(20));
                antaran.setBsuTax(cursor.getDouble(21));
                antaran.setKantorAsal(cursor.getString(22));
                antaran.setKantorTujuan(cursor.getString(23));
                antaran.setIdCustomer(cursor.getString(24));
                antaran.setIdExternal(cursor.getString(25));
                antaran.setTglPosting(cursor.getString(26));
                antaran.setTipe(cursor.getString(27));
                antaran.setAlamatBaru(cursor.getString(28));
                antaran.setCatatan(cursor.getString(29));
                antaran.setStatusTutup(cursor.getString(30));
                antaran.setStatusUpdate(cursor.getString(31));
                arrayList.add(antaran);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<Antaran> getAllAntaranGagalPending(String currentDate) {
        ArrayList<Antaran> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_UPDATE + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " LIKE 'G%'"
                + " AND " + AppConfig.STATUS_UPDATE + " = '0'"
                + " ORDER BY " + AppConfig.WAKTU_UPDATE + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Antaran antaran = new Antaran();
                antaran.setUid(cursor.getString(0));
                antaran.setIdItem(cursor.getString(1));
                antaran.setIdDeliveryOrder(cursor.getString(2));
                antaran.setIdStatus(cursor.getString(3));
                antaran.setKeterangan(cursor.getString(4));
                antaran.setWaktuEntri(cursor.getString(5));
                antaran.setWaktuUpdate(cursor.getString(6));
                antaran.setGarisLintang(Double.parseDouble(cursor.getString(7)));
                antaran.setGarisBujur(Double.parseDouble(cursor.getString(8)));
                antaran.setTandaTangan(cursor.getString(9));
                antaran.setFoto(cursor.getString(10));
                antaran.setNamaPengirim(cursor.getString(11));
                antaran.setAlamatPengirim(cursor.getString(12));
                antaran.setHpPengirim(cursor.getString(13));
                antaran.setNamaPenerima(cursor.getString(14));
                antaran.setAlamatPenerima(cursor.getString(15));
                antaran.setHpPenerima(cursor.getString(16));
                antaran.setIdProduk(cursor.getString(17));
                antaran.setBerat(cursor.getDouble(18));
                antaran.setBsuCod(cursor.getDouble(19));
                antaran.setBsuBlb(cursor.getDouble(20));
                antaran.setBsuTax(cursor.getDouble(21));
                antaran.setKantorAsal(cursor.getString(22));
                antaran.setKantorTujuan(cursor.getString(23));
                antaran.setIdCustomer(cursor.getString(24));
                antaran.setIdExternal(cursor.getString(25));
                antaran.setTglPosting(cursor.getString(26));
                antaran.setTipe(cursor.getString(27));
                antaran.setAlamatBaru(cursor.getString(28));
                antaran.setCatatan(cursor.getString(29));
                antaran.setStatusTutup(cursor.getString(30));
                antaran.setStatusUpdate(cursor.getString(31));
                arrayList.add(antaran);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<Antaran> getAllAntaranByIdItem(String idItem) {
        ArrayList<Antaran> arrayList = new ArrayList<>();
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.ID_ITEM + "='" + idItem + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Antaran antaran = new Antaran();
                antaran.setUid(cursor.getString(0));
                antaran.setIdItem(cursor.getString(1));
                antaran.setIdDeliveryOrder(cursor.getString(2));
                antaran.setIdStatus(cursor.getString(3));
                antaran.setKeterangan(cursor.getString(4));
                antaran.setWaktuEntri(cursor.getString(5));
                antaran.setWaktuUpdate(cursor.getString(6));
                antaran.setGarisLintang(Double.parseDouble(cursor.getString(7)));
                antaran.setGarisBujur(Double.parseDouble(cursor.getString(8)));
                antaran.setTandaTangan(cursor.getString(9));
                antaran.setFoto(cursor.getString(10));
                antaran.setNamaPengirim(cursor.getString(11));
                antaran.setAlamatPengirim(cursor.getString(12));
                antaran.setHpPengirim(cursor.getString(13));
                antaran.setNamaPenerima(cursor.getString(14));
                antaran.setAlamatPenerima(cursor.getString(15));
                antaran.setHpPenerima(cursor.getString(16));
                antaran.setIdProduk(cursor.getString(17));
                antaran.setBerat(cursor.getDouble(18));
                antaran.setBsuCod(cursor.getDouble(19));
                antaran.setBsuBlb(cursor.getDouble(20));
                antaran.setBsuTax(cursor.getDouble(21));
                antaran.setKantorAsal(cursor.getString(22));
                antaran.setKantorTujuan(cursor.getString(23));
                antaran.setIdCustomer(cursor.getString(24));
                antaran.setIdExternal(cursor.getString(25));
                antaran.setTglPosting(cursor.getString(26));
                antaran.setTipe(cursor.getString(27));
                antaran.setAlamatBaru(cursor.getString(28));
                antaran.setCatatan(cursor.getString(29));
                antaran.setStatusTutup(cursor.getString(30));
                antaran.setStatusUpdate(cursor.getString(31));
                arrayList.add(antaran);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public ArrayList<ItemDeliveryOrder> getAllAntaranDeliveryOrder(String idDeliveryOrder) {
        ArrayList<ItemDeliveryOrder> arrayList = new ArrayList<>();
        String query =
                "SELECT " + AppConfig.UID + ", " + AppConfig.ID_ITEM + " FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.ID_DELIVERY_ORDER + " = '" + idDeliveryOrder + "'"
                + " AND " + AppConfig.STATUS_TUTUP + " = '0'"
                + " ORDER BY " + AppConfig.WAKTU_ENTRI + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                ItemDeliveryOrder itemDeliveryOrder = new ItemDeliveryOrder();
                itemDeliveryOrder.setUid(cursor.getString(0));
                itemDeliveryOrder.setIdItem(cursor.getString(1));
                arrayList.add(itemDeliveryOrder);
            } while (cursor.moveToNext());
        }

        return arrayList;
    }

    public int getCountAntaran(String currentDate) {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_ENTRI + " LIKE '" + currentDate + "%'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountStatus() {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_STATUS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountFileUploadError() {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_FILE_UPLOAD_ERROR;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountFileUploadUsage() {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_FILE_UPLOAD_USAGE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountFileUploadUsageByDate(String date) {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_FILE_UPLOAD_USAGE
                + " WHERE " + AppConfig.TANGGAL + "='" + date + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountAntaranDeliveryOrder(String idDeliveryOrder) {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.ID_DELIVERY_ORDER + " = '" + idDeliveryOrder + "'"
                + " AND " + AppConfig.STATUS_TUTUP + " = '0'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountAntaranUnclosed(String currentDate) {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_ENTRI + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.STATUS_TUTUP + " = '0'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountAntaranProses(String currentDate) {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_ENTRI + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " = 'P01'"
                + " AND " + AppConfig.STATUS_TUTUP + " = '1'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountAntaranPendingNonProses(String currentDate) {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_UPDATE + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " != 'P01'"
                + " AND " + AppConfig.STATUS_UPDATE + " = '0'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountAntaranBerhasil(String currentDate) {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_UPDATE + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " LIKE 'B%'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountAntaranBerhasilNonPending(String currentDate) {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_UPDATE + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " LIKE 'B%'"
                + " AND " + AppConfig.STATUS_UPDATE + " = '1'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountAntaranGagal(String currentDate) {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_UPDATE + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " LIKE 'G%'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public int getCountAntaranGagalNonPending(String currentDate) {
        int count = 0;
        String query =
                "SELECT * FROM " + TBL_ANTARAN
                + " WHERE " + AppConfig.WAKTU_UPDATE + " LIKE '" + currentDate + "%'"
                + " AND " + AppConfig.ID_STATUS + " LIKE 'G%'"
                + " AND " + AppConfig.STATUS_UPDATE + " = '1'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getCount();
        }

        return count;
    }

    public String getKeteranganFromStatus(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = null;
        String[] projection = {AppConfig.KETERANGAN};
        String selection = AppConfig.ID + " =?";
        String[] selectionArgs = {id};
        String sortOrder = AppConfig.ID + " ASC";

        Cursor cursor = db.query(TBL_STATUS, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getIdStatusFromAntaran(String idItem) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.ID_STATUS};
        String selection = AppConfig.ID_ITEM + "=?";
        String[] selectionArgs = {idItem};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getNamaPenerimaFromAntaran(String idItem) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.NAMA_PENERIMA};
        String selection = AppConfig.ID_ITEM + "=?";
        String[] selectionArgs = {idItem};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getAlamatPenerimaFromAntaran(String idItem) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.ALAMAT_PENERIMA};
        String selection = AppConfig.ID_ITEM + "=?";
        String[] selectionArgs = {idItem};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getIdProdukFromAntaran(String uid) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.ID_PRODUK};
        String selection = AppConfig.UID+ "=?";
        String[] selectionArgs = {uid};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getIdDeliveryOrderFromAntaran(String idItem) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.ID_DELIVERY_ORDER};
        String selection = AppConfig.ID_ITEM + "=?";
        String[] selectionArgs = {idItem};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getIdDeliveryOrderFromAntaranByDO(String idDeliveryOrder) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.ID_DELIVERY_ORDER};
        String selection = AppConfig.ID_DELIVERY_ORDER + "=?";
        String[] selectionArgs = {idDeliveryOrder};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getIdPengantarFromAntaran() {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.ID_PENGANTAR};
        String selection = "";
        String[] selectionArgs = {};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getIdItemFromAntaran(String idItem, String idDeliveryOrder) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.ID_ITEM};
        String selection = AppConfig.ID_ITEM + "=? AND " + AppConfig.ID_DELIVERY_ORDER + "=?";
        String[] selectionArgs = {idItem, idDeliveryOrder};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getUidFromAntaran(String idItem, String idDeliveryOrder) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.UID};
        String selection = AppConfig.ID_ITEM + "=? AND " + AppConfig.ID_DELIVERY_ORDER + "=?";
        String[] selectionArgs = {idItem, idDeliveryOrder};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getStatusUpdateFromAntaran(String idItem, String idDeliveryOrder) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.STATUS_UPDATE};
        String selection = AppConfig.ID_ITEM + "=? AND " + AppConfig.ID_DELIVERY_ORDER + "=?";
        String[] selectionArgs = {idItem, idDeliveryOrder};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getNamaFileFromFileUploadError(String uid) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.NAMA_FILE};
        String selection = AppConfig.UID + "=?";
        String[] selectionArgs = {uid};
        String sortOrder = AppConfig.UID + "ASC";

        Cursor cursor = db.query(TBL_FILE_UPLOAD_ERROR, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getDirektoriFromFileUploadError(String uid) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.DIREKTORI};
        String selection = AppConfig.UID + "=?";
        String[] selectionArgs = {uid};
        String sortOrder = AppConfig.UID + "ASC";

        Cursor cursor = db.query(TBL_FILE_UPLOAD_ERROR, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getKodeDnFromStatus(String idStatus) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.KODE_DN};
        String selection = AppConfig.ID + "=?";
        String[] selectionArgs = {idStatus};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_STATUS, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public String getStatusUpdateFromFileUploadUsage(String tanggal) {
        SQLiteDatabase db = this.getReadableDatabase();

        String result = "";
        String[] projection = {AppConfig.STATUS_UPDATE};
        String selection = AppConfig.TANGGAL + "=?";
        String[] selectionArgs = {tanggal};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_FILE_UPLOAD_USAGE, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        return result;
    }

    public boolean isExistOnAntaran(String idItem, String idDeliveryOrder) {
        SQLiteDatabase db = this.getReadableDatabase();

        boolean result = false;
        String[] projection = {AppConfig.ID_ITEM, AppConfig.ID_DELIVERY_ORDER};
        String selection = AppConfig.ID_ITEM + "=? AND " + AppConfig.ID_DELIVERY_ORDER + "=?";
        String[] selectionArgs = {idItem, idDeliveryOrder};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_ANTARAN, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = true;
        }

        return result;
    }

    public boolean isExistOnStatus(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        boolean result = false;
        String[] projection = {AppConfig.ID};
        String selection = AppConfig.ID + "=?";
        String[] selectionArgs = {id};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_STATUS, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = true;
        }

        return result;
    }

    public boolean isExistOnFileUploadUsage(String tanggal) {
        SQLiteDatabase db = this.getReadableDatabase();

        boolean result = false;
        String[] projection = {AppConfig.TANGGAL};
        String selection = AppConfig.TANGGAL + "=?";
        String[] selectionArgs = {tanggal};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_FILE_UPLOAD_USAGE, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = true;
        }

        return result;
    }

    public boolean isExistOnFileUploadError(String namaFile) {
        SQLiteDatabase db = this.getReadableDatabase();

        boolean result = false;
        String[] projection = {AppConfig.NAMA_FILE};
        String selection = AppConfig.NAMA_FILE + "=?";
        String[] selectionArgs = {namaFile};
        String sortOrder = AppConfig.ID + "ASC";

        Cursor cursor = db.query(TBL_FILE_UPLOAD_ERROR, projection, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            result = true;
        }

        return result;
    }

    public boolean deleteAntaranByUid(String uid) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        if (db.delete(TBL_ANTARAN, AppConfig.UID + " =?", new String[] {uid}) > 0) {
            result = true;
        }

        return result;
    }

    public boolean deleteAntaranByIdDeliveryOrder(String idDeliveryOrder) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        if (db.delete(TBL_ANTARAN, AppConfig.ID_DELIVERY_ORDER + " =?", new String[] {idDeliveryOrder}) > 0) {
            result = true;
        }

        return result;
    }

    public boolean deleteFileUploadErrorByUid(String uid) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        if (db.delete(TBL_FILE_UPLOAD_ERROR, AppConfig.UID + " =?", new String[] {uid}) > 0) {
            result = true;
        }

        return result;
    }

    public boolean deleteFileUploadErrorByNamaFile(String namaFile) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        if (db.delete(TBL_FILE_UPLOAD_ERROR, AppConfig.NAMA_FILE + " =?", new String[] {namaFile}) > 0) {
            result = true;
        }

        return result;
    }

    public boolean truncate(String tableName) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        if (db.delete(tableName, null, null) > 0) {
            result = true;
        }

        return result;
    }
}
