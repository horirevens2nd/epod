package horirevens.com.epod;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class DateSession {
    private static final String TAG = DateSession.class.getName();
    private static final String PREF_NAME = "DatePreferences";

    private Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public DateSession(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setSession(String date) {
        editor.putString("date", date);
        editor.commit();
    }

    public HashMap<String, String> getSession() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("date", preferences.getString("date", null));

        return hashMap;
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }
}
