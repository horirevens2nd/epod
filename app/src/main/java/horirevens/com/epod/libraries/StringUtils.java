package horirevens.com.epod.libraries;

public class StringUtils {

    public static String uppercaseSetences(String str) {
        char ch[] = str.toCharArray();
        for (int i = 0; i < str.length(); i++) {

            if (i == 0 && ch[i] != ' ' ||
                    ch[i] != ' ' && ch[i - 1] == ' ') {
                if (ch[i] >= 'a' && ch[i] <= 'z') {
                    ch[i] = (char)(ch[i] - 'a' + 'A');
                }
            } else if (ch[i] >= 'A' && ch[i] <= 'Z') {
                ch[i] = (char) (ch[i] + 'a' - 'A');
            }
        }

        String st = new String(ch);
        return st;
    }
    
    public static String getProductName(String productId) {
        String result = null;
        
        if (productId.equalsIgnoreCase("210")) {
            result = "Surat Kilat Khusus";
        } else if (productId.equalsIgnoreCase("240")) {
            result = "Paket Kilat Khusus";
        } else if (productId.equalsIgnoreCase("230")) {
            result = "Paket Pos Biasa";
        } else if (productId.equalsIgnoreCase("417")) {
            result = "Surat Express";
        } else if (productId.equalsIgnoreCase("447")) {
            result = "Paket Express";
        } else if (productId.equalsIgnoreCase("PJE")) {
            result = "Paket Jumbo Ekonomi";
        } else if (productId.equalsIgnoreCase("000")) {
            result = "Not Found";
        } else {
            result = productId.toUpperCase();
        }
        
        return result;
    }
}
