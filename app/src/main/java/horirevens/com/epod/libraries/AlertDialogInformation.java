package horirevens.com.epod.libraries;

import android.app.AlertDialog;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;

import com.horirevens.mylibrary.fonts.ButtonLatoBold;
import com.horirevens.mylibrary.fonts.TextViewLatoBold;
import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import horirevens.com.epod.R;

/**
 * Created by horirevens on 5/14/17.
 */

public class AlertDialogInformation extends AlertDialog {

    private Context context;
    private AlertDialog alertDialog;

    private String title, message, actionPositive, titlePositive;
    
    private AlertDialogInformationListener listener;

    public AlertDialogInformation(Context context, AlertDialogInformationListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    public AlertDialogInformation setTitle(String title) {
        this.title = title;
        return this;
    }
    
    public AlertDialogInformation setMessage(String message) {
        this.message = message;
        return this;
    }
    
    public AlertDialogInformation setActionPositive(String actionPositive) {
        this.actionPositive = actionPositive;
        return this;
    }
    
    public AlertDialogInformation setTitleOnPositiveButton(String titlePositive) {
        this.titlePositive = titlePositive;
        return this;
    }

    public AlertDialogInformation showDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.alert_dialog_default, null);
        ConstraintLayout headerContainer = (ConstraintLayout) view.findViewById(R.id.header_container);
        TextViewLatoBold tvTitle = (TextViewLatoBold) view.findViewById(R.id.tv_title);
        TextViewLatoRegular tvMessage = (TextViewLatoRegular) view.findViewById(R.id.tv_message);
        ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        ButtonLatoBold btnNegative = (ButtonLatoBold) view.findViewById(R.id.btn_negative);
        btnNegative.setVisibility(View.GONE);

        if (title == null) {
            headerContainer.setVisibility(View.GONE);
        }

        if (message == null) {
            message = "";
        }

        if (actionPositive == null) {
            actionPositive = "";
        }

        if (titlePositive == null) {
            titlePositive = context.getResources().getString(R.string.button_ok);
        }

        tvTitle.setText(title);
        tvMessage.setText(message);
        btnPositive.setText(titlePositive);

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                listener.positiveButtonClickedOnAlertDialogInformation(actionPositive);
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        
        return this;
    }

    public interface AlertDialogInformationListener {

        void positiveButtonClickedOnAlertDialogInformation(String action);

    }

}
