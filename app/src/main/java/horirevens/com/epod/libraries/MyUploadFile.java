package horirevens.com.epod.libraries;

import android.content.Context;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.ftp.FTPUploadRequest;

import java.io.FileNotFoundException;

import es.dmoral.toasty.Toasty;
import horirevens.com.epod.AppConfig;

public class MyUploadFile {

    private static final String UPLOAD_URL = "https://epod.posindonesia.co.id:4433/upload_process.php";
    //public static final String UPLOAD_URL = "http://13.76.229.21:1680/upload_process.php";
    //public static final String UPLOAD_URL = "https://13.76.229.21:4433/upload_process.php";
    private static final String UPLOADED_FILE = "uploaded_file";
    private static final String ABSOLUTE_PATH = "absolute_path";
    private Context context;

    public MyUploadFile(Context context) {
        this.context = context;
    }

    public String UploadHTTP(
            Context context, String filePath, String remotePath
    ) {
        String uploadID = null;
        try {
            uploadID = new MultipartUploadRequest(context, UPLOAD_URL)
                    .addFileToUpload(filePath, UPLOADED_FILE)
                    .addParameter(ABSOLUTE_PATH, remotePath)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setAutoDeleteFilesAfterSuccessfulUpload(true)
                    .setMaxRetries(3)
                    .startUpload();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return uploadID;
    }

    /*public String uploadFTP(
        Context context, String host, int port, String username, String password, String filePath, String remotePath
    ) {
        String uploadedID = null;
        try {
            uploadedID = new FTPUploadRequest(context, host, port)
                    .setUsernameAndPassword(username, password)
                    .addFileToUpload(filePath, remotePath)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .useCompressedFileTransferMode(true)
                    .setAutoDeleteFilesAfterSuccessfulUpload(true)
                    .setMaxRetries(3)
                    .startUpload();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return uploadedID;
    }*/
}
