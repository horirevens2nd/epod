package horirevens.com.epod.libraries;

import android.content.Context;
import android.os.Environment;

import java.io.File;

import es.dmoral.toasty.Toasty;
import horirevens.com.epod.AppConfig;

/**
 * Created by horirevens on 3/23/18.
 */

public class MyFile {
    private Context context;

    public MyFile(Context context) {
        this.context = context;
    }

    public File getFolderPicturesOnStorageDirectory(Context context, String name) {
        File fileDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), name);

        if (!fileDir.exists()) {
            if (!fileDir.mkdirs()) {
                String message = "Membuat folder " + name + " gagal. Pastikan perizinan \"STORAGE\" telah di-allow";
                Toasty.error(context, message, 3000, false).show();
            }
        }

        return fileDir;
    }

    public File getFolderDirectory(String name) {
        File file = new File(getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_SIGNATURE), name);

        return file;
    }
}
