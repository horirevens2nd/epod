package horirevens.com.epod.libraries;

import android.app.AlertDialog;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;

import com.horirevens.mylibrary.fonts.ButtonLatoBold;
import com.horirevens.mylibrary.fonts.TextViewLatoBold;
import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import horirevens.com.epod.R;

/**
 * Created by horirevens on 3/1/18.
 */

public class AlertDialogConfirmation extends AlertDialog {
    
    private Context context;
    private AlertDialog alertDialog;

    private String title, message, actionPositive, titlePositive, actionNegative, titleNegative;

    private AlertDialogConfirmationListener listener;

    public AlertDialogConfirmation(Context context, AlertDialogConfirmationListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    public AlertDialogConfirmation setTitle(String title) {
        this.title = title;
        return this;
    }

    public AlertDialogConfirmation setMessage(String message) {
        this.message = message;
        return this;
    }

    public AlertDialogConfirmation setActionPositive(String actionPositive) {
        this.actionPositive = actionPositive;
        return this;
    }

    public AlertDialogConfirmation setTitleOnPositiveButton(String titlePositive) {
        this.titlePositive = titlePositive;
        return this;
    }

    public AlertDialogConfirmation setActionNegative(String actionNegative) {
        this.actionNegative = actionNegative;
        return this;
    }

    public AlertDialogConfirmation setTitleOnNegativeButton(String titleNegative) {
        this.titleNegative = titleNegative;
        return this;
    }

    public AlertDialogConfirmation showDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.alert_dialog_default, null);
        ConstraintLayout headerContainer = (ConstraintLayout) view.findViewById(R.id.header_container);
        TextViewLatoBold tvTitle = (TextViewLatoBold) view.findViewById(R.id.tv_title);
        TextViewLatoRegular tvMessage = (TextViewLatoRegular) view.findViewById(R.id.tv_message);
        ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        ButtonLatoBold btnNegative = (ButtonLatoBold) view.findViewById(R.id.btn_negative);

        if (title == null) {
            headerContainer.setVisibility(View.GONE);
        }

        if (message == null) {
            message = "";
        }

        if (actionPositive == null) {
            actionPositive = "";
        }

        if (titlePositive == null) {
            titlePositive = context.getResources().getString(R.string.button_yes);
        }

        if (actionNegative == null) {
            actionNegative = "";
        }

        if (titleNegative == null) {
            titleNegative = context.getResources().getString(R.string.button_no);
        }

        tvTitle.setText(title);
        tvMessage.setText(message);
        btnPositive.setText(titlePositive);
        btnNegative.setText(titleNegative);

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                listener.positiveButtonClickedOnAlertDialogConfirmation(actionPositive);
            }
        });

        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                listener.negativeButtonClickedOnAlertDialogConfirmation(actionNegative);
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        return this;
    }

    public interface AlertDialogConfirmationListener {

        void positiveButtonClickedOnAlertDialogConfirmation(String action);

        void negativeButtonClickedOnAlertDialogConfirmation(String action);

    }
}
