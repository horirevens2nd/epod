package horirevens.com.epod.libraries;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by horirevens on 10/1/17.
 */

class ImageUtil {
    private static final String TAG = "ImageUtil";

    private static Bitmap scaledBitmap;

    private ImageUtil() {
        // Do nothing
    }

    static File compressImage(File oldFileDir, File oldFile, String fileName, int reqWidth, int reqHeight, Bitmap.CompressFormat compressFormat,
                              int quality) throws IOException {
        FileOutputStream fileOutputStream = null;

        String[] splitFileName = fileName.split("_");
        String newFileName = "IMG_" + splitFileName[1];
        File fileDest = new File(oldFileDir, newFileName);
        Log.i(TAG, String.valueOf(fileDest));

        if (!oldFileDir.exists()) {
            oldFileDir.mkdirs();
        }

        try {
            fileOutputStream = new FileOutputStream(fileDest);
            decodeSampledBitmapFromFile(oldFile, reqWidth, reqHeight).compress(compressFormat, quality, fileOutputStream);

            if (scaledBitmap != null) {
                oldFile.delete();
            }
        } finally {
            if (fileOutputStream != null) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        }

        return fileDest;
    }

    static Bitmap decodeSampledBitmapFromFile(File oldFile, int reqWidth, int reqHeight) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(oldFile.getAbsolutePath(), options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;

        scaledBitmap = BitmapFactory.decodeFile(oldFile.getAbsolutePath(), options);
        ExifInterface exif;
        exif = new ExifInterface(oldFile.getAbsolutePath());
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);

        Matrix matrix = new Matrix();
        if (orientation == 6) {
            matrix.postRotate(90);
        } else if (orientation == 3) {
            matrix.postRotate(180);
        } else if (orientation == 8) {
            matrix.postRotate(270);
        }

        scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        return scaledBitmap;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
