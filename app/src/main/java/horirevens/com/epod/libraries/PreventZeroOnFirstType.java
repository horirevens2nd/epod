package horirevens.com.epod.libraries;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

/**
 * Created by horirevens on 3/20/18.
 */

public class PreventZeroOnFirstType implements TextWatcher {
    private View view;

    public PreventZeroOnFirstType(View view) {
        this.view = view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        delete(s);
    }

    private void delete(Editable s) {
        if (s.toString().equals("0") && s.toString().length() == 1) {
            s.replace(0, 1, "");
        }
    }
}
