package horirevens.com.epod.libraries;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import horirevens.com.epod.R;

/**
 * Created by horirevens on 5/14/17.
 */

public class AlertDialogLoading extends AlertDialog {

    private Context context;
    private AlertDialog alertDialog;
    private boolean isCancelAble;

    public AlertDialogLoading(Context context) {
        super(context);
        this.context = context;
    }

    public AlertDialogLoading setCancelAble(boolean isCancelAble) {
        this.isCancelAble = isCancelAble;
        return this;
    }

    public AlertDialogLoading showDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.alert_dialog_loading, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.setCancelable(isCancelAble);
        alertDialog.setCanceledOnTouchOutside(isCancelAble);
        alertDialog.show();

        return this;
    }

    public AlertDialogLoading dismissDialog() {
        alertDialog.dismiss();

        return this;
    }
}
