package horirevens.com.epod.libraries;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;

import io.reactivex.Flowable;

/**
 * Created by horirevens on 10/1/17.
 */

public class ImageCompressor {

    private int maxWidth = 640;
    private int maxHeight = 480;
    private int quality = 80;
    private Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.JPEG;
    private File fileDir;

    public ImageCompressor(Context context) {
        fileDir = new File(context.getCacheDir().getPath());
    }

    public ImageCompressor setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
        return this;
    }

    public ImageCompressor setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
        return this;
    }

    public ImageCompressor setCompressFormat(Bitmap.CompressFormat compressFormat) {
        this.compressFormat = compressFormat;
        return this;
    }

    public ImageCompressor setQuality(int quality) {
        this.quality = quality;
        return this;
    }

    public ImageCompressor setAlbumName(String folderName) {
        this.fileDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), folderName);
        return this;
    }

    public File compressToFile(File file) throws IOException {
        return compressToFile(file, file.getName());
    }

    public File compressToFile(File oldFile, String fileName) throws IOException {
        return ImageUtil.compressImage(fileDir, oldFile, fileName, maxWidth, maxHeight, compressFormat, quality);
    }

    public Bitmap compressToBitmap(File file) throws IOException {
        return ImageUtil.decodeSampledBitmapFromFile(file, maxWidth, maxHeight);
    }

    public Flowable<File> compressToFileAsFlowable(final File file) {
        return compressToFileAsFlowable(file, file.getName());
    }

    public Flowable<File> compressToFileAsFlowable(final File file, final String fileName) {
        return Flowable.defer(new Callable<Flowable<File>>() {
            @Override
            public Flowable<File> call() {
                try {
                    return Flowable.just(compressToFile(file, fileName));
                } catch (IOException e) {
                    return Flowable.error(e);
                }
            }
        });
    }

    public Flowable<Bitmap> compressToBitmapAsFlowable(final File file) {
        return Flowable.defer(new Callable<Flowable<Bitmap>>() {
            @Override
            public Flowable<Bitmap> call() {
                try {
                    return Flowable.just(compressToBitmap(file));
                } catch (IOException e) {
                    return Flowable.error(e);
                }
            }
        });
    }
}
