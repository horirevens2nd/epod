package horirevens.com.epod.libraries;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.reflect.Field;


/**
 * Created by horirevens on 3/4/18.
 */

public class CustomFont {

    public static void setDefaultFont(Context context, String fontFaceName, String fontAssetName) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontAssetName);
        replaceFont(fontFaceName, typeface);
    }

    protected static void replaceFont(String fontFaceName, Typeface fontAssetName) {
        try {
            Field staticField = Typeface.class.getDeclaredField(fontFaceName);
            staticField.setAccessible(true);
            staticField.set(null, fontAssetName);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
