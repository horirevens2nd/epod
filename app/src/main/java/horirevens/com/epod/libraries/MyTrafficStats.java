package horirevens.com.epod.libraries;

import android.content.Context;
import android.net.TrafficStats;

import java.text.DecimalFormat;
import java.util.logging.Handler;

import horirevens.com.epod.helpers.DatabaseHelper;

public class MyTrafficStats {
    private Context context;
    private Handler handler;
    private DatabaseHelper db;
    private long startTx = 0;
    private long startRx = 0;

    public MyTrafficStats(Context context) {
        this.context = context;
        this.startRx = TrafficStats.getMobileRxBytes();
        this.startTx = TrafficStats.getMobileTxBytes();
    }

    public void submitTrafficStatsIntoDataUsage(Context context, long mobileRx, long mobileTx) {
        db = new DatabaseHelper(context);

        // cek jika tidak null
    }

    private String convertSize(long size) {
        if (size <= 0) {
            return "0";
        }

        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));

        return new DecimalFormat("#,##0.##").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }
}
