package horirevens.com.epod.libraries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by horirevens on 3/22/18.
 */

public class MyDate {

    public static String setToDatetime(String date) {
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);
        String time = date.substring(11, 19);

        String dayName = getDayOfWeek(date);
        String monthName = getMonthOfYear(date);

        // Senin, 17 Agustus 2017 23:00:00 WIB
        String result = dayName +", "+ day +" " +monthName +" "+ year + " - " + time +" WIB";
        return  result;
    }

    public static String getDayOfWeek(String date) {
        String result = null;
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);

        int intYear = Integer.parseInt(year);
        int intMonth = Integer.parseInt(month);
        int intDay = Integer.parseInt(day);

        Calendar calendar = new GregorianCalendar(intYear, intMonth-1, intDay);
        int calResult = calendar.get(Calendar.DAY_OF_WEEK);

        switch (calResult) {
            case Calendar.SUNDAY:
                result = ("Minggu");
                break;
            case Calendar.MONDAY:
                result = ("Senin");
                break;
            case Calendar.TUESDAY:
                result = ("Selasa");
                break;
            case Calendar.WEDNESDAY:
                result = ("Rabu");
                break;
            case Calendar.THURSDAY:
                result = ("Kamis");
                break;
            case Calendar.FRIDAY:
                result = ("Jumat");
                break;
            case Calendar.SATURDAY:
                result = ("Sabtu");
                break;
        }

        return result;
    }

    public static String getMonthOfYear(String date) {
        String result = null;
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);

        int intYear = Integer.parseInt(year);
        int intMonth = Integer.parseInt(month);
        int intDay = Integer.parseInt(day);

        Calendar calendar = new GregorianCalendar(intYear, intMonth-1, intDay);
        int calResult = calendar.get(Calendar.MONTH);

        switch (calResult) {
            case Calendar.JANUARY:
                result = ("Januari");
                break;
            case Calendar.FEBRUARY:
                result = ("Februari");
                break;
            case Calendar.MARCH:
                result = ("Maret");
                break;
            case Calendar.APRIL:
                result = ("April");
                break;
            case Calendar.MAY:
                result = ("Mei");
                break;
            case Calendar.JUNE:
                result = ("Juni");
                break;
            case Calendar.JULY:
                result = ("Juli");
                break;
            case Calendar.AUGUST:
                result = ("Agustus");
                break;
            case Calendar.SEPTEMBER:
                result = ("September");
                break;
            case Calendar.OCTOBER:
                result = ("Oktober");
                break;
            case Calendar.NOVEMBER:
                result = ("November");
                break;
            case Calendar.DECEMBER:
                result = ("Desember");
                break;
        }

        return result;
    }

    public static String getMonthOfYearShort(String date) {
        String monthName = null;
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);

        int intYear = Integer.parseInt(year);
        int intMonth = Integer.parseInt(month);
        int intDay = Integer.parseInt(day);

        Calendar calendar = new GregorianCalendar(intYear, intMonth-1, intDay);
        int calResult = calendar.get(Calendar.MONTH);

        switch (calResult) {
            case Calendar.JANUARY:
                monthName = ("Jan");
                break;
            case Calendar.FEBRUARY:
                monthName = ("Feb");
                break;
            case Calendar.MARCH:
                monthName = ("Mar");
                break;
            case Calendar.APRIL:
                monthName = ("Apr");
                break;
            case Calendar.MAY:
                monthName = ("Mei");
                break;
            case Calendar.JUNE:
                monthName = ("Jun");
                break;
            case Calendar.JULY:
                monthName = ("Jul");
                break;
            case Calendar.AUGUST:
                monthName = ("Agu");
                break;
            case Calendar.SEPTEMBER:
                monthName = ("Sep");
                break;
            case Calendar.OCTOBER:
                monthName = ("Okt");
                break;
            case Calendar.NOVEMBER:
                monthName = ("Nov");
                break;
            case Calendar.DECEMBER:
                monthName = ("Des");
                break;
        }

        String result = monthName;

        return result;
    }

    public static String getMonthNameShort(String month) {
        String monthName = null;

        switch (month) {
            case "01":
                monthName = ("JAN");
                break;
            case "02":
                monthName = ("FEB");
                break;
            case "03":
                monthName = ("MAR");
                break;
            case "04":
                monthName = ("APR");
                break;
            case "05":
                monthName = ("MEI");
                break;
            case "06":
                monthName = ("JUN");
                break;
            case "07":
                monthName = ("JUL");
                break;
            case "08":
                monthName = ("AGU");
                break;
            case "09":
                monthName = ("SEP");
                break;
            case "10":
                monthName = ("OKT");
                break;
            case "11":
                monthName = ("NOV");
                break;
            case "12":
                monthName = ("DES");
                break;
        }

        String result = monthName;

        return result;
    }

    public static String setToAmPm(String dateTime) {
        StringTokenizer tk = new StringTokenizer(dateTime);
        String date = tk.nextToken();
        String time = tk.nextToken();

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss", Locale.getDefault());
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        Date dt = null;

        try {
            dt = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return sdfs.format(dt);
    }

    public static String getTimeShort(String time) {
        return time.substring(11, 16);
    }

    public static String getMonthShort(String date) {
        return date.substring(5, 7);
    }

    public static String getDateShort(String date) {
        String day = date.substring(8, 10);
        String month = date.substring(5, 7);

        return day +"/"+ month;
    }

    public static String setDateTimeIndo(String date) { // yyyy-MM-dd
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);

        return day + "-" + month + "-" + year;
    }

}
