package horirevens.com.epod;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

import horirevens.com.epod.activities.LoginActivity;
import horirevens.com.epod.activities.MainActivity;

/**
 * Created by horirevens on 3/2/18.
 */

public class LoginSession {
    private static final String TAG = LoginSession.class.getName();
    private static final String IS_LOGGED_IN = "IsLogin";
    private static final String PREF_NAME = "LoginPreferences";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;
    private Intent intent;

    public LoginSession(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setSession(
            String id, String nama, String idLogin, String idKantor, String idMandor, String namaMandor, String idGrup,
            String server, String baseUrl
    ) {
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.putString(AppConfig.ID, id);
        editor.putString(AppConfig.NAMA, nama);
        editor.putString(AppConfig.ID_LOGIN, idLogin);
        editor.putString(AppConfig.ID_KANTOR, idKantor);
        editor.putString(AppConfig.ID_MANDOR, idMandor);
        editor.putString(AppConfig.NAMA_MANDOR, namaMandor);
        editor.putString(AppConfig.ID_GRUP, idGrup);
        editor.putString(AppConfig.SERVER, server);
        editor.putString(AppConfig.BASE_URL, baseUrl);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return preferences.getBoolean(IS_LOGGED_IN, false);
    }

    public void checkLogin() {
        if (!this.isLoggedIn()) {
            intent = new Intent(context, LoginActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        } else {
            intent = new Intent(context, MainActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
    }

    public HashMap<String, String> getSession() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConfig.ID, preferences.getString(AppConfig.ID, null));
        hashMap.put(AppConfig.NAMA, preferences.getString(AppConfig.NAMA, null));
        hashMap.put(AppConfig.ID_LOGIN, preferences.getString(AppConfig.ID_LOGIN, null));
        hashMap.put(AppConfig.ID_KANTOR, preferences.getString(AppConfig.ID_KANTOR, null));
        hashMap.put(AppConfig.ID_MANDOR, preferences.getString(AppConfig.ID_MANDOR, null));
        hashMap.put(AppConfig.NAMA_MANDOR, preferences.getString(AppConfig.NAMA_MANDOR, null));
        hashMap.put(AppConfig.ID_GRUP, preferences.getString(AppConfig.ID_GRUP, null));
        hashMap.put(AppConfig.SERVER, preferences.getString(AppConfig.SERVER, null));
        hashMap.put(AppConfig.BASE_URL, preferences.getString(AppConfig.BASE_URL, null));

        return hashMap;
    }

    public void clearSession() {
        editor.clear();
        editor.commit();

       /* intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("exit", true);
        context.startActivity(intent);*/
    }
}
