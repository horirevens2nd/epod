package horirevens.com.epod.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.github.florent37.camerafragment.CameraFragment;
import com.github.florent37.camerafragment.CameraFragmentApi;
import com.github.florent37.camerafragment.PreviewActivity;
import com.github.florent37.camerafragment.configuration.Configuration;
import com.github.florent37.camerafragment.listeners.CameraFragmentControlsAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener;
import com.github.florent37.camerafragment.listeners.CameraFragmentStateListener;
import com.github.florent37.camerafragment.widgets.CameraSettingsView;
import com.github.florent37.camerafragment.widgets.CameraSwitchView;
import com.github.florent37.camerafragment.widgets.FlashSwitchView;
import com.github.florent37.camerafragment.widgets.RecordButton;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import horirevens.com.epod.AppConfig;
import horirevens.com.epod.R;
import horirevens.com.epod.libraries.MyFile;

public class CameraActivity extends AppCompatActivity {

    private static final String TAG = CameraActivity.class.getName();
    public static final String FRAGMENT_TAG = "camera";
    private static final int REQUEST_PREVIEW_CODE = 1001;

    private MyFile myFile;
    
    private File imageFile;
    private String idItem, imageFileName;

    @BindView(R.id.settings_view)
    CameraSettingsView settingsView;
    @BindView(R.id.flash_switch_view)
    FlashSwitchView flashSwitchView;
    @BindView(R.id.front_back_camera_switcher)
    CameraSwitchView cameraSwitchView;
    @BindView(R.id.record_button)
    RecordButton recordButton;
    @BindView(R.id.cameraLayout)
    View cameraLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        Log.d(TAG, "onCreate");

        ButterKnife.bind(this);
        myFile = new MyFile(this);
        idItem = getIntent().getStringExtra("idItem");

        addCamera();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PREVIEW_CODE) {
            if (resultCode == RESULT_OK) {
                String resultAction = data.getStringExtra("action");
                boolean resultUseBackCamera = data.getExtras().getBoolean("isUseBackCamera");

                if ("save".equalsIgnoreCase(resultAction)) {
                    Intent intent = new Intent();
                    intent.putExtra("action", "save");
                    intent.putExtra("imageFileName", imageFileName + ".jpg");
                    intent.putExtra("imageFile", String.valueOf(imageFile + ".jpg"));
                    intent.putExtra("isUseBackCamera", resultUseBackCamera);
                    setResult(RESULT_OK, intent);
                    finish();
                }

                if ("repeat".equalsIgnoreCase(resultAction)) {
                    // Do nothing
                }

                if ("cancel".equalsIgnoreCase(resultAction)) {
                    Intent intent = new Intent();
                    intent.putExtra("action", "cancel");
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void addCamera() {
        Log.d(TAG, "addCamera");
        final Configuration.Builder builder = new Configuration.Builder();
        builder
                .setCamera(Configuration.CAMERA_FACE_REAR)
                .setMediaQuality(Configuration.MEDIA_QUALITY_HIGHEST)
                .setFlashMode(Configuration.FLASH_MODE_OFF)
                .setMediaAction(Configuration.MEDIA_ACTION_PHOTO);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        CameraFragment cameraFragment = CameraFragment.newInstance(builder.build());
        getSupportFragmentManager().beginTransaction().replace(R.id.content, cameraFragment, FRAGMENT_TAG).commit();

        if (cameraFragment != null) {
            cameraFragment.setResultListener(new CameraFragmentResultListener() {
                @Override
                public void onVideoRecorded(String filePath) {

                }

                @Override
                public void onPhotoTaken(byte[] bytes, String filePath) {
                    Intent intent = PreviewActivity.newIntentPhoto(CameraActivity.this, filePath);
                    startActivityForResult(intent, REQUEST_PREVIEW_CODE);
                }
            });

            cameraFragment.setStateListener(new CameraFragmentStateListener() {
                @Override
                public void onCurrentCameraBack() {
                    cameraSwitchView.displayBackCamera();
                }

                @Override
                public void onCurrentCameraFront() {
                    cameraSwitchView.displayFrontCamera();
                }

                @Override
                public void onFlashAuto() {
                    flashSwitchView.displayFlashAuto();
                }

                @Override
                public void onFlashOn() {
                    flashSwitchView.displayFlashOn();
                }

                @Override
                public void onFlashOff() {
                    flashSwitchView.displayFlashOff();
                }

                @Override
                public void onCameraSetupForPhoto() {

                }

                @Override
                public void onCameraSetupForVideo() {

                }

                @Override
                public void onRecordStateVideoReadyForRecord() {

                }

                @Override
                public void onRecordStateVideoInProgress() {

                }

                @Override
                public void onRecordStatePhoto() {

                }

                @Override
                public void shouldRotateControls(int degrees) {
                    ViewCompat.setRotation(flashSwitchView, degrees);
                }

                @Override
                public void onStartVideoRecord(File outputFile) {

                }

                @Override
                public void onStopVideoRecord() {

                }
            });

            cameraFragment.setControlsListener(new CameraFragmentControlsAdapter() {
                @Override
                public void lockControls() {
                    recordButton.setEnabled(false);
                    flashSwitchView.setEnabled(false);
                    settingsView.setEnabled(false);
                    cameraSwitchView.setEnabled(false);
                }

                @Override
                public void unLockControls() {
                    recordButton.setEnabled(true);
                    flashSwitchView.setEnabled(true);
                    settingsView.setEnabled(true);
                    cameraSwitchView.setEnabled(true);
                }

                @Override
                public void allowCameraSwitching(boolean allow) {
                    cameraSwitchView.setVisibility(allow ? View.VISIBLE : View.GONE);
                }

                @Override
                public void allowRecord(boolean allow) {
                    recordButton.setEnabled(allow);
                }
            });
        }
    }

    @OnClick(R.id.settings_view)
    public void onSettingsClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.openSettingDialog();
        }
    }

    @OnClick(R.id.flash_switch_view)
    public void onFlashSwitchClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.toggleFlashMode();
        }
    }

    @OnClick(R.id.front_back_camera_switcher)
    public void onSwitchCameraClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.switchCameraTypeFrontBack();
        }
    }

    @OnClick(R.id.record_button)
    public void onRecordButtonClicked() {
        File imageDir = myFile.getFolderPicturesOnStorageDirectory(CameraActivity.this, AppConfig.FOLDER_IMAGE);
        imageFileName = "IMGU_" + idItem;
        imageFile = new File(myFile.getFolderPicturesOnStorageDirectory(CameraActivity.this, AppConfig.FOLDER_IMAGE), imageFileName);

        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.takePhotoOrCaptureVideo(new CameraFragmentResultListener() {
                @Override
                public void onVideoRecorded(String filePath) {

                }

                @Override
                public void onPhotoTaken(byte[] bytes, String filePath) {

                }
            }, String.valueOf(imageDir), imageFileName);
        }
    }

    private CameraFragmentApi getCameraFragment() {
        return (CameraFragmentApi) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra("action", "cancel");
        setResult(RESULT_OK, intent);
        finish();
    }
}
