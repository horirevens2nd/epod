package horirevens.com.epod.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.horirevens.mylibrary.fonts.TextViewLatoBold;
import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import horirevens.com.epod.R;
import horirevens.com.epod.helpers.ExceptionHandler;

public class CrashReportActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_crash_report);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        TextViewLatoBold tvTitleToolbar = (TextViewLatoBold) findViewById(R.id.tv_title);
        tvTitleToolbar.setText(getResources().getString(R.string.title_force_close));

        String message = getIntent().getStringExtra("crashMessage");
        TextViewLatoRegular tvCrashMessage = (TextViewLatoRegular) findViewById(R.id.tv_crash_message);
        tvCrashMessage.setText(message);

    }
}
