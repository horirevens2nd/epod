package horirevens.com.epod.activities;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.horirevens.mylibrary.fonts.ButtonLatoBold;
import com.horirevens.mylibrary.fonts.TextInputEditTextLatoRegular;
import com.horirevens.mylibrary.fonts.TextViewLatoBold;
import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import horirevens.com.epod.libraries.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.UUID;

import es.dmoral.toasty.Toasty;
import horirevens.com.epod.AppConfig;
import horirevens.com.epod.DateSession;
import horirevens.com.epod.LoginSession;
import horirevens.com.epod.MainApplication;
import horirevens.com.epod.R;
import horirevens.com.epod.adapters.ItemCollectiveAdapter;
import horirevens.com.epod.adapters.StatusAdapter;
import horirevens.com.epod.helpers.DatabaseHelper;
import horirevens.com.epod.helpers.ExceptionHandler;
import horirevens.com.epod.models.Antaran;
import horirevens.com.epod.models.FileUploadError;
import horirevens.com.epod.models.FileUploadUsage;
import horirevens.com.epod.models.ItemCollective;
import horirevens.com.epod.models.Status;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class CollectiveActivity extends AppCompatActivity implements
        ItemCollectiveAdapter.ItemCollectiveListener,
        AlertDialogInformation.AlertDialogInformationListener,
        AlertDialogConfirmation.AlertDialogConfirmationListener,
        LocationListener,
        View.OnClickListener {
    private static final String TAG = CollectiveActivity.class.getName();
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; //meter
    private static final long MIN_TIME_BETWEEN_UPDATES = 5000;
    private static final int REQUEST_CODE_CAMERA = 101;
    private static final String[] PERMISSIONS_REQUIRED = new String[] {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };
    private static final int PERMISSION_CALLBACK_CONSTANT = 101;

    private DatabaseHelper db;
    private MyFile myFile;
    private MyUploadFile myUploadFile;
    private AlertDialogLoading alertDialogLoading;
    private AlertDialogInformation alertDialogInformation;
    private AlertDialogConfirmation alertDialogConfirmation;
    private OkHttpClient client;
    private ArrayList<Antaran> arrayList = new ArrayList<>();
    private ArrayList<ItemCollective> arrayListCheckedItem = new ArrayList<>();
    private ItemCollectiveAdapter adapter;
    private SharedPreferences locationPreferences;
    private SharedPreferences.Editor locationEditor;
    private LocationManager locationManager;
    private Location location;
    private AlertDialog adUpdateStatus, adStatus, adRecipient, adSignature, adValidation;

    private ConstraintLayout constraintLayout;
    private RecyclerView recyclerView;
    private TextViewLatoBold tvTitleToolbar;
    private SearchView searchView;
    //private CheckBox checkBoxAll;

    private String idPengantar, idKantor, baseUrl, keteranganBerhasil, keteranganGagal;
    private String signatureFileName, imageFileName, remotePath;
    private String idStatus, keterangan, tindakan, kodeLn, kodeDn, kodeStatusGagal;
    private String dateFromSession;
    private String alamatBaru, catatan;
    private File signatureFile, compressedImage;
    private boolean isBerhasil, isWithPhoto;
    private Double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_collective);
        Log.i(TAG, "onCreate");

        if (getIntent().getBooleanExtra("exit", false)) {
            finish();
        }

        db = new DatabaseHelper(this);
        alertDialogLoading = new AlertDialogLoading(this);
        alertDialogInformation = new AlertDialogInformation(this, this);
        alertDialogConfirmation = new AlertDialogConfirmation(this, this);
        myFile = new MyFile(this);
        myUploadFile = new MyUploadFile(this);
        client = MainApplication.getUnsafeOkHttpClient();
        locationPreferences = getSharedPreferences("LocationPreferences", Context.MODE_PRIVATE);
        latitude = 0.000000;
        longitude = 0.000000;
        alamatBaru = "";
        catatan = "";
        resetAllValue();

        LoginSession loginSession = new LoginSession(this);
        HashMap<String, String> hashMap = loginSession.getSession();
        idPengantar = hashMap.get(AppConfig.ID);
        idKantor = hashMap.get(AppConfig.ID_KANTOR);
        baseUrl = hashMap.get(AppConfig.BASE_URL);

        DateSession dateSession = new DateSession(this);
        HashMap<String, String> hashMap2 = dateSession.getSession();
        dateFromSession = hashMap2.get("date");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvTitleToolbar = (TextViewLatoBold) findViewById(R.id.tv_title);
        tvTitleToolbar.setText(getResources().getString(R.string.title_collective));

        constraintLayout = (ConstraintLayout) findViewById(R.id.info_container);
        constraintLayout.setVisibility(View.GONE);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Date date = new Date(System.currentTimeMillis());
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);

                int countItem = db.getCountAntaranProses(currentDate);
                if (countItem > 0) {
                    arrayList.clear();
                    arrayList = db.getAllAntaranProses(currentDate);
                    adapter = new ItemCollectiveAdapter(CollectiveActivity.this, arrayList, CollectiveActivity.this);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);

                    constraintLayout.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } else {
                    constraintLayout.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
        }, 500);

        validatePermissions();
    }

    public boolean isGPSEnabled() {
        Log.i(TAG, "isGPSEnabled");
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void validatePermissions() {
        Log.i(TAG, "validatePermissions");
        if (ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[0]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[1]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[2]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[3]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[4]) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[0]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[1]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[2]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[3]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[4])) {
                alertDialogInformation
                        .setMessage(getResources().getString(R.string.message_permissions))
                        .setActionPositive("permissionRequired")
                        .showDialog();
            } else {
                ActivityCompat.requestPermissions(this, PERMISSIONS_REQUIRED, PERMISSION_CALLBACK_CONSTANT);
            }
        } else {
            if (!isGPSEnabled()) {
                alertDialogInformation
                        .setMessage(getResources().getString(R.string.message_gps))
                        .setActionPositive("locationSetting")
                        .showDialog();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            boolean allGranted = false;
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    allGranted = true;
                } else {
                    allGranted = false;
                    break;
                }
            }

            if (allGranted) {
                if (!isGPSEnabled()) {
                    alertDialogInformation
                            .setMessage(getResources().getString(R.string.message_gps))
                            .setActionPositive("locationSetting")
                            .showDialog();
                }
            } else if (
                    ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[0]) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[1]) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[2]) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[3]) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[4])) {
                alertDialogInformation
                        .setMessage(getResources().getString(R.string.message_permissions))
                        .setActionPositive("permissionRequired")
                        .showDialog();
            }
        }
    }

    private void startLocation() {
        Log.i(TAG, "startLocation");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                MIN_TIME_BETWEEN_UPDATES,
                MIN_DISTANCE_CHANGE_FOR_UPDATES,
                this);

        if (locationManager != null) {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                locationEditor = locationPreferences.edit();
                locationEditor.putString("latitude", String.valueOf(latitude));
                locationEditor.putString("longitude", String.valueOf(longitude));
                locationEditor.apply();
            }
        }
    }

    private void stopLocation() {
        Log.i(TAG, "stopLocation");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationManager.removeUpdates(this);
        locationManager = null;
        location = null;
    }

    private void searchIdItem(MenuItem menuItem) {
        final SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        if (menuItem != null) {
            MenuItemCompat.setOnActionExpandListener(menuItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem menuItem) {
                    if (searchView != null) {
                        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                        searchView.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                        searchView.setIconified(false);
                        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                            @Override
                            public boolean onQueryTextSubmit(String query) {
                                return false;
                            }

                            @Override
                            public boolean onQueryTextChange(String newText) {
                                adapter.search(newText);
                                recyclerView.invalidate();
                                return false;
                            }
                        });
                    }
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                    return true;
                }
            });

            searchView = (SearchView) menuItem.getActionView();
        }
    }

    private void showDialogUpdateStatus() {
        Log.i(TAG, "showDialogUpdateStatus");
        View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_update_status_collective, null, false);
        TextViewLatoRegular tvTotalItem = (TextViewLatoRegular) view.findViewById(R.id.tv_total_item);
        tvTotalItem.setText(String.valueOf(arrayListCheckedItem.size()));

        ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBerhasil = true;
                showDialogStatus();
                adUpdateStatus.dismiss();
            }
        });

        ButtonLatoBold btnNegative = (ButtonLatoBold) view.findViewById(R.id.btn_negative);
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBerhasil = false;
                showDialogStatus();
                adUpdateStatus.dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setView(view);
        adUpdateStatus = builder.create();
        adUpdateStatus.setCanceledOnTouchOutside(true);
        adUpdateStatus.show();
    }

    private void showDialogStatus() {
        Log.i(TAG, "showDialogStatus");
        String title;
        ArrayList<Status> arrayList = new ArrayList<>();
        arrayList.clear();

        if (isBerhasil) {
            arrayList = db.getAllStatusBerhasil();
            title = getResources().getString(R.string.title_status_success);
        } else {
            arrayList = db.getAllStatusGagal();
            title = getResources().getString(R.string.title_status_failed);
        }

        StatusAdapter adapter = new StatusAdapter(this, arrayList, new StatusAdapter.StatusListener() {
            @Override
            public void onItemClicked(Status status) {
                idStatus = status.getId();
                keterangan = status.getKeterangan();
                tindakan = status.getTindakan();
                kodeLn = status.getKodeLn();
                kodeDn = status.getKodeDn();

                if (
                        idStatus.equalsIgnoreCase("B01") || idStatus.equalsIgnoreCase("B02") ||
                        idStatus.equalsIgnoreCase("B03") || idStatus.equalsIgnoreCase("B04") ||
                        idStatus.equalsIgnoreCase("B05") || idStatus.equalsIgnoreCase("B06") ||
                        idStatus.equalsIgnoreCase("B07") || idStatus.equalsIgnoreCase("B08") ||
                        idStatus.equalsIgnoreCase("B09") || idStatus.equalsIgnoreCase("B10") ||
                        idStatus.equalsIgnoreCase("B11") || idStatus.equalsIgnoreCase("B12"))
                {
                    if (status.getTerakhir().equalsIgnoreCase("1")) {
                        adStatus.dismiss();
                        showDialogRecipient();
                    }
                } else {
                    if (idStatus.equalsIgnoreCase("G01")) {
                        kodeStatusGagal = "15";
                    } else if (
                            idStatus.equalsIgnoreCase("G02") || idStatus.equalsIgnoreCase("G03") ||
                            idStatus.equalsIgnoreCase("G04"))
                    {
                        kodeStatusGagal = "14";
                    } else if (idStatus.equalsIgnoreCase("G05")) {
                        kodeStatusGagal = "17";
                    }

                    adStatus.dismiss();
                    showDialogStatusLevel2(idStatus);
                }
            }
        });

        View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_status, null, false);
        TextViewLatoBold tvTitle = (TextViewLatoBold) view.findViewById(R.id.tv_title);
        tvTitle.setText(title);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setView(view);
        adStatus = builder.create();
        adStatus.setCanceledOnTouchOutside(true);
        adStatus.show();
    }

    private void showDialogStatusLevel2(String id) {
        Log.i(TAG, "showDialogStatusLevel2");
        ArrayList<Status> arrayList = new ArrayList<>();
        arrayList.clear();
        arrayList = db.getAllStatusGagalLevel2(id);

        StatusAdapter adapter = new StatusAdapter(this, arrayList, new StatusAdapter.StatusListener() {
            @Override
            public void onItemClicked(Status status) {
                idStatus = status.getId();
                keterangan = status.getKeterangan();
                tindakan = status.getTindakan();
                kodeLn = status.getKodeLn();
                kodeDn = status.getKodeDn();

                if (status.getTerakhir().equalsIgnoreCase("1")) {
                    adStatus.dismiss();
                    startCameraActivity();
                }
            }
        });

        View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_status, null, false);
        TextViewLatoBold tvTitle = (TextViewLatoBold) view.findViewById(R.id.tv_title);
        tvTitle.setText(getResources().getString(R.string.title_status_failed));

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setView(view);
        adStatus = builder.create();
        adStatus.setCanceledOnTouchOutside(true);
        adStatus.show();
    }

    private void showDialogRecipient() {
        Log.i(TAG, "showDialogRecipient");
        View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_entry_recipient, null, false);
        TextViewLatoBold tvTitle = (TextViewLatoBold) view.findViewById(R.id.tv_title);
        tvTitle.setText(getResources().getString(R.string.title_entry_recipient));

        TextViewLatoRegular tvNamaPenerima = (TextViewLatoRegular) view.findViewById(R.id.tv_nama_penerima);
        tvNamaPenerima.setText(db.getNamaPenerimaFromAntaran(arrayListCheckedItem.get(0).getIdItem()));
        TextViewLatoRegular tvAlamatPenerima = (TextViewLatoRegular) view.findViewById(R.id.tv_alamat_penerima);
        tvAlamatPenerima.setText(StringUtils.uppercaseSetences(db.getAlamatPenerimaFromAntaran(arrayListCheckedItem.get(0).getIdItem())));

        TextInputEditTextLatoRegular etAlamatBaru = (TextInputEditTextLatoRegular) view.findViewById(R.id.et_alamat_baru);
        TextInputEditTextLatoRegular etCatatan = (TextInputEditTextLatoRegular) view.findViewById(R.id.et_catatan);

        final TextInputEditTextLatoRegular etKeterangan = (TextInputEditTextLatoRegular) view.findViewById(R.id.et_keterangan);
        etKeterangan.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateName(etKeterangan)) {
                        alamatBaru = etAlamatBaru.getText().toString().trim();
                        catatan = etCatatan.getText().toString().trim();

                        adRecipient.dismiss();
                        showDialogSignature();
                    }
                }

                return false;
            }
        });

        ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateName(etKeterangan)) {
                    alamatBaru = etAlamatBaru.getText().toString().trim();
                    catatan = etCatatan.getText().toString().trim();

                    adRecipient.dismiss();
                    showDialogSignature();
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setView(view);
        adRecipient = builder.create();
        adRecipient.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        adRecipient.setCanceledOnTouchOutside(true);
        adRecipient.show();
    }

    private boolean validateName(TextInputEditTextLatoRegular etKeterangan) {
        Log.i(TAG, "validateName");
        keteranganBerhasil = etKeterangan.getText().toString().trim().toUpperCase();

        if (keteranganBerhasil.isEmpty() || keteranganBerhasil.length() == 0) {
            Toasty.error(this, getString(R.string.name_empty), 3000, false).show();
            return false;
        } else if (!keteranganBerhasil.matches(AppConfig.NAME_PATTERN)) {
            Toasty.error(this, getString(R.string.name_invalid_format), 3000, false).show();
            return false;
        }

        return true;
    }

    private void showDialogSignature() {
        Log.i(TAG, "showDialogSignature");
        View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_signature, null, false);

        final ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        btnPositive.setEnabled(false);
        btnPositive.setTextColor(ContextCompat.getColor(this, R.color.disableText));

        final ButtonLatoBold btnNegative = (ButtonLatoBold) view.findViewById(R.id.btn_negative);
        btnNegative.setEnabled(false);
        btnNegative.setTextColor(ContextCompat.getColor(this, R.color.disableText));

        final SignaturePad signaturePad = (SignaturePad) view.findViewById(R.id.signature_pad);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                btnNegative.setEnabled(true);
                btnPositive.setEnabled(true);

                btnNegative.setTextColor(ContextCompat.getColor(CollectiveActivity.this, R.color.colorPrimary));
                btnPositive.setTextColor(ContextCompat.getColor(CollectiveActivity.this, R.color.colorPrimary));
            }

            @Override
            public void onClear() {
                btnNegative.setEnabled(false);
                btnPositive.setEnabled(false);

                btnNegative.setTextColor(ContextCompat.getColor(CollectiveActivity.this, R.color.disableText));
                btnPositive.setTextColor(ContextCompat.getColor(CollectiveActivity.this, R.color.disableText));
            }
        });

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = signaturePad.getSignatureBitmap();

                if (addFileIntoStorage(bitmap)) {
                    adSignature.dismiss();

                    String message = "Apakah Anda ingin menambahkan foto?";
                    alertDialogConfirmation
                            .setMessage(message)
                            .setActionPositive("startCameraActivity")
                            .setActionNegative("showDialogValidation")
                            .showDialog();
                } else {
                    String message = "Simpan tanda tangan gagal. Silakan dicoba kembali";
                    Toasty.error(CollectiveActivity.this, message, 3000, false).show();
                }
            }
        });

        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setView(view);
        adSignature = builder.create();
        adSignature.setCanceledOnTouchOutside(true);
        adSignature.show();
    }

    private void convertToJpg(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat. JPEG, 80, stream);
        stream.close();
    }

    private void scanMediaFile(File fileName) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(fileName);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private boolean addFileIntoStorage(Bitmap bitmap) {
        boolean result = false;
        try {
            signatureFileName = "SIGN_" + arrayListCheckedItem.get(0).getIdItem() + ".jpg";
            signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(this, AppConfig.FOLDER_SIGNATURE), signatureFileName);

            convertToJpg(bitmap, signatureFile);
            scanMediaFile(signatureFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void startCameraActivity() {
        Log.i(TAG, "startCameraActivity");
        Intent intent = new Intent(this, CameraActivity.class);
        intent.putExtra("idItem", arrayListCheckedItem.get(0).getIdItem());
        startActivityForResult(intent, REQUEST_CODE_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CAMERA) {
            String action = data.getStringExtra("action");

            if (action.equalsIgnoreCase("save")) {
                File imageFile;

                if (isBerhasil) {
                    try {
                        isWithPhoto = true;
                        imageFile = new File(data.getStringExtra("imageFile"));
                        compressedImage = new ImageCompressor(this)
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(80)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setAlbumName(AppConfig.FOLDER_IMAGE)
                                .compressToFile(imageFile);

                        imageFileName = compressedImage.getName();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    showDialogValidation();
                } else {
                    try {
                        isWithPhoto = true;
                        imageFile = new File(data.getStringExtra("imageFile"));
                        compressedImage = new ImageCompressor(this)
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(80)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setAlbumName(AppConfig.FOLDER_IMAGE)
                                .compressToFile(imageFile);

                        imageFileName = compressedImage.getName();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    showDialogValidation();
                }
            }

            if (action.equalsIgnoreCase("cancel")) {
                // Do nothing
            }
        }
    }

    private void showDialogValidation() {
        Log.i(TAG, "showDialogValidation");
        View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_validation_collective, null, false);
        TextViewLatoRegular tvTotalItem = (TextViewLatoRegular) view.findViewById(R.id.tv_total_item);
        tvTotalItem.setText(String.valueOf(arrayListCheckedItem.size()));
        TextViewLatoRegular tvLabel2 = (TextViewLatoRegular) view.findViewById(R.id.tv_label2);
        TextViewLatoRegular tvKeterangan = (TextViewLatoRegular) view.findViewById(R.id.tv_keterangan);
        TextViewLatoRegular tvTandaTangan = (TextViewLatoRegular) view.findViewById(R.id.tv_tanda_tangan);
        TextViewLatoRegular tvFoto= (TextViewLatoRegular) view.findViewById(R.id.tv_foto);
        TextViewLatoRegular textView = (TextViewLatoRegular) view.findViewById(R.id.textview);
        ImageView ivTandaTangan = (ImageView) view.findViewById(R.id.iv_tanda_tangan);
        ImageView ivFoto = (ImageView) view.findViewById(R.id.iv_foto);

        if (isBerhasil) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            Bitmap signatureBitmap = BitmapFactory.decodeFile(String.valueOf(signatureFile), options);
            ivTandaTangan.setImageBitmap(signatureBitmap);
            Bitmap imageBitmap = BitmapFactory.decodeFile(String.valueOf(compressedImage), options);
            ivFoto.setImageBitmap(imageBitmap);

            tvLabel2.setText("Penerima");
            tvKeterangan.setText(keterangan + " [" + keteranganBerhasil + "]");

            if (!isWithPhoto) {
                imageFileName = "";
                textView.setVisibility(View.GONE);
                tvFoto.setVisibility(View.GONE);
                ivFoto.setVisibility(View.GONE);
            }
        } else {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            Bitmap imageBitmap = BitmapFactory.decodeFile(String.valueOf(compressedImage), options);
            ivFoto.setImageBitmap(imageBitmap);

            String id = idStatus.substring(0, 3);
            if (id.equals("G01")) {
                tvLabel2.setText("Alasan Retur");
            } else if (id.equals("G02")) {
                tvLabel2.setText("Alasan Dipanggil");
            } else if (id.equals("G03")) {
                tvLabel2.setText("Alasan Ditahan");
            } else if (id.equals("G04")) {
                tvLabel2.setText("Alasan Diteruskan");
            } else if (id.equals("G05")) {
                tvLabel2.setText("Alasan Antar Ulang");
            } else {
                tvLabel2.setText("Alasan Gagal");
            }

            keteranganGagal = kodeLn + " " + keterangan + " - " + tindakan;
            tvKeterangan.setText(keterangan);

            signatureFileName = "";
            tvTandaTangan.setVisibility(View.GONE);
            ivTandaTangan.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);
        }

        ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    adValidation.dismiss();
                    updateAntaranIntoServer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        ButtonLatoBold btnNegative = (ButtonLatoBold) view.findViewById(R.id.btn_negative);
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adValidation.dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setView(view);
        adValidation = builder.create();
        adValidation.setCanceledOnTouchOutside(true);
        adValidation.show();
    }

    private void updateAntaranIntoServer() throws IOException {
        Log.i(TAG, "updateAntaranIntoServer");
        alertDialogLoading.setCancelAble(false).showDialog();

        Date date = new Date(System.currentTimeMillis());
        final String currentDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(date);
        final String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        String mLatitude = locationPreferences.getString("latitude", null);
        String mLongitude = locationPreferences.getString("longitude", null);
        final String keteranganUpdate;

        if (mLatitude == null || mLongitude == null) {
            mLatitude = "0.000000";
            mLongitude = "0.000000";

            latitude = Double.parseDouble(mLatitude);
            longitude = Double.parseDouble(mLongitude);
        } else {
            latitude = Double.parseDouble(mLatitude);
            longitude = Double.parseDouble(mLongitude);
        }

        if (isBerhasil) {
            keteranganUpdate = keteranganBerhasil;
        } else {
            keteranganUpdate = keteranganGagal;
        }

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            for (int i=0; i<arrayListCheckedItem.size(); i++) {
                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put(AppConfig.UID, arrayListCheckedItem.get(i).getUid());
                jsonObject2.put(AppConfig.ID_ITEM, arrayListCheckedItem.get(i).getIdItem());
                jsonObject2.put(AppConfig.ID_DELIVERY_ORDER, arrayListCheckedItem.get(i).getIdDeliveryOrder());
                jsonObject2.put(AppConfig.ID_STATUS, idStatus);
                jsonObject2.put(AppConfig.WAKTU_UPDATE, currentDateTime);
                jsonObject2.put(AppConfig.GARIS_LINTANG, latitude);
                jsonObject2.put(AppConfig.GARIS_BUJUR, longitude);
                jsonObject2.put(AppConfig.TANDA_TANGAN, signatureFileName);
                jsonObject2.put(AppConfig.FOTO, imageFileName);
                jsonObject2.put(AppConfig.ID_PENGANTAR, idPengantar);
                jsonObject2.put(AppConfig.ID_KANTOR, idKantor);
                jsonObject2.put(AppConfig.KODE_DN, kodeDn);
                jsonObject2.put(AppConfig.TANGGAL_PERANGKAT, currentDate);
                jsonObject2.put(AppConfig.ALAMAT_BARU, alamatBaru);
                jsonObject2.put(AppConfig.CATATAN, catatan);

                if (isBerhasil) {
                    jsonObject2.put(AppConfig.KETERANGAN, keteranganBerhasil);
                } else {
                    jsonObject2.put(AppConfig.KETERANGAN, keteranganGagal);
                }

                jsonArray.put(jsonObject2);
            }

            jsonObject.put("action", "update_collective");
            jsonObject.put("data", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(AppConfig.MEDIA_TYPE_JSON, jsonObject.toString());

        Request request = new Request.Builder()
                .url(baseUrl + "antaran_collective.php")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(final Call call, IOException e) {
                CollectiveActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (currentDate.equals(dateFromSession)) {
                            int j = 0;
                            for (int i=0; i<arrayListCheckedItem.size(); i++) {
                                String uid = arrayListCheckedItem.get(i).getUid();

                                boolean isUpdated = db.updateIntoAntaran(
                                    new Antaran(
                                        uid, idStatus, keteranganUpdate, currentDateTime, latitude, longitude,
                                        signatureFileName, imageFileName, alamatBaru, catatan, "0"
                                    )
                                );

                                if (isUpdated) {
                                    j++;

                                    if (j == arrayListCheckedItem.size()) {
                                        alertDialogLoading.dismissDialog();

                                        String message = "Update status item berhasil (pending) dengan jumlah " + arrayListCheckedItem.size() + " item";
                                        Toasty.info(getApplicationContext(), message, 3000, false).show();

                                        resetAllValue();
                                        Intent intent = new Intent();
                                        intent.putExtra("action", "success");
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    }
                                }
                            }
                        } else {
                            String errorMessage =
                                    "Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server" +
                                    "\n\nTanggal Perangkat : " + MyDate.setDateTimeIndo(currentDate) + "\nTanggal Server       : " +
                                    MyDate.setDateTimeIndo(dateFromSession);

                            alertDialogLoading.dismissDialog();
                            alertDialogInformation
                                    .setMessage(errorMessage)
                                    .setActionPositive("backToMainActivity")
                                    .showDialog();
                        }
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(final ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        CollectiveActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (currentDate.equals(dateFromSession)) {
                                    int j = 0;
                                    for (int i=0; i<arrayListCheckedItem.size(); i++) {
                                        String uid = arrayListCheckedItem.get(i).getUid();

                                        boolean isUpdated = db.updateIntoAntaran(
                                            new Antaran(
                                                uid, idStatus, keteranganUpdate, currentDateTime, latitude, longitude,
                                                signatureFileName, imageFileName, alamatBaru, catatan, "0"
                                            )
                                        );

                                        if (isUpdated) {
                                            j++;

                                            if (j == arrayListCheckedItem.size()) {
                                                alertDialogLoading.dismissDialog();

                                                String message = "Update status item berhasil (pending) dengan jumlah " + arrayListCheckedItem.size() + " item";
                                                Toasty.info(getApplicationContext(), message, 3000, false).show();

                                                resetAllValue();
                                                Intent intent = new Intent();
                                                intent.putExtra("action", "success");
                                                setResult(RESULT_OK, intent);
                                                finish();
                                            }
                                        }
                                    }
                                } else {
                                    String errorMessage =
                                            "Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server" +
                                            "\n\nTanggal Perangkat : " + MyDate.setDateTimeIndo(currentDate) + "\nTanggal Server       : " +
                                            MyDate.setDateTimeIndo(dateFromSession);

                                    alertDialogLoading.dismissDialog();
                                    alertDialogInformation
                                            .setMessage(errorMessage)
                                            .setActionPositive("backToMainActivity")
                                            .showDialog();
                                }
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        CollectiveActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (isError) {
                                        String errorType = jsonObject.getString(AppConfig.ERROR_TYPE);
                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);

                                        if (errorType.equalsIgnoreCase("alert")) {
                                            alertDialogLoading.dismissDialog();
                                            alertDialogInformation
                                                    .setMessage(errorMessage)
                                                    .setActionPositive("backToMainActivity")
                                                    .showDialog();
                                        } else {
                                            int j = 0;
                                            for (int i=0; i<arrayListCheckedItem.size(); i++) {
                                                String uid = arrayListCheckedItem.get(i).getUid();

                                                boolean isUpdated = db.updateIntoAntaran(
                                                    new Antaran(
                                                        uid, idStatus, keteranganUpdate, currentDateTime, latitude,
                                                        longitude, signatureFileName, imageFileName, alamatBaru, catatan,
                                                        "0"
                                                    )
                                                );

                                                if (isUpdated) {
                                                    j++;

                                                    if (j == arrayListCheckedItem.size()) {
                                                        alertDialogLoading.dismissDialog();

                                                        String message = "Update status item berhasil (pending) dengan jumlah " + arrayListCheckedItem.size() + " item";
                                                        Toasty.info(getApplicationContext(), message, 3000, false).show();

                                                        resetAllValue();

                                                        Intent intent = new Intent();
                                                        intent.putExtra("action", "success");
                                                        setResult(RESULT_OK, intent);
                                                        finish();
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        String message = jsonObject.getString(AppConfig.MESSAGE);
                                        JSONArray antaran = jsonObject.getJSONArray("antaran");
                                        int j = 0;
                                        for (int i=0; i<antaran.length(); i++) {
                                            JSONObject object = antaran.getJSONObject(i);
                                            String uid = object.getString(AppConfig.UID);
                                            String signatureFileName = object.getString(AppConfig.TANDA_TANGAN);
                                            String imageFileName = object.getString(AppConfig.FOTO);

                                            boolean isUpdated = db.updateIntoAntaran(
                                                new Antaran(
                                                    uid, idStatus, keteranganUpdate, currentDateTime, latitude, longitude,
                                                    signatureFileName, imageFileName, alamatBaru, catatan,
                                                    "1"
                                                )
                                            );

                                            if (isUpdated) {
                                                j++;

                                                if (j == antaran.length()) {
                                                    alertDialogLoading.dismissDialog();
                                                    Toasty.success(CollectiveActivity.this, message, 3000, false).show();

                                                    uploadHTTP(CollectiveActivity.this, signatureFileName, imageFileName);
                                                    resetAllValue();

                                                    Intent intent = new Intent();
                                                    intent.putExtra("action", "success");
                                                    setResult(RESULT_OK, intent);
                                                    finish();
                                                }
                                            }
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void calculateFileSize(String currentDate, double signatureSize, double imageSize) {
        Log.i(TAG, "calculateFileSize");
        int signatureFile = 0;
        if (signatureSize > 0) {
            signatureFile = 1;
        }

        int imageFile = 0;
        if (imageSize > 0) {
            imageFile = 1;
        }

        int totalFile = imageFile + signatureFile;
        double totalSize = signatureSize + imageSize;

        int countRow = db.getCountFileUploadUsageByDate(currentDate);
        if (countRow > 0) {
            String statusUpdate = db.getStatusUpdateFromFileUploadUsage(currentDate);
            if (statusUpdate.equalsIgnoreCase("1")) {
                try {
                    submitFileUploadUsageIntoServer(totalFile, imageFile, signatureFile, totalSize, imageSize, signatureSize, "1");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                ArrayList<FileUploadUsage> arrayList;
                arrayList = db.getAllFileUploadUsageByDate(currentDate);

                int jmlBerkasLama = arrayList.get(0).getJmlBerkas();
                int jmlBerkasFotoLama = arrayList.get(0).getJmlBerkasFoto();
                int jmlBerkasTtdLama = arrayList.get(0).getJmlBerkasTtd();
                double jmlUkuranLama = arrayList.get(0).getJmlUkuran();
                double jmlUkuranFotoLama = arrayList.get(0).getJmlUkuranFoto();
                double jmlUkuranTtdLama = arrayList.get(0).getJmlUkuranTtd();

                int jmlBerkasBaru = jmlBerkasLama + totalFile;
                int jmlBerkasFotoBaru = jmlBerkasFotoLama + imageFile;
                int jmlBerkasTtdBaru = jmlBerkasTtdLama + signatureFile;
                double jmlUkuranBaru = jmlUkuranLama + totalSize;
                double jmlUkuranFotoBaru = jmlUkuranFotoLama + imageSize;
                double jmlUkuranTtdBaru = jmlUkuranTtdLama + signatureSize;

                try {
                    submitFileUploadUsageIntoServer(jmlBerkasBaru, jmlBerkasFotoBaru, jmlBerkasTtdBaru, jmlUkuranBaru, jmlUkuranFotoBaru, jmlUkuranTtdBaru, "0");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                submitFileUploadUsageIntoServer(totalFile, imageFile, signatureFile, totalSize, imageSize, signatureSize, "1");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onFailureSubmitDataUsage(
            String currentDate, String uid, int jmlBerkas, int jmlBerkasFoto, int jmlBerkasTtd, double jmlUkuran,
            double jmlUkuranFoto, double jmlUkuranTtd
    ) {
        Log.i(TAG, "onFailureSubmitDataUsage");
        int countRow = db.getCountFileUploadUsageByDate(currentDate);
        if (countRow > 0) {
            ArrayList<FileUploadUsage> arrayList;
            arrayList = db.getAllFileUploadUsageByDate(currentDate);

            String uidLama = arrayList.get(0).getUid();
            int jmlBerkasLama = arrayList.get(0).getJmlBerkas();
            int jmlBerkasFotoLama = arrayList.get(0).getJmlBerkasFoto();
            int jmlBerkasTtdLama = arrayList.get(0).getJmlBerkasTtd();
            double jmlUkuranLama = arrayList.get(0).getJmlUkuran();
            double jmlUkuranFotoLama = arrayList.get(0).getJmlUkuranFoto();
            double jmlUkuranTtdLama = arrayList.get(0).getJmlUkuranTtd();

            String statusUpdate = db.getStatusUpdateFromFileUploadUsage(currentDate);
            if (statusUpdate.equalsIgnoreCase("1")) {
                int jmlBerkasBaru = jmlBerkasLama + jmlBerkas;
                int jmlBerkasFotoBaru = jmlBerkasFotoLama + jmlBerkasFoto;
                int jmlBerkasTtdBaru = jmlBerkasTtdLama + jmlBerkasTtd;
                double jmlUkuranBaru = jmlUkuranLama + jmlUkuran;
                double jmlUkuranFotoBaru = jmlUkuranFotoLama + jmlUkuranFoto;
                double jmlUkuranTtdBaru = jmlUkuranTtdLama + jmlUkuranTtd;

                db.updateIntoFileUploadUsage(
                    new FileUploadUsage(
                        uidLama, jmlBerkasBaru, jmlBerkasFotoBaru, jmlBerkasTtdBaru, jmlUkuranBaru, jmlUkuranFotoBaru,
                        jmlUkuranTtdBaru, "0"
                    )
                );
            } else {
                db.updateIntoFileUploadUsage(
                    new FileUploadUsage(
                        uidLama, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd,
                        "0"
                    )
                );
            }
        } else {
            db.insertIntoFileUploadUsage(
                new FileUploadUsage(
                    uid, currentDate, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran,
                    jmlUkuranFoto, jmlUkuranTtd, "0", idPengantar
                )
            );
        }
    }

    private void submitFileUploadUsageIntoServer(
            final int jmlBerkas, final int jmlBerkasFoto, final int jmlBerkasTtd, final double jmlUkuran,
            final double jmlUkuranFoto, final double jmlUkuranTtd, String statusUpdate
    ) throws IOException {
        Log.i(TAG, "submitFileUploadUsageIntoServer");
        Date currentDate = new Date();
        final String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(currentDate);
        final String uid = UUID.randomUUID().toString().replace("-",".").substring(0,20);

        RequestBody formBody = new FormBody.Builder()
                .add("action", "submit")
                .add(AppConfig.UID, uid)
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .add(AppConfig.TANGGAL, date)
                .add(AppConfig.JML_BERKAS, String.valueOf(jmlBerkas))
                .add(AppConfig.JML_BERKAS_FOTO, String.valueOf(jmlBerkasFoto))
                .add(AppConfig.JML_BERKAS_TTD, String.valueOf(jmlBerkasTtd))
                .add(AppConfig.JML_UKURAN, String.valueOf(jmlUkuran))
                .add(AppConfig.JML_UKURAN_FOTO, String.valueOf(jmlUkuranFoto))
                .add(AppConfig.JML_UKURAN_TTD, String.valueOf(jmlUkuranTtd))
                .add(AppConfig.STATUS_UPDATE, statusUpdate)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "file_upload_usage.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFailureSubmitDataUsage(date, uid, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd);

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        //throw new IOException("Unexpected code " + response.code() + " : " + response.message());
                        onFailureSubmitDataUsage(date, uid, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd);
                    } else {
                        final String responseData = responseBody.string();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (!isError) {
                                        JSONObject fileUploadUsage = jsonObject.getJSONObject("file_upload_usage");
                                        String action = fileUploadUsage.getString("action");
                                        String uidServer = fileUploadUsage.getString(AppConfig.UID);
                                        String tanggalServer = fileUploadUsage.getString(AppConfig.TANGGAL);
                                        int jmlBerkasServer = fileUploadUsage.getInt(AppConfig.JML_BERKAS);
                                        int jmlBerkasFotoServer = fileUploadUsage.getInt(AppConfig.JML_BERKAS_FOTO);
                                        int jmlBerkasTtdServer = fileUploadUsage.getInt(AppConfig.JML_BERKAS_TTD);
                                        double jmlUkuranServer = fileUploadUsage.getDouble(AppConfig.JML_UKURAN);
                                        double jmlUkuranFotoServer = fileUploadUsage.getDouble(AppConfig.JML_UKURAN_FOTO);
                                        double jmlUkuranTtdServer = fileUploadUsage.getDouble(AppConfig.JML_UKURAN_TTD);

                                        if (action.equalsIgnoreCase("insert")) {
                                            db.insertIntoFileUploadUsage(
                                                new FileUploadUsage(
                                                    uidServer, tanggalServer, jmlBerkasServer, jmlBerkasFotoServer, jmlBerkasTtdServer,
                                                    jmlUkuranServer, jmlUkuranFotoServer, jmlUkuranTtdServer, "1", idPengantar
                                                )
                                            );
                                        } else {
                                            db.updateIntoFileUploadUsage(
                                                new FileUploadUsage(
                                                    uidServer, jmlBerkasServer, jmlBerkasFotoServer, jmlBerkasTtdServer, jmlUkuranServer,
                                                    jmlUkuranFotoServer, jmlUkuranTtdServer, "1"
                                                )
                                            );
                                        }
                                    } else {
                                        onFailureSubmitDataUsage(date, uid, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void uploadHTTP(Context context, String signatureFileName, String imageFileName) {
        Log.i(TAG, "uploadHTTP");
        Date date = new Date();
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        String year = currentDate.substring(0, 4);
        String month = currentDate.substring(5, 7);
        String day = currentDate.substring(8, 10);
        remotePath = "/" + year + month + "/" + day + "/";

        MyFile myFile = new MyFile(context);
        File signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_SIGNATURE), signatureFileName);
        File imageFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_IMAGE), imageFileName);

        if (isBerhasil) {
            if (isWithPhoto) {
                double signatureSize = (double) signatureFile.length() / 1024;
                double imageSize = (double) imageFile.length() / 1024;
                calculateFileSize(currentDate, signatureSize, imageSize);

                uploadHTTPSignatureFile(signatureFile);
                uploadHTTPImageFile(imageFile);
            } else {
                double signatureSize = (double) signatureFile.length() / 1024;
                calculateFileSize(currentDate, signatureSize, 0);

                uploadHTTPSignatureFile(signatureFile);
            }
        } else {
            double imageSize = (double) imageFile.length() / 1024;
            calculateFileSize(currentDate, 0, imageSize);

            uploadHTTPImageFile(imageFile);
        }
    }

    private void uploadHTTPSignatureFile(File fileName) {
        Log.i(TAG, "uploadHTTPSignatureFile");
        String signatureUploadID = myUploadFile.UploadHTTP(
                this,
                String.valueOf(fileName),
                remotePath
        );

        db.insertIntoFileUploadError(new FileUploadError(signatureUploadID, signatureFileName, remotePath, idPengantar));
    }

    private void uploadHTTPImageFile(File fileName) {
        Log.i(TAG, "uploadHTTPImageFile");
        String imageUploadID = myUploadFile.UploadHTTP(
                this,
                String.valueOf(fileName),
                remotePath
        );

        db.insertIntoFileUploadError(new FileUploadError(imageUploadID, imageFileName, remotePath, idPengantar));
    }

    /*private void uploadFTP(Context context, String signatureFileName, String imageFileName) {
        Log.i(TAG, "uploadFTP");
        Date currentDate = new Date();
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(currentDate);
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);
        remotePath = "/" + year + month + "/" + day + "/";

        MyFile myFile = new MyFile(context);
        File signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_SIGNATURE), signatureFileName);
        File imageFile = new File(myFile.getFolderPicturesOnStorageDirectory(context, AppConfig.FOLDER_IMAGE), imageFileName);

        if (isBerhasil) {
            if (isWithPhoto) {
                double signatureSize = (double) signatureFile.length() / 1024;
                double imageSize = (double) imageFile.length() / 1024;
                calculateFileSize(date, signatureSize, imageSize);

                uploadFTPSignatureFile(signatureFile);
                uploadFTPImageFile(imageFile);
            } else {
                double signatureSize = (double) signatureFile.length() / 1024;
                calculateFileSize(date, signatureSize, 0);

                uploadFTPSignatureFile(signatureFile);
            }
        } else {
            double imageSize = (double) imageFile.length() / 1024;
            calculateFileSize(date, 0, imageSize);

            uploadFTPImageFile(imageFile);
        }
    }

    private void uploadFTPSignatureFile(File fileName) {
        Log.i(TAG, "uploadFTPSignatureFile");
        String signatureUploadID = myUploadFile.uploadFTP(
                this,
                AppConfig.FTP_HOST,
                Integer.parseInt(AppConfig.FTP_PORT),
                AppConfig.FTP_USERNAME,
                AppConfig.FTP_PASSWORD,
                String.valueOf(fileName),
                remotePath
        );

        db.insertIntoFileUploadError(new FileUploadError(signatureUploadID, signatureFileName, remotePath, idPengantar));
    }

    private void uploadFTPImageFile(File fileName) {
        Log.i(TAG, "uploadFTPImageFile");
        String imageUploadID = myUploadFile.uploadFTP(
                this,
                AppConfig.FTP_HOST,
                Integer.parseInt(AppConfig.FTP_PORT),
                AppConfig.FTP_USERNAME,
                AppConfig.FTP_PASSWORD,
                String.valueOf(fileName),
                remotePath
        );

        db.insertIntoFileUploadError(new FileUploadError(imageUploadID, imageFileName, remotePath, idPengantar));
    }*/

    private void resetAllValue() {
        Log.i(TAG, "resetAllValue");
        idStatus ="";
        keterangan ="";
        keteranganBerhasil = "";
        keteranganGagal = "";
        latitude = 0.000000;
        longitude = 0.000000;
        signatureFileName = "";
        imageFileName = "";
        alamatBaru = "";
        catatan = "";
        isBerhasil = false;
        isWithPhoto = false;
        arrayList.clear();
        arrayListCheckedItem.clear();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isGPSEnabled()) {
            alertDialogInformation
                    .setMessage(getResources().getString(R.string.message_gps))
                    .setActionPositive("locationSetting")
                    .showDialog();
        } else {
            if (locationManager == null) {
                locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                startLocation();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (locationManager != null) {
            stopLocation();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.collective, menu);
        searchView = (SearchView) menu.findItem(R.id.nav_search).getActionView();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                intent.putExtra("action", "cancel");
                setResult(RESULT_OK, intent);
                finish();

                return true;
            case R.id.nav_search:
                searchIdItem(item);
                return true;
            case R.id.nav_update:
                if (!isGPSEnabled()) {
                    alertDialogInformation
                            .setMessage(getResources().getString(R.string.message_gps))
                            .setActionPositive("locationSetting")
                            .showDialog();
                } else {
                    if (arrayListCheckedItem.size() > 0) {
                        showDialogUpdateStatus();
                    } else {
                        String message = "Belum ada item yang dipilih. Pilih item terlebih dahulu sebelum melakukan update status";
                        alertDialogInformation
                                .setMessage(message)
                                .showDialog();
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCheckboxChecked(int position) {
        String uid = arrayList.get(position).getUid();
        String idItem = arrayList.get(position).getIdItem();
        String idDeliveryOrder = arrayList.get(position).getIdDeliveryOrder();
        String waktuEntri = arrayList.get(position).getWaktuEntri();
        arrayListCheckedItem.add(new ItemCollective(uid, idItem, idDeliveryOrder, waktuEntri));

        if (arrayListCheckedItem.size() > 0) {
            tvTitleToolbar.setText(getResources().getString(R.string.title_collective) + " | " + arrayListCheckedItem.size() + " ITEM");
        } else {
            tvTitleToolbar.setText(getResources().getString(R.string.title_collective));
        }
    }

    @Override
    public void onCheckboxUnchecked(int position) {
        Iterator<ItemCollective> iterator = arrayListCheckedItem.iterator();
        while (iterator.hasNext()) {
            String idItem = arrayList.get(position).getIdItem();
            if (iterator.next().getIdItem().equals(idItem)) {
                iterator.remove();
            }
        }

        if (arrayListCheckedItem.size() == 0) {
            tvTitleToolbar.setText(getResources().getString(R.string.title_collective));
        } else {
            tvTitleToolbar.setText(getResources().getString(R.string.title_collective) + " | " + arrayListCheckedItem.size() + " ITEM");
        }
    }

    @Override
    public void positiveButtonClickedOnAlertDialogInformation(String action) {
        if (action.equalsIgnoreCase("backToMainActivity")) {
            Intent intent = new Intent();
            intent.putExtra("action", "success");
            setResult(RESULT_OK, intent);
            finish();
        }

        if (action.equalsIgnoreCase("locationSetting")) {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void positiveButtonClickedOnAlertDialogConfirmation(String action) {
        if (action.equalsIgnoreCase("startCameraActivity")) {
            startCameraActivity();
        }
    }

    @Override
    public void negativeButtonClickedOnAlertDialogConfirmation(String action) {
        if (action.equalsIgnoreCase("showDialogValidation")) {
            showDialogValidation();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        Intent intent = new Intent();
        intent.putExtra("action", "cancel");
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.checkbox_all:
                CheckBox checkBoxAll = (CheckBox) v.findViewById(R.id.checkbox_all);
                arrayListCheckedItem.clear();

                if (checkBoxAll.isChecked()) {
                     for (int i=0; i<=arrayList.size() - 1; i++) {
                         adapter.addSelection(i, true);
                    }

                    tvTitleToolbar.setText(getResources().getString(R.string.title_collective) + " | " + arrayListCheckedItem.size() + " ITEM");
                } else {
                    adapter.removeSelection();
                    arrayListCheckedItem.clear();

                    tvTitleToolbar.setText(getResources().getString(R.string.title_collective));
                }


                break;*/
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        locationEditor = locationPreferences.edit();
        locationEditor.clear();
        locationEditor.apply();

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        locationEditor = locationPreferences.edit();
        locationEditor.putString("latitude", String.valueOf(latitude));
        locationEditor.putString("longitude", String.valueOf(longitude));
        locationEditor.apply();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
