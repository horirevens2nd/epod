package horirevens.com.epod.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.horirevens.mylibrary.fonts.TextViewLatoBold;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import horirevens.com.epod.AppConfig;
import horirevens.com.epod.LoginSession;
import horirevens.com.epod.R;
import horirevens.com.epod.adapters.FileUploadUsageAdapter;
import horirevens.com.epod.helpers.DatabaseHelper;
import horirevens.com.epod.helpers.ExceptionHandler;
import horirevens.com.epod.models.FileUploadUsage;

public class FileUploadUsageActivity extends AppCompatActivity implements
        FileUploadUsageAdapter.DataUsageListener {
    private static final String TAG = FileUploadUsageActivity.class.getName();

    private FileUploadUsageAdapter adapter;
    private ArrayList<FileUploadUsage> arrayList = new ArrayList<>();
    private DatabaseHelper db;

    private ConstraintLayout constraintLayout;
    private RecyclerView recyclerView;
    
    private String baseUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_file_upload_usage);
        Log.i(TAG, "onCreate");

        if (getIntent().getBooleanExtra("exit", false)) {
            finish();
        }

        db = new DatabaseHelper(this);

        LoginSession loginSession = new LoginSession(this);
        HashMap<String, String> hashMap = loginSession.getSession();
        baseUrl = hashMap.get(AppConfig.BASE_URL);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextViewLatoBold tvTitleToolbar = (TextViewLatoBold) findViewById(R.id.tv_title);
        tvTitleToolbar.setText(getResources().getString(R.string.title_file_upload_usage));

        constraintLayout = (ConstraintLayout) findViewById(R.id.info_container);
        constraintLayout.setVisibility(View.GONE);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Date date = new Date(System.currentTimeMillis());
                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);

                arrayList.clear();
                int countRow = db.getCountFileUploadUsage();

                if (countRow > 0) {
                    constraintLayout.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    arrayList = db.getAllFileUploadUsage(currentDate);
                    adapter = new FileUploadUsageAdapter(FileUploadUsageActivity.this, arrayList, FileUploadUsageActivity.this);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    constraintLayout.setVisibility(View.VISIBLE);
                }
            }
        }, 500);
    }

    @Override
    public void OnItemClicked() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(FileUploadUsageActivity.this, MainActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(FileUploadUsageActivity.this, MainActivity.class));
        finish();
    }
}
