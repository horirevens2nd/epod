package horirevens.com.epod.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.HashMap;

import com.horirevens.mylibrary.fonts.TextViewLatoRegular;
import horirevens.com.epod.AppConfig;
import horirevens.com.epod.BuildConfig;
import horirevens.com.epod.LoginSession;
import horirevens.com.epod.R;
import horirevens.com.epod.helpers.ExceptionHandler;

public class SplashScreenActivity extends AppCompatActivity {

    private static final String TAG = SplashScreenActivity.class.getName();

    private LoginSession loginSession;

    private String idLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_splash_screen);
        Log.i(TAG, "onCreate");

        loginSession = new LoginSession(this);
        HashMap<String, String> hashMap = loginSession.getSession();
        idLogin = hashMap.get(AppConfig.ID_LOGIN);

        TextViewLatoRegular tvVersion = (TextViewLatoRegular) findViewById(R.id.tv_version);
        tvVersion.setText("versi " + BuildConfig.VERSION_NAME);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (idLogin != null) {
                    loginSession.checkLogin();
                    finish();
                } else {
                    startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }, 3000);
    }
}
