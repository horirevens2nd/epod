package horirevens.com.epod.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.telephony.TelephonyManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.*;

import com.horirevens.mylibrary.fonts.ButtonLatoBold;
import com.horirevens.mylibrary.fonts.TextInputEditTextLatoRegular;
import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import es.dmoral.toasty.Toasty;
import horirevens.com.epod.AppConfig;
import horirevens.com.epod.BuildConfig;
import horirevens.com.epod.DateSession;
import horirevens.com.epod.LoginSession;
import horirevens.com.epod.MainApplication;
import horirevens.com.epod.R;
import horirevens.com.epod.helpers.DatabaseHelper;
import horirevens.com.epod.helpers.ExceptionHandler;
import horirevens.com.epod.libraries.AlertDialogInformation;
import horirevens.com.epod.libraries.AlertDialogLoading;
import horirevens.com.epod.models.Antaran;
import horirevens.com.epod.models.FileUploadUsage;
import horirevens.com.epod.models.FileUploadError;
import horirevens.com.epod.models.Status;
import horirevens.com.epod.models.StringWithTag;
import horirevens.com.epod.receivers.NetworkReceiver;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class LoginActivity extends AppCompatActivity implements
        AlertDialogInformation.AlertDialogInformationListener,
        NetworkReceiver.NetworkReceiverListener {
    private static final String TAG = LoginActivity.class.getName();
    private static final String[] PERMISSIONS_REQUIRED = new String[] {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int PERMISSION_CALLBACK_CONSTANT = 101;
    private static final String BASE_URL = "http://104.215.144.200:180/epodv441_api/";
    //private static final String BASE_URL = "https://epod.posindonesia.co.id:2443/epodv440_api/";
    private static final String SERVER = "CA";

    private AlertDialogInformation alertDialogInformation;
    private AlertDialogLoading alertDialogLoading;
    private AlertDialog alertDialogDownloading;
    private OkHttpClient client;
    private LoginSession loginSession;
    private DateSession dateSession;
    private DatabaseHelper db;
    private Date date;

    private String strIdLogin, strPassword, currentDate;
    private String idPengantar, nama, idLogin, idKantor, idMandor, namaMandor, idGrup;
    
    private TextInputEditTextLatoRegular etIdLogin, etPassword;
    private TextViewLatoRegular tvMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_login);
        Log.i(TAG, "onCreate");

        if (getIntent().getBooleanExtra("exit", false)) {
            finish();
        }

        alertDialogInformation = new AlertDialogInformation(this, this);
        alertDialogLoading = new AlertDialogLoading(this);
        loginSession = new LoginSession(this);
        dateSession = new DateSession(this);
        db = new DatabaseHelper(this);
        client = MainApplication.getUnsafeOkHttpClient();
        date = new Date(System.currentTimeMillis());
        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);

        TextViewLatoRegular tvVersion = (TextViewLatoRegular) findViewById(R.id.tv_version);
        tvVersion.setText("versi " + BuildConfig.VERSION_NAME);

        etIdLogin = (TextInputEditTextLatoRegular) findViewById(R.id.et_id_login);

        etPassword = (TextInputEditTextLatoRegular) findViewById(R.id.et_password);
        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateIdLogin() && validatePassword()) {
                        try {
                            validateLogin();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                return false;
            }
        });

        AppCompatCheckBox checkBox = (AppCompatCheckBox) findViewById(R.id.cb_show_password);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        ButtonLatoBold btnLogin = (ButtonLatoBold) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateIdLogin() && validatePassword()) {
                    try {
                        validateLogin();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        validatePermissions();
    }

    private boolean isConnected() {
        return NetworkReceiver.isConnected();
    }

    @SuppressLint("MissingPermission")
    private String getDeviceImei() {
        Log.i(TAG, "getDeviceImei");
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    private void validateLogin() throws IOException {
        Log.i(TAG, "validateLogin");
        alertDialogLoading.setCancelAble(false).showDialog();

        String parInput = strIdLogin + "#" + strPassword + "#" + getDeviceImei() + "#" + BuildConfig.VERSION_NAME +
                "#" + SERVER + "#" + currentDate;

        RequestBody formBody = new FormBody.Builder()
                .add("parInput", parInput)
                .build();

        Request request = new Request.Builder()
                //.url("https://httpbin.org/delay/10")
                .url(BASE_URL + "pengantar.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogLoading.dismissDialog();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogLoading.dismissDialog();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        alertDialogLoading.dismissDialog();
                        final String responseData = responseBody.string();

                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    final boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (isError) {
                                        String errorCode = jsonObject.getString(AppConfig.ERROR_CODE);
                                        String errorMessage;

                                        if (errorCode.equalsIgnoreCase("001")) {
                                            errorMessage = getResources().getString(R.string.get_pengantar_001);
                                        } else if (errorCode.equalsIgnoreCase("002")) {
                                            errorMessage = getResources().getString(R.string.get_pengantar_002);
                                        } else if (errorCode.equalsIgnoreCase("003")) {
                                            errorMessage = getResources().getString(R.string.get_pengantar_003);
                                        } else if (errorCode.equalsIgnoreCase("004")) {
                                            errorMessage = getResources().getString(R.string.get_pengantar_004);
                                        } else if (errorCode.equalsIgnoreCase("005") || errorCode.equalsIgnoreCase("")) {
                                            errorMessage = getResources().getString(R.string.get_pengantar_005);
                                        } else if (errorCode.equalsIgnoreCase("006")) {
                                            errorMessage = getResources().getString(R.string.get_pengantar_006);
                                        } else if (errorCode.equalsIgnoreCase("999")) {
                                            errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);
                                        } else {
                                            errorMessage = getResources().getString(R.string.undefined_error);
                                        }

                                        alertDialogInformation.setMessage(errorMessage).showDialog();
                                    } else {
                                        JSONObject pengantar = jsonObject.getJSONObject("pengantar");
                                        boolean isError2 = jsonObject.getBoolean(AppConfig.ERROR);

                                        if (!isError2) {
                                            dateSession.setSession(currentDate);

                                            idPengantar = pengantar.getString(AppConfig.ID);
                                            nama = pengantar.getString(AppConfig.NAMA);
                                            idLogin = pengantar.getString(AppConfig.ID_LOGIN);
                                            idKantor = pengantar.getString(AppConfig.ID_KANTOR);
                                            idMandor = pengantar.getString(AppConfig.ID_MANDOR);
                                            namaMandor = pengantar.getString(AppConfig.NAMA_MANDOR);
                                            idGrup = pengantar.getString(AppConfig.ID_GRUP);

                                            int countAntaran = db.getCountAntaran(currentDate);
                                            int countStatus = db.getCountStatus();
                                            int countFileUploadUsage = db.getCountFileUploadUsage();
                                            int countFileUploadError = db.getCountFileUploadError();

                                            if (countAntaran == 0 && countFileUploadUsage == 0 && countFileUploadError == 0) {
                                                showAlertDialogDownloading();
                                                getAllAntaranFromServer();
                                            } else {
                                                String idPengantarFromSqlite = db.getIdPengantarFromAntaran();
                                                if (!idPengantarFromSqlite.equals(idPengantar)) {
                                                    if (db.truncate("antaran") || db.truncate("file_upload_usage") ||
                                                        db.truncate("file_upload_error")) {
                                                        showAlertDialogDownloading();
                                                        getAllAntaranFromServer();
                                                    }
                                                } else {
                                                    showAlertDialogDownloading();
                                                    getAllAntaranFromServer();
                                                }
                                            }
                                        } else {
                                            String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);
                                            alertDialogInformation
                                                    .setMessage(errorMessage)
                                                    .showDialog();
                                        }
                                    }
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void showAlertDialogDownloading() {
        View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_downloading, null);
        tvMessage = (TextViewLatoRegular) view.findViewById(R.id.tv_message);
        tvMessage.setText("Mengunduh data...");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        alertDialogDownloading = builder.create();
        alertDialogDownloading.setCancelable(true);
        alertDialogDownloading.setCanceledOnTouchOutside(true);
        alertDialogDownloading.show();
    }

    private void getAllAntaranFromServer() throws IOException {
        Log.i(TAG, "getAllAntaranFromServer");
        tvMessage.setText("Mengunduh data \"Antaran\"");
        RequestBody formBody = new FormBody.Builder()
                .add("action", "get_all")
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "antaran.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogDownloading.dismiss();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogDownloading.dismiss();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (!isError) {
                                        String message = jsonObject.getString(AppConfig.MESSAGE);

                                        if (message.equalsIgnoreCase("not_empty")) {
                                            JSONArray antaran = jsonObject.getJSONArray("antaran");
                                            int countItem = antaran.length();
                                            int j = 0;

                                            for (int i=0; i<countItem; i++) {
                                                double mGarisLintang, mGarisBujur, mBerat;
                                                double mBsuCod,  mBsuBlb, mBsuTax;

                                                JSONObject object = antaran.getJSONObject(i);
                                                String uid = object.getString(AppConfig.UID);
                                                String idItem = object.getString(AppConfig.ID_ITEM);
                                                String idDeliveryOrder = object.getString(AppConfig.ID_DELIVERY_ORDER);
                                                String idStatus = object.getString(AppConfig.ID_STATUS);
                                                String keterangan = object.getString(AppConfig.KETERANGAN);
                                                if (keterangan == null || keterangan.equalsIgnoreCase("null")) {
                                                    keterangan = "";
                                                }
                                                String waktuEntri = object.getString(AppConfig.WAKTU_ENTRI);
                                                String waktuUpdate = object.getString(AppConfig.WAKTU_UPDATE);
                                                if (waktuUpdate == null || waktuUpdate.equalsIgnoreCase("null")) {
                                                    waktuUpdate = "";
                                                }
                                                String garisLintang = object.getString(AppConfig.GARIS_LINTANG);
                                                if (garisLintang == null || garisLintang.equalsIgnoreCase("null")) {
                                                    mGarisLintang = 0.000000;
                                                } else {
                                                    mGarisLintang = Double.parseDouble(garisLintang);
                                                }
                                                String garisBujur = object.getString(AppConfig.GARIS_BUJUR);
                                                if (garisBujur == null || garisBujur.equalsIgnoreCase("null")) {
                                                    mGarisBujur = 0.000000;
                                                } else {
                                                    mGarisBujur = Double.parseDouble(garisBujur);
                                                }
                                                String tandaTangan = object.getString(AppConfig.TANDA_TANGAN);
                                                if (tandaTangan == null || tandaTangan.equalsIgnoreCase("null")) {
                                                    tandaTangan = "";
                                                }
                                                String foto = object.getString(AppConfig.FOTO);
                                                if (foto == null || foto.equalsIgnoreCase("null")) {
                                                    foto = "";
                                                }
                                                String namaPengirim = object.getString(AppConfig.NAMA_PENGIRIM);
                                                if (namaPengirim == null || namaPengirim.equalsIgnoreCase("null")) {
                                                    namaPengirim = "";
                                                }
                                                String alamatPengirim = object.getString(AppConfig.ALAMAT_PENGIRIM);
                                                if (alamatPengirim == null || alamatPengirim.equalsIgnoreCase("null")) {
                                                    alamatPengirim = "";
                                                }
                                                String hpPengirim = object.getString(AppConfig.HP_PENGIRIM);
                                                if (hpPengirim == null || hpPengirim.equalsIgnoreCase("null")) {
                                                    hpPengirim = "";
                                                }
                                                String namaPenerima = object.getString(AppConfig.NAMA_PENERIMA);
                                                if (namaPenerima == null || namaPenerima.equalsIgnoreCase("null")) {
                                                    namaPenerima = "";
                                                }
                                                String alamatPenerima = object.getString(AppConfig.ALAMAT_PENERIMA);
                                                if (alamatPenerima == null || alamatPenerima.equalsIgnoreCase("null")) {
                                                    alamatPenerima = "";
                                                }
                                                String hpPenerima = object.getString(AppConfig.HP_PENERIMA);
                                                if (hpPenerima == null || hpPenerima.equalsIgnoreCase("null")) {
                                                    hpPenerima = "";
                                                }
                                                String idProduk = object.getString(AppConfig.ID_PRODUK);
                                                if (idProduk == null || idProduk.equalsIgnoreCase("null")) {
                                                    idProduk = "";
                                                }
                                                String berat = object.getString(AppConfig.BERAT);
                                                if (berat == null || berat.equalsIgnoreCase("null")) {
                                                    mBerat = 0.0;
                                                } else {
                                                    mBerat = Double.parseDouble(berat);
                                                }
                                                String bsuCod = object.getString(AppConfig.BSU_COD);
                                                if (bsuCod == null || bsuCod.equalsIgnoreCase("null")) {
                                                    mBsuCod = 0;
                                                } else {
                                                    mBsuCod = Double.parseDouble(bsuCod);
                                                }
                                                String bsuBlb = object.getString(AppConfig.BSU_BLB);
                                                if (bsuBlb == null || bsuBlb.equalsIgnoreCase("null")) {
                                                    mBsuBlb = 0;
                                                } else {
                                                    mBsuBlb = Double.parseDouble(bsuBlb);
                                                }
                                                String bsuTax = object.getString(AppConfig.BSU_TAX);
                                                if (bsuTax == null || bsuTax.equalsIgnoreCase("null")) {
                                                    mBsuTax = 0;
                                                } else {
                                                    mBsuTax = Double.parseDouble(bsuTax);
                                                }
                                                String kantorAsal = object.getString(AppConfig.KANTOR_ASAL);
                                                if (kantorAsal == null || kantorAsal.equalsIgnoreCase("null")) {
                                                    kantorAsal = "";
                                                }
                                                String kantorTujuan = object.getString(AppConfig.KANTOR_TUJUAN);
                                                if (kantorTujuan == null || kantorTujuan.equalsIgnoreCase("null")) {
                                                    kantorTujuan = "";
                                                }
                                                String idCustomer = object.getString(AppConfig.ID_CUSTOMER);
                                                if (idCustomer == null || idCustomer.equalsIgnoreCase("null")) {
                                                    idCustomer = "";
                                                }
                                                String idExternal = object.getString(AppConfig.ID_EXTERNAL);
                                                if (idExternal == null || idExternal.equalsIgnoreCase("null")) {
                                                    idExternal = "";
                                                }
                                                String tglPosting = object.getString(AppConfig.ID_EXTERNAL);
                                                if (tglPosting == null || tglPosting.equalsIgnoreCase("null")) {
                                                    tglPosting = "";
                                                }
                                                String tipe = object.getString(AppConfig.TIPE);
                                                if (tipe == null || tipe.equalsIgnoreCase("null")) {
                                                    tipe = "";
                                                }
                                                String alamatBaru = object.getString(AppConfig.ALAMAT_BARU);
                                                if (alamatBaru == null || alamatBaru.equalsIgnoreCase("null")) {
                                                    alamatBaru = "";
                                                }
                                                String catatan = object.getString(AppConfig.CATATAN);
                                                if (catatan == null || catatan.equalsIgnoreCase("null")) {
                                                    catatan = "";
                                                }
                                                String statusTutup = object.getString(AppConfig.TUTUP);
                                                String statusUpdate = idStatus.equalsIgnoreCase("P01") ? "0" : "1";

                                                if (db.isExistOnAntaran(idItem, idDeliveryOrder)) {
                                                    if (idStatus.equalsIgnoreCase("P01")) {
                                                        String mStatusUpdate = db.getStatusUpdateFromAntaran(idItem, idDeliveryOrder);
                                                        if (mStatusUpdate.equalsIgnoreCase("1")) {
                                                            db.updateIntoAntaran(
                                                                new Antaran(
                                                                    uid, idStatus, keterangan, waktuUpdate, mGarisLintang,
                                                                    mGarisBujur, tandaTangan, foto, alamatBaru, catatan,
                                                                    statusUpdate
                                                                )
                                                            );
                                                            j++;
                                                        } else {
                                                            j++;
                                                        }
                                                    } else {
                                                        db.updateIntoAntaran(
                                                            new Antaran(
                                                                uid, idStatus, keterangan, waktuUpdate, mGarisLintang,
                                                                mGarisBujur, tandaTangan, foto, alamatBaru, catatan,
                                                                statusUpdate
                                                            )
                                                        );
                                                        j++;
                                                    }
                                                } else {
                                                    db.insertIntoAntaran(
                                                        new Antaran(
                                                            uid, idItem, idDeliveryOrder, idStatus, keterangan,
                                                            waktuEntri, waktuUpdate, mGarisLintang, mGarisBujur,
                                                            tandaTangan, foto, namaPengirim, alamatPengirim,
                                                            hpPengirim, namaPenerima, alamatPenerima, hpPenerima,
                                                            idProduk, mBerat, mBsuCod, mBsuBlb, mBsuTax, kantorAsal,
                                                            kantorTujuan, idCustomer, idExternal, tglPosting, tipe,
                                                            alamatBaru, catatan, statusTutup, statusUpdate, idPengantar
                                                        )
                                                    );
                                                    j++;
                                                }

                                                if (j == countItem) {
                                                    getAllStatusFromServer();
                                                }
                                            }
                                        } else {
                                            getAllStatusFromServer();
                                        }
                                    } else {
                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);

                                        alertDialogDownloading.dismiss();
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    }
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void getAllStatusFromServer() throws IOException {
        Log.i(TAG, "getAllStatusFromServer");
        tvMessage.setText("Mengunduh data \"Kode Status Update\"");
        RequestBody formBody = new FormBody.Builder()
                .add("action", "get_all")
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "status.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogDownloading.dismiss();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogDownloading.dismiss();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (!isError) {
                                        String message = jsonObject.getString(AppConfig.MESSAGE);

                                        if (message.equalsIgnoreCase("not_empty")) {
                                            JSONArray status = jsonObject.getJSONArray("status");
                                            int countItem = status.length();
                                            int j = 0;

                                            for (int i=0; i<countItem; i++) {
                                                JSONObject object = status.getJSONObject(i);
                                                String id = object.getString(AppConfig.ID);
                                                String keterangan = object.getString(AppConfig.KETERANGAN);
                                                String tindakan = object.getString(AppConfig.TINDAKAN);
                                                String kodeLn = object.getString(AppConfig.KODE_LN);
                                                String kodeDn = object.getString(AppConfig.KODE_DN);
                                                String urutan = object.getString(AppConfig.URUTAN);
                                                String level = object.getString(AppConfig.LEVEL);
                                                String terakhir = object.getString(AppConfig.TERAKHIR);
                                                String idStatusInduk = object.getString(AppConfig.ID_STATUS_INDUK);

                                                if (db.isExistOnStatus(id)) {
                                                    db.updateIntoStatus(
                                                        new Status(
                                                            id, keterangan, tindakan, kodeLn, kodeDn, urutan, level, terakhir, idStatusInduk
                                                        )
                                                    );
                                                    j++;
                                                } else {
                                                    db.insertIntoStatus(
                                                        new Status(
                                                            id, keterangan, tindakan, kodeLn, kodeDn, urutan, level, terakhir, idStatusInduk
                                                        )
                                                    );
                                                    j++;
                                                }

                                                if (j == countItem) {
                                                    getAllFileUploadUsageFromServer();
                                                }
                                            }
                                        } else {
                                            getAllFileUploadUsageFromServer();
                                        }
                                    } else {
                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);

                                        alertDialogDownloading.dismiss();
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    }
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void getAllFileUploadUsageFromServer() throws IOException {
        Log.i(TAG, "getAllFileUploadUsageFromServer");
        tvMessage.setText("Mengunduh data \"File Upload Usage\"");
        RequestBody formBuilder = new FormBody.Builder()
                .add("action", "get_all")
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "file_upload_usage.php")
                .post(formBuilder)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogDownloading.dismiss();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogDownloading.dismiss();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (!isError) {
                                        String message = jsonObject.getString(AppConfig.MESSAGE);

                                        if (message.equalsIgnoreCase("not_empty")) {
                                            JSONArray fileUploadUsage = jsonObject.getJSONArray("file_upload_usage");
                                            int countItem = fileUploadUsage.length();
                                            int j = 0;

                                            for (int i=0; i<countItem; i++) {
                                                JSONObject object = fileUploadUsage.getJSONObject(i);
                                                String uid = object.getString(AppConfig.UID);
                                                String tanggal = object.getString(AppConfig.TANGGAL);
                                                int jmlBerkas = object.getInt(AppConfig.JML_BERKAS);
                                                int jmlBerkasFoto = object.getInt(AppConfig.JML_BERKAS_FOTO);
                                                int jmlBerkasTtd = object.getInt(AppConfig.JML_BERKAS_TTD);
                                                double jmlUkuran = object.getDouble(AppConfig.JML_UKURAN);
                                                double jmlUkuranFoto = object.getDouble(AppConfig.JML_UKURAN_FOTO);
                                                double jmlUkuranTtd = object.getDouble(AppConfig.JML_UKURAN_TTD);

                                                if (db.isExistOnFileUploadUsage(tanggal)) {
                                                    db.updateIntoFileUploadUsage(
                                                        new FileUploadUsage(
                                                            uid, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran, jmlUkuranFoto, jmlUkuranTtd, "1"
                                                        )
                                                    );
                                                    j++;
                                                } else {
                                                    db.insertIntoFileUploadUsage(
                                                        new FileUploadUsage(
                                                            uid, tanggal, jmlBerkas, jmlBerkasFoto, jmlBerkasTtd, jmlUkuran,
                                                            jmlUkuranFoto, jmlUkuranTtd, "1", idPengantar
                                                        )
                                                    );
                                                    j++;
                                                }

                                                if (j == countItem) {
                                                    getAllFileUploadErrorFromServer();
                                                }
                                            }
                                        } else {
                                            getAllFileUploadErrorFromServer();
                                        }
                                    } else {
                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);

                                        alertDialogDownloading.dismiss();
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    }
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }

            }
        });
    }

    private void getAllFileUploadErrorFromServer() throws IOException {
        Log.i(TAG, "getAllFileUploadErrorFromServer");
        tvMessage.setText("Mengunduh data \"File Upload Error\"");
        RequestBody formBuilder = new FormBody.Builder()
                .add("action", "get_all")
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "file_upload_error.php")
                .post(formBuilder)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogDownloading.dismiss();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogDownloading.dismiss();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (!isError) {
                                        String message = jsonObject.getString(AppConfig.MESSAGE);

                                        if (message.equalsIgnoreCase("not_empty")) {
                                            JSONArray fileUploadUsage = jsonObject.getJSONArray("file_upload_error");
                                            int countItem = fileUploadUsage.length();
                                            int j = 0;

                                            for (int i=0; i<countItem; i++) {
                                                JSONObject object = fileUploadUsage.getJSONObject(i);
                                                String uid = object.getString(AppConfig.UID);
                                                String namaFile = object.getString(AppConfig.NAMA_FILE);
                                                String direktori = object.getString(AppConfig.DIREKTORI);

                                                if (!db.isExistOnFileUploadError(namaFile)) {
                                                    db.insertIntoFileUploadError(
                                                        new FileUploadError(uid, namaFile, direktori, idPengantar)
                                                    );
                                                }
                                                j++;

                                                if (j == countItem) {
                                                    loginSession.setSession(idPengantar, nama, idLogin, idKantor,
                                                            idMandor, namaMandor, idGrup, SERVER, BASE_URL);

                                                    if (!LoginActivity.this.isDestroyed()) {
                                                        alertDialogDownloading.dismiss();

                                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                                        finish();
                                                    }
                                                }
                                            }
                                        } else {
                                            loginSession.setSession(idPengantar, nama, idLogin, idKantor, idMandor,
                                                    namaMandor, idGrup, SERVER, BASE_URL);

                                            if (!LoginActivity.this.isDestroyed()) {
                                                alertDialogDownloading.dismiss();

                                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                                finish();
                                            }
                                        }
                                    } else {
                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);

                                        alertDialogDownloading.dismiss();
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }

            }
        });
    }
    
    private boolean validateIdLogin() {
        Log.i(TAG, "validateIdLogin");
        strIdLogin = etIdLogin.getText().toString().trim();

        if (strIdLogin.isEmpty()) {
            Toasty.error(this, getResources().getString(R.string.id_login_empty), 3000, false)
                    .show();
            return false;
        } else if (strIdLogin.length() < 9) {
            Toasty.error(this, getResources().getString(R.string.id_login_min_length), 3000, false)
                    .show();
            return false;
        }

        return true;
    }

    private boolean validatePassword() {
        Log.i(TAG, "validatePassword");
        strPassword = etPassword.getText().toString().trim();

        if (strPassword.isEmpty()) {
            Toasty.error(this, getResources().getString(R.string.password_empty), 3000, false)
                    .show();
            return false;
        }

        return true;
    }

    private void validatePermissions() {
        Log.i(TAG, "validatePermissions");
        if (ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[0]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[1]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[2]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[3]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[4]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[5]) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[0]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[1]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[2]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[3]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[4]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[5])) {
                alertDialogInformation
                       .setMessage(getResources().getString(R.string.message_permissions))
                       .setActionPositive("permissionRequired")
                       .showDialog();
            } else {
                ActivityCompat.requestPermissions(this, PERMISSIONS_REQUIRED, PERMISSION_CALLBACK_CONSTANT);
            }
        } else {
            if (!isGPSEnabled()) {
                alertDialogInformation
                        .setMessage(getResources().getString(R.string.message_gps))
                        .setActionPositive("locationSetting")
                        .showDialog();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            boolean allGranted = false;
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    allGranted = true;
                } else {
                    allGranted = false;
                    break;
                }
            }

            if (allGranted) {
                if (!isGPSEnabled()) {
                    alertDialogInformation
                            .setMessage(getResources().getString(R.string.message_gps))
                            .setActionPositive("locationSetting")
                            .showDialog();
                }
            } else if (
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[0]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[1]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[2]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[3]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[4]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[5])) {
                alertDialogInformation
                        .setMessage(getResources().getString(R.string.message_permissions))
                        .setActionPositive("permissionRequired")
                        .showDialog();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean isGPSEnabled() {
        Log.i(TAG, "isGPSEnabled");
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    public void positiveButtonClickedOnAlertDialogInformation(String action) {
        if (action.equalsIgnoreCase("permissionRequired")) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_REQUIRED, PERMISSION_CALLBACK_CONSTANT);
        }

        if (action.equalsIgnoreCase("locationSetting")) {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.i(TAG, "onNetworkConnectionChanged");
        if (!isConnected) {
            //Toasty.error(this, getResources().getString(R.string.network_no_internet), 1000, false).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.getInstance().setNetworkListener(this);
        if (!isGPSEnabled()) {
            alertDialogInformation
                    .setMessage(getResources().getString(R.string.message_gps))
                    .setActionPositive("locationSetting")
                    .showDialog();
        }
    }
}
