package horirevens.com.epod.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.horirevens.mylibrary.fonts.TextViewLatoBold;
import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import horirevens.com.epod.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import horirevens.com.epod.fragments.MainFragment;
import horirevens.com.epod.helpers.DatabaseHelper;
import horirevens.com.epod.helpers.ExceptionHandler;
import horirevens.com.epod.libraries.AlertDialogConfirmation;
import horirevens.com.epod.libraries.AlertDialogInformation;
import horirevens.com.epod.libraries.AlertDialogLoading;
import horirevens.com.epod.libraries.MyFile;
import horirevens.com.epod.libraries.MyUploadFile;
import horirevens.com.epod.models.Antaran;
import horirevens.com.epod.models.FileUploadError;
import horirevens.com.epod.models.Status;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        AlertDialogConfirmation.AlertDialogConfirmationListener,
        AlertDialogInformation.AlertDialogInformationListener,
        LocationListener {
    private static final String TAG = MainActivity.class.getName();
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; //meter
    private static final long MIN_TIME_BETWEEN_UPDATES = 5000;
    private static final String[] PERMISSIONS_REQUIRED = new String[] {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int PERMISSION_CALLBACK_CONSTANT = 101;

    private AlertDialogConfirmation alertDialogConfirmation;
    private AlertDialogInformation alertDialogInformation;
    private AlertDialogLoading alertDialogLoading;
    private AlertDialog alertDialogDownloading;
    private LoginSession loginSession;
    private DateSession dateSession;
    private VersionNotificationSession versionNotificationSession;
    private OkHttpClient client;
    private DatabaseHelper db;
    private Location location;
    private LocationManager locationManager;
    private SharedPreferences locationPreferences;
    private SharedPreferences.Editor locationEditor;
    private MyUploadFile myUploadFile;
    private MyFile myFile;
    private Date date;

    private String idPengantar, nama, idKantor, baseUrl, currentDate;
    private double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_main);
        Log.i(TAG, "onCreate");

        if (getIntent().getBooleanExtra("exit", false)) {
            finish();
        }

        alertDialogConfirmation = new AlertDialogConfirmation(this, this);
        alertDialogInformation = new AlertDialogInformation(this, this);
        alertDialogLoading = new AlertDialogLoading(this);
        loginSession = new LoginSession(this);
        dateSession = new DateSession(this);
        versionNotificationSession = new VersionNotificationSession(this);
        db = new DatabaseHelper(this);
        myUploadFile = new MyUploadFile(this);
        myFile = new MyFile(this);
        date = new Date(System.currentTimeMillis());
        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        client = MainApplication.getUnsafeOkHttpClient();
        locationPreferences = getSharedPreferences("LocationPreferences", MODE_PRIVATE);

        HashMap<String, String> hashMap = loginSession.getSession();
        idPengantar = hashMap.get(AppConfig.ID);
        nama = hashMap.get(AppConfig.NAMA);
        idKantor = hashMap.get(AppConfig.ID_KANTOR);
        baseUrl = hashMap.get(AppConfig.BASE_URL);
        String server = hashMap.get(AppConfig.SERVER);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextViewLatoBold tvTitle = (TextViewLatoBold) findViewById(R.id.tv_title);
        tvTitle.setText(nama);
        TextViewLatoRegular tvDesc = (TextViewLatoRegular) findViewById(R.id.tv_desc);
        tvDesc.setText("Nippos. " + idPengantar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View navHeaderView = navigationView.getHeaderView(0);
        TextViewLatoBold tvName = (TextViewLatoBold) navHeaderView.findViewById(R.id.tv_name);
        tvName.setText(nama);
        TextViewLatoRegular tvId = (TextViewLatoRegular) navHeaderView.findViewById(R.id.tv_id);
        tvId.setText("Nippos. " + idPengantar);
        TextViewLatoRegular tvVersion = (TextViewLatoRegular) navHeaderView.findViewById(R.id.tv_version);
        tvVersion.setText("versi " + BuildConfig.VERSION_NAME);

        displaySelectedScreen(R.id.nav_home);
        validatePermissions();

        // 17027500753
        // UN764995255CN
        // 22138804205
        //db.deleteAntaranByUid("003");
        /*db.insertIntoAntaran(
            new Antaran(
                "001", "17027500753", "644000019AZ000001", "P01", "", "2019-02-09 09:00:00",
                "", 0.000000, 0.000000, "", "", "NON COD",
                "210", "00000", "64400", "YOGI TRISMAYANA", "PERUM BUMI ASRI E-6 KALIOMBO KEDIRI, JAWA TIMUR, " +
                "INDONESIA", "+6282140513878", "NOVI CINDY RISTANTI", "JL LOCALHOST 127 PAYAMAN NGANJUK, JAWA " +
                "TIMUR, INDONESIA", "6282142528779", 500, 0, 0, 0, "", "1", "0", idPengantar
            )
        );
        db.insertIntoAntaran(
            new Antaran(
                "002", "UN764995255CN", "644000019AZ000001", "P01", "", "2019-02-09 09:00:00",
                "", 0.000000, 0.000000, "", "", "BLB",
                "230", "00000", "64400", "YOGI TRISMAYANA", "PERUM BUMI ASRI E-6 KALIOMBO KEDIRI, JAWA TIMUR, " +
                "INDONESIA", "+6282140513878", "NOVI CINDY RISTANTI", "JL LOCALHOST 127 PAYAMAN NGANJUK, JAWA " +
                "TIMUR, INDONESIA", "082142528779", 5000, 0, 20000, 0, "", "1", "0", idPengantar
            )
        );
        db.insertIntoAntaran(
            new Antaran(
                "003", "22138804205", "644000019AZ000001", "P01", "", "2019-02-09 09:00:00",
                "", 0.000000, 0.000000, "", "", "COD",
                "417", "00000", "64400", "YOGI TRISMAYANA", "PERUM BUMI ASRI E-6 KALIOMBO KEDIRI, JAWA TIMUR, " +
                "INDONESIA", "+6282140513878", "NOVI CINDY RISTANTI", "JL LOCALHOST 127 PAYAMAN NGANJUK, JAWA " +
                "TIMUR, INDONESIA", "0", 1500, 650000, 0, 0, "BSJ0000000001", "1", "0", idPengantar
            )
        );*/

        /*db.updateIntoAntaran(
            new Antaran(
                "001", "G0101", "YOGI TRISMAYANA", "2019-02-09 10:00:00",
                0.000000, 0.000000, "", "", "1"
            )
        );
        db.updateIntoAntaran(
            new Antaran(
                "002", "G0102", "YOGI TRISMAYANA", "2019-02-09 10:00:00",
                0.000000, 0.000000, "", "", "1"
            )
        );
        db.updateIntoAntaran(
            new Antaran(
                "003", "G0103", "YOGI TRISMAYANA", "2019-02-09 10:00:00",
                0.000000, 0.000000, "", "", "1"
            )
        );*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displaySelectedScreen(item.getItemId());
        return true;
    }

    private void displaySelectedScreen(int itemId) {
        Fragment fragment = null;
        Intent intent;

        switch (itemId) {
            case R.id.nav_home:
                fragment = new MainFragment();
                break;
            case R.id.nav_delivery_order:
                try {
                    generateDeliveryOrder();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.nav_file_upload_usage:
                intent = new Intent(MainActivity.this, FileUploadUsageActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_download_antaran:
                if (db.getCountAntaranPendingNonProses(currentDate) > 0) {
                    String messageAntaran =
                            "Masih ada data dengan status pending pada antaran Anda. " +
                            "Lakukan unggah data dengan cara klik tombol unggah yang ada " +
                            "pada pojok kanan atas Tab Berhasil/Gagal untuk menaikkan data";
                    alertDialogInformation
                            .setMessage(messageAntaran)
                            .showDialog();
                } else {
                    String messageAntaran =
                            "Data antaran Anda saat ini akan dihapus dan akan mengunduh data baru dari server. " +
                            "Data yang sudah di-update status tidak akan hilang. Apakah Anda yakin akan melanjutkan?";

                    alertDialogConfirmation
                            .setMessage(messageAntaran)
                            .setActionPositive("downloadAntaran")
                            .showDialog();
                }
                break;
            case R.id.nav_download_status:
                String messageStatusUpdate =
                        "Data kode status update akan dihapus dan akan mengunduh data baru dari server. Apakah Anda yakin akan melanjutkan?";

                alertDialogConfirmation
                        .setMessage(messageStatusUpdate)
                        .setActionPositive("downloadStatus")
                        .showDialog();
                break;
            case R.id.nav_logout:
                alertDialogConfirmation
                        .setMessage("Anda yakin akan keluar?")
                        .setActionPositive("logout")
                        .showDialog();
                break;
        }

        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_main, fragment);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void generateDeliveryOrder() throws IOException {
        Log.i(TAG, "generateDeliveryOrder");
        alertDialogLoading.setCancelAble(false).showDialog();

        Date date = new Date(System.currentTimeMillis());
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);

        RequestBody formBody = new FormBody.Builder()
                .add("action", "generate_id")
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .add(AppConfig.ID_KANTOR, idKantor)
                .add(AppConfig.TANGGAL_PERANGKAT, currentDate)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "delivery_order.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogLoading.dismissDialog();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogLoading.dismissDialog();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (isError) {
                                        alertDialogLoading.dismissDialog();

                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    } else {
                                        JSONObject deliveryOrder = jsonObject.getJSONObject("delivery_order");
                                        String idDeliveryOrder = deliveryOrder.getString(AppConfig.ID);

                                        if (!MainActivity.this.isDestroyed()) {
                                            alertDialogLoading.dismissDialog();

                                            Intent intent = new Intent(MainActivity.this, DeliveryOrderActivity.class);
                                            intent.putExtra("idDeliveryOrder", idDeliveryOrder);
                                            startActivity(intent);

                                            finish();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public void positiveButtonClickedOnAlertDialogConfirmation(String action) {
        if (action.equalsIgnoreCase("logout")) {
            Log.i(TAG, "logout");
            loginSession.clearSession();
            dateSession.clearSession();

            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }

        if (action.equalsIgnoreCase("downloadAntaran")) {
            int countRow = db.getCountAntaran(currentDate);
            if (countRow > 0) {
                if (db.truncate("antaran")) {
                    try {
                        showAlertDialogDownloading();
                        getAllAntaranFromServer();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    showAlertDialogDownloading();
                    getAllAntaranFromServer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (action.equalsIgnoreCase("downloadStatus")) {
            int countRow = db.getCountStatus();
            if (countRow > 0) {
                if (db.truncate("status")) {
                    try {
                        showAlertDialogDownloading();
                        getAllStatusFromServer();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    showAlertDialogDownloading();
                    getAllStatusFromServer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void negativeButtonClickedOnAlertDialogConfirmation(String action) {

    }

    private void showAlertDialogDownloading() {
        View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_downloading, null);
        TextViewLatoRegular tvMessage = (TextViewLatoRegular) view.findViewById(R.id.tv_message);
        tvMessage.setText("Mengunduh data...");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        alertDialogDownloading = builder.create();
        alertDialogDownloading.setCancelable(true);
        alertDialogDownloading.setCanceledOnTouchOutside(true);
        alertDialogDownloading.show();
    }

    private void validateVersion() throws IOException {
        Log.i(TAG, "validateVersion");
        Date date = new Date(System.currentTimeMillis());
        final String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
        final String deviceVersion = BuildConfig.VERSION_NAME;

        RequestBody formBody = new FormBody.Builder()
                .add("versi", deviceVersion)
                .add("tipe", "PRO")
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "versi.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        throw new IOException(response.code() + " : " + response.message());
                    } else {
                        final String responseData = responseBody.string();

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (isError) {
                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);
                                        String latestVersion = jsonObject.getString("latest_version");
                                        String typeVersion = jsonObject.getString("type_version");

                                        if (typeVersion.equalsIgnoreCase("MAJOR")) {
                                            alertDialogInformation
                                                .setMessage(errorMessage)
                                                .setActionPositive("finish")
                                                .showDialog();
                                        } else {
                                            HashMap<String, String> hashMap = versionNotificationSession.getSession();
                                            String hmDeviceVersion = hashMap.get("version");
                                            String hmTypeVersion = hashMap.get("typeVersion");
                                            String hmDate = hashMap.get("date");

                                            if (hmDate == null) {
                                                versionNotificationSession.setSession(latestVersion, typeVersion, currentDate);

                                                alertDialogInformation
                                                    .setMessage(errorMessage)
                                                    .showDialog();
                                            } else {
                                                if (!hmDate.equals(currentDate)) {
                                                    versionNotificationSession.setSession(latestVersion, typeVersion, currentDate);

                                                    alertDialogInformation
                                                        .setMessage(errorMessage)
                                                        .showDialog();
                                                }
                                            }
                                        }
                                    } else {
                                        getAllFileUploadErrorFromServer();
                                    }
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void getAllFileUploadErrorFromServer() throws IOException {
        Log.i(TAG, "getAllFileUploadErrorFromServer");
        RequestBody formBuilder = new FormBody.Builder()
                .add("action", "get_all")
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "file_upload_error.php")
                .post(formBuilder)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        throw new IOException(response.code() + " : " + response.message());
                    } else {
                        final String responseData = responseBody.string();

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (!isError) {
                                        String message = jsonObject.getString(AppConfig.MESSAGE);

                                        if (message.equalsIgnoreCase("not_empty")) {
                                            JSONArray fileUploadError = jsonObject.getJSONArray("file_upload_error");
                                            int countItem = fileUploadError.length();
                                            int j = 0;

                                            for (int i=0; i<countItem; i++) {
                                                JSONObject object = fileUploadError.getJSONObject(i);
                                                String uid = object.getString(AppConfig.UID);
                                                String namaFile = object.getString(AppConfig.NAMA_FILE);
                                                String direktori = object.getString(AppConfig.DIREKTORI);

                                                if (!db.isExistOnFileUploadError(namaFile)) {
                                                    db.insertIntoFileUploadError(
                                                        new FileUploadError(uid, namaFile, direktori, idPengantar)
                                                    );
                                                }
                                                j++;

                                                if (j == countItem) {
                                                    if (db.getCountFileUploadError() > 0) {
                                                        uploadHTTP();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }

            }
        });
    }

    @Override
    public void positiveButtonClickedOnAlertDialogInformation(String action) {
        if (action.equalsIgnoreCase("finish")) {
            finish();
        }

        if (action.equalsIgnoreCase("refresh")) {
            displaySelectedScreen(R.id.nav_home);
        }

        if (action.equalsIgnoreCase("permissionRequired")) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_REQUIRED, PERMISSION_CALLBACK_CONSTANT);
        }

        if (action.equalsIgnoreCase("locationSetting")) {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
            finish();
        }
    }

    private void startLocation() {
        Log.i(TAG, "startLocation");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                MIN_TIME_BETWEEN_UPDATES,
                MIN_DISTANCE_CHANGE_FOR_UPDATES,
                this);

        if (locationManager != null) {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                locationEditor = locationPreferences.edit();
                locationEditor.putString("latitude", String.valueOf(latitude));
                locationEditor.putString("longitude", String.valueOf(longitude));
                locationEditor.apply();
            }
        }
    }

    private void stopLocation() {
        Log.i(TAG, "stopLocation");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationManager.removeUpdates(this);
        locationManager = null;
        location = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isGPSEnabled()) {
            alertDialogInformation
                    .setMessage(getResources().getString(R.string.message_gps))
                    .setActionPositive("locationSetting")
                    .showDialog();
        } else {
            if (locationManager == null) {
                locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                startLocation();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (locationManager != null) {
            stopLocation();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        locationEditor = locationPreferences.edit();
        locationEditor.clear();
        locationEditor.apply();

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        locationEditor = locationPreferences.edit();
        locationEditor.putString("latitude", String.valueOf(latitude));
        locationEditor.putString("longitude", String.valueOf(longitude));
        locationEditor.apply();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void uploadHTTP() {
        Log.i(TAG, "uploadHTTP");
        ArrayList<FileUploadError> arrayList;
        arrayList = db.getAllFileUploadError();

        for (FileUploadError fileUploadError : arrayList) {
            String uid = fileUploadError.getUid();
            String namaFile = fileUploadError.getNamaFile();
            String direktori = fileUploadError.getDirektori();
            String codeFileName = namaFile.substring(0, 1);

            if (codeFileName.equalsIgnoreCase("S")) {
                File signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(this, AppConfig.FOLDER_SIGNATURE), namaFile);

                if (signatureFile.exists()) {
                    String uploadId = myUploadFile.UploadHTTP(
                            this,
                            String.valueOf(signatureFile),
                            direktori
                    );

                    db.updateUidIntoFileUploadError(uploadId, uid);
                } else {
                    if (db.deleteFileUploadErrorByNamaFile(namaFile)) {
                        try {
                            deleteFileUploadErrorFromServer(idPengantar, namaFile, baseUrl);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                File imageFile = new File(myFile.getFolderPicturesOnStorageDirectory(this, AppConfig.FOLDER_IMAGE), namaFile);

                if (imageFile.exists()) {
                    String uploadId = myUploadFile.UploadHTTP(
                            this,
                            String.valueOf(imageFile),
                            direktori
                    );

                    db.updateUidIntoFileUploadError(uploadId, uid);
                } else {
                    if (db.deleteFileUploadErrorByNamaFile(namaFile)) {
                        try {
                            deleteFileUploadErrorFromServer(idPengantar, namaFile, baseUrl);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private void deleteFileUploadErrorFromServer(String idPengantar, String namaFile, String baseUrl)
            throws IOException {
        Log.i(TAG, "deleteFileUploadErrorFromServer");
        RequestBody formBody = new FormBody.Builder()
                .add("action", "delete")
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .add(AppConfig.NAMA_FILE, namaFile)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "file_upload_error.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        throw new IOException(response.code() + " : " + response.message());
                    } else {
                        String responseData = responseBody.string();
                        try {
                            JSONObject jsonObject = new JSONObject(responseData);
                            boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                            if (!isError) {
                                // Do nothing
                            } else {
                                // Do nothing
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    /*private void uploadFTP() {
        Log.i(TAG, "uploadFTP");
        ArrayList<FileUploadError> arrayList;
        arrayList = db.getAllFileUploadError();

        for (FileUploadError ftpUploadError : arrayList) {
            String uid = ftpUploadError.getUid();
            String fileName = ftpUploadError.getNamaFile();
            String remotePath = ftpUploadError.getDirektori();
            String codeFileName = fileName.substring(0, 1);

            if (codeFileName.equalsIgnoreCase("S")) {
                File signatureFile = new File(myFile.getFolderPicturesOnStorageDirectory(this, AppConfig.FOLDER_SIGNATURE), fileName);

                if (signatureFile.exists()) {
                    String uploadId = myUploadFile.uploadFTP(
                            this,
                            AppConfig.FTP_HOST,
                            Integer.parseInt(AppConfig.FTP_PORT),
                            AppConfig.FTP_USERNAME,
                            AppConfig.FTP_PASSWORD,
                            String.valueOf(signatureFile),
                            remotePath
                    );

                    db.updateUidIntoFileUploadError(uploadId, uid);
                } else {
                    db.deleteFileUploadErrorByNamaFile(fileName);
                }
            } else {
                File imageFile = new File(myFile.getFolderPicturesOnStorageDirectory(this, AppConfig.FOLDER_IMAGE), fileName);

                if (imageFile.exists()) {
                    String uploadId = myUploadFile.uploadFTP(
                            this,
                            AppConfig.FTP_HOST,
                            Integer.parseInt(AppConfig.FTP_PORT),
                            AppConfig.FTP_USERNAME,
                            AppConfig.FTP_PASSWORD,
                            String.valueOf(imageFile),
                            remotePath
                    );

                    db.updateUidIntoFileUploadError(uploadId, uid);
                } else {
                    db.deleteFileUploadErrorByNamaFile(fileName);
                }
            }
        }
    }*/

    private void getAllAntaranFromServer() throws IOException {
        Log.i(TAG, "getAllAntaranFromServer");
        RequestBody formBody = new FormBody.Builder()
                .add("action", "get_all")
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "antaran.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogDownloading.dismiss();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogDownloading.dismiss();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (!isError) {
                                        String message = jsonObject.getString(AppConfig.MESSAGE);

                                        if (message.equalsIgnoreCase("not_empty")) {
                                            JSONArray antaran = jsonObject.getJSONArray("antaran");
                                            int countItem = antaran.length();
                                            int j = 0;
                                            int p = 0;
                                            int b = 0;
                                            int g = 0;

                                            for (int i=0; i<countItem; i++) {
                                                double mGarisLintang, mGarisBujur, mBerat;
                                                double mBsuCod,  mBsuBlb, mBsuTax;

                                                JSONObject object = antaran.getJSONObject(i);
                                                String uid = object.getString(AppConfig.UID);
                                                String idItem = object.getString(AppConfig.ID_ITEM);
                                                String idDeliveryOrder = object.getString(AppConfig.ID_DELIVERY_ORDER);
                                                String idStatus = object.getString(AppConfig.ID_STATUS);
                                                String keterangan = object.getString(AppConfig.KETERANGAN);
                                                if (keterangan == null || keterangan.equalsIgnoreCase("null")) {
                                                    keterangan = "";
                                                }
                                                String waktuEntri = object.getString(AppConfig.WAKTU_ENTRI);
                                                String waktuUpdate = object.getString(AppConfig.WAKTU_UPDATE);
                                                if (waktuUpdate == null || waktuUpdate.equalsIgnoreCase("null")) {
                                                    waktuUpdate = "";
                                                }
                                                String garisLintang = object.getString(AppConfig.GARIS_LINTANG);
                                                if (garisLintang == null || garisLintang.equalsIgnoreCase("null")) {
                                                    mGarisLintang = 0.000000;
                                                } else {
                                                    mGarisLintang = Double.parseDouble(garisLintang);
                                                }
                                                String garisBujur = object.getString(AppConfig.GARIS_BUJUR);
                                                if (garisBujur == null || garisBujur.equalsIgnoreCase("null")) {
                                                    mGarisBujur = 0.000000;
                                                } else {
                                                    mGarisBujur = Double.parseDouble(garisBujur);
                                                }
                                                String tandaTangan = object.getString(AppConfig.TANDA_TANGAN);
                                                if (tandaTangan == null || tandaTangan.equalsIgnoreCase("null")) {
                                                    tandaTangan = "";
                                                }
                                                String foto = object.getString(AppConfig.FOTO);
                                                if (foto == null || foto.equalsIgnoreCase("null")) {
                                                    foto = "";
                                                }
                                                String namaPengirim = object.getString(AppConfig.NAMA_PENGIRIM);
                                                if (namaPengirim == null || namaPengirim.equalsIgnoreCase("null")) {
                                                    namaPengirim = "";
                                                }
                                                String alamatPengirim = object.getString(AppConfig.ALAMAT_PENGIRIM);
                                                if (alamatPengirim == null || alamatPengirim.equalsIgnoreCase("null")) {
                                                    alamatPengirim = "";
                                                }
                                                String hpPengirim = object.getString(AppConfig.HP_PENGIRIM);
                                                if (hpPengirim == null || hpPengirim.equalsIgnoreCase("null")) {
                                                    hpPengirim = "";
                                                }
                                                String namaPenerima = object.getString(AppConfig.NAMA_PENERIMA);
                                                if (namaPenerima == null || namaPenerima.equalsIgnoreCase("null")) {
                                                    namaPenerima = "";
                                                }
                                                String alamatPenerima = object.getString(AppConfig.ALAMAT_PENERIMA);
                                                if (alamatPenerima == null || alamatPenerima.equalsIgnoreCase("null")) {
                                                    alamatPenerima = "";
                                                }
                                                String hpPenerima = object.getString(AppConfig.HP_PENERIMA);
                                                if (hpPenerima == null || hpPenerima.equalsIgnoreCase("null")) {
                                                    hpPenerima = "";
                                                }
                                                String idProduk = object.getString(AppConfig.ID_PRODUK);
                                                if (idProduk == null || idProduk.equalsIgnoreCase("null")) {
                                                    idProduk = "";
                                                }
                                                String berat = object.getString(AppConfig.BERAT);
                                                if (berat == null || berat.equalsIgnoreCase("null")) {
                                                    mBerat = 0.0;
                                                } else {
                                                    mBerat = Double.parseDouble(berat);
                                                }
                                                String bsuCod = object.getString(AppConfig.BSU_COD);
                                                if (bsuCod == null || bsuCod.equalsIgnoreCase("null")) {
                                                    mBsuCod = 0;
                                                } else {
                                                    mBsuCod = Double.parseDouble(bsuCod);
                                                }
                                                String bsuBlb = object.getString(AppConfig.BSU_BLB);
                                                if (bsuBlb == null || bsuBlb.equalsIgnoreCase("null")) {
                                                    mBsuBlb = 0;
                                                } else {
                                                    mBsuBlb = Double.parseDouble(bsuBlb);
                                                }
                                                String bsuTax = object.getString(AppConfig.BSU_TAX);
                                                if (bsuTax == null || bsuTax.equalsIgnoreCase("null")) {
                                                    mBsuTax = 0;
                                                } else {
                                                    mBsuTax = Double.parseDouble(bsuTax);
                                                }
                                                String kantorAsal = object.getString(AppConfig.KANTOR_ASAL);
                                                if (kantorAsal == null || kantorAsal.equalsIgnoreCase("null")) {
                                                    kantorAsal = "";
                                                }
                                                String kantorTujuan = object.getString(AppConfig.KANTOR_TUJUAN);
                                                if (kantorTujuan == null || kantorTujuan.equalsIgnoreCase("null")) {
                                                    kantorTujuan = "";
                                                }
                                                String idCustomer = object.getString(AppConfig.ID_EXTERNAL);
                                                if (idCustomer == null || idCustomer.equalsIgnoreCase("null")) {
                                                    idCustomer = "";
                                                }
                                                String idExternal = object.getString(AppConfig.ID_EXTERNAL);
                                                if (idExternal == null || idExternal.equalsIgnoreCase("null")) {
                                                    idExternal = "";
                                                }
                                                String tglPosting = object.getString(AppConfig.ID_EXTERNAL);
                                                if (tglPosting == null || tglPosting.equalsIgnoreCase("null")) {
                                                    tglPosting = "";
                                                }
                                                String tipe = object.getString(AppConfig.TIPE);
                                                if (tipe == null || tipe.equalsIgnoreCase("null")) {
                                                    tipe = "";
                                                }
                                                String alamatBaru = object.getString(AppConfig.ALAMAT_BARU);
                                                if (alamatBaru == null || alamatBaru.equalsIgnoreCase("null")) {
                                                    alamatBaru = "";
                                                }
                                                String catatan = object.getString(AppConfig.CATATAN);
                                                if (catatan == null || catatan.equalsIgnoreCase("null")) {
                                                    catatan = "";
                                                }
                                                String statusTutup = object.getString(AppConfig.TUTUP);
                                                String statusUpdate = idStatus.equalsIgnoreCase("P01") ? "0" : "1";

                                                String initialStatus = idStatus.substring(0, 1);
                                                if (initialStatus.equalsIgnoreCase("P")) {
                                                    p++;
                                                } else if (initialStatus.equalsIgnoreCase("B")) {
                                                    b++;
                                                } else if (initialStatus.equalsIgnoreCase("G")) {
                                                    g++;
                                                }

                                                db.insertIntoAntaran(
                                                    new Antaran(
                                                        uid, idItem, idDeliveryOrder, idStatus, keterangan,
                                                        waktuEntri, waktuUpdate, mGarisLintang, mGarisBujur,
                                                        tandaTangan, foto, namaPengirim, alamatPengirim, hpPengirim,
                                                        namaPenerima, alamatPenerima, hpPenerima, idProduk, mBerat,
                                                        mBsuCod, mBsuBlb, mBsuTax, kantorAsal, kantorTujuan,
                                                        idCustomer, idExternal, tglPosting, tipe, alamatBaru, catatan,
                                                        statusTutup, statusUpdate, idPengantar
                                                    )
                                                );
                                                j++;

                                                if (j == countItem) {
                                                    String messageSuccess =
                                                            "Unduh data antaran Anda sukses dengan jumlah total " + countItem + " item. " +
                                                            "Delivery Order " + p + " item, Berhasil Antar " + b + " item dan Gagal Antar " + g + " item";

                                                    alertDialogDownloading.dismiss();
                                                    alertDialogInformation
                                                            .setMessage(messageSuccess)
                                                            .setActionPositive("refresh")
                                                            .showDialog();
                                                }
                                            }
                                        } else {
                                            String messageSuccess = "Data antaran Anda hari ini tidak ada pada server karena belum melakukan entri delivery order";

                                            alertDialogDownloading.dismiss();
                                            alertDialogInformation
                                                    .setMessage(messageSuccess)
                                                    .setActionPositive("refresh")
                                                    .showDialog();
                                        }
                                    } else {
                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);

                                        alertDialogDownloading.dismiss();
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void getAllStatusFromServer() throws IOException {
        Log.i(TAG, "getStatusFromServer");
        RequestBody formBody = new FormBody.Builder()
                .add("action", "get_all")
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "status.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogDownloading.dismiss();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogDownloading.dismiss();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (!isError) {
                                        String message = jsonObject.getString(AppConfig.MESSAGE);

                                        if (message.equalsIgnoreCase("not_empty")) {
                                            JSONArray status = jsonObject.getJSONArray("status");
                                            int countItem = status.length();
                                            int j = 0;

                                            for (int i=0; i<countItem; i++) {
                                                JSONObject object = status.getJSONObject(i);
                                                String id = object.getString(AppConfig.ID);
                                                String keterangan = object.getString(AppConfig.KETERANGAN);
                                                String tindakan = object.getString(AppConfig.TINDAKAN);
                                                String kodeLn = object.getString(AppConfig.KODE_LN);
                                                String kodeDn = object.getString(AppConfig.KODE_DN);
                                                String urutan = object.getString(AppConfig.URUTAN);
                                                String level = object.getString(AppConfig.LEVEL);
                                                String terakhir = object.getString(AppConfig.TERAKHIR);
                                                String idStatusInduk = object.getString(AppConfig.ID_STATUS_INDUK);

                                                db.insertIntoStatus(
                                                    new Status(id, keterangan, tindakan, kodeLn, kodeDn, urutan, level, terakhir, idStatusInduk)
                                                );
                                                j++;

                                                if (j == countItem) {
                                                    String messageSuccess = "Unduh data kode status update Anda sukses dengan jumlah " + countItem + " item";

                                                    alertDialogDownloading.dismiss();
                                                    alertDialogInformation
                                                            .setMessage(messageSuccess)
                                                            .setActionPositive("refresh")
                                                            .showDialog();
                                                }
                                            }
                                        } else {
                                            String messageSuccess = "Data kode status update pada server tidak ada. Mohon untuk menginformasikan ke tim ePOD";

                                            alertDialogDownloading.dismiss();
                                            alertDialogInformation
                                                    .setMessage(messageSuccess)
                                                    .setActionPositive("refresh")
                                                    .showDialog();
                                        }
                                    } else {
                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);

                                        alertDialogDownloading.dismiss();
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private boolean isGPSEnabled() {
        Log.i(TAG, "isGPSEnabled");
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void validatePermissions() {
        Log.i(TAG, "validatePermissions");
        if (ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[0]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[1]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[2]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[3]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[4]) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(this, PERMISSIONS_REQUIRED[5]) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[0]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[1]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[2]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[3]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[4]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[5])) {
                alertDialogInformation
                        .setMessage(getResources().getString(R.string.message_permissions))
                        .setActionPositive("permissionRequired")
                        .showDialog();
            } else {
                ActivityCompat.requestPermissions(this, PERMISSIONS_REQUIRED, PERMISSION_CALLBACK_CONSTANT);
            }
        } else {
            if (!isGPSEnabled()) {
                alertDialogInformation
                        .setMessage(getResources().getString(R.string.message_gps))
                        .setActionPositive("locationSetting")
                        .showDialog();
            } else {
                try {
                    validateVersion();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            boolean allGranted = false;
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    allGranted = true;
                } else {
                    allGranted = false;
                    break;
                }
            }

            if (allGranted) {
                if (!isGPSEnabled()) {
                    alertDialogInformation
                            .setMessage(getResources().getString(R.string.message_gps))
                            .setActionPositive("locationSetting")
                            .showDialog();
                } else {
                    try {
                        validateVersion();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[0]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[1]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[2]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[3]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[4]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS_REQUIRED[5])) {
                alertDialogInformation
                        .setMessage(getResources().getString(R.string.message_permissions))
                        .setActionPositive("permissionRequired")
                        .showDialog();
            }
        }
    }
}
