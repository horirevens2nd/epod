package horirevens.com.epod.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.horirevens.mylibrary.fonts.ButtonLatoBold;
import com.horirevens.mylibrary.fonts.TextInputEditTextLatoRegular;
import com.horirevens.mylibrary.fonts.TextViewLatoBold;
import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import es.dmoral.toasty.Toasty;
import horirevens.com.epod.AppConfig;
import horirevens.com.epod.DateSession;
import horirevens.com.epod.LoginSession;
import horirevens.com.epod.MainApplication;
import horirevens.com.epod.R;
import horirevens.com.epod.adapters.ItemDeliveryOrderAdapter;
import horirevens.com.epod.helpers.DatabaseHelper;
import horirevens.com.epod.helpers.ExceptionHandler;
import horirevens.com.epod.libraries.AlertDialogConfirmation;
import horirevens.com.epod.libraries.AlertDialogInformation;
import horirevens.com.epod.libraries.AlertDialogLoading;
import horirevens.com.epod.libraries.PreventZeroOnFirstType;
import horirevens.com.epod.models.Antaran;
import horirevens.com.epod.models.ItemDeliveryOrder;

public class DeliveryOrderActivity extends AppCompatActivity implements
        View.OnClickListener,
        AlertDialogInformation.AlertDialogInformationListener,
        ItemDeliveryOrderAdapter.ItemDeliveryOrderListener,
        AlertDialogConfirmation.AlertDialogConfirmationListener {
    private static final String TAG = DeliveryOrderActivity.class.getName();
    private static final String X_POS_USER = "epod";
    private static final String X_POS_PASSWORD = "3?0d!w4xw4W";
    private static final String GET_TOKEN = "https://api.posindonesia.co.id:8245/token";
    private static final String GET_ITEM = "https://api.posindonesia.co.id:8245/epod/1.0.0/getItem";

    private ItemDeliveryOrderAdapter adapter;
    private ArrayList<ItemDeliveryOrder> arrayList = new ArrayList<>();
    private DatabaseHelper db;
    private AlertDialogLoading alertDialogLoading;
    private AlertDialogInformation alertDialogInformation;
    private AlertDialogConfirmation alertDialogConfirmation;
    private OkHttpClient client;
    private DateSession dateSession;

    private ConstraintLayout constraintLayout;
    private RecyclerView recyclerView;
    private TextInputEditTextLatoRegular etIdItem;
    private TextViewLatoBold tvTitleToolbar;

    private String idDeliveryOrder, idItem, strUid, idMandor, idPengantar, idKantor, baseUrl;
    private boolean isUsingCamera, isUsingUsb, isInsert, isDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_delivery_order);
        Log.i(TAG, "onCreate");

        if (getIntent().getBooleanExtra("exit", false)) {
            finish();
        }

        db = new DatabaseHelper(this);
        dateSession = new DateSession(this);
        alertDialogLoading = new AlertDialogLoading(this);
        alertDialogInformation = new AlertDialogInformation(this, this);
        alertDialogConfirmation = new AlertDialogConfirmation(this, this);
        client = MainApplication.getUnsafeOkHttpClient();
        idDeliveryOrder = getIntent().getStringExtra("idDeliveryOrder");
        resetValue();

        LoginSession loginSession = new LoginSession(this);
        HashMap<String, String> hashMap = loginSession.getSession();
        idPengantar = hashMap.get(AppConfig.ID);
        idMandor = hashMap.get(AppConfig.ID_MANDOR);
        idKantor = hashMap.get(AppConfig.ID_KANTOR);
        baseUrl = hashMap.get(AppConfig.BASE_URL);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvTitleToolbar = (TextViewLatoBold) findViewById(R.id.tv_title);
        tvTitleToolbar.setText(getResources().getString(R.string.title_delivery_order));

        TextViewLatoRegular tvDescToolbar = (TextViewLatoRegular) findViewById(R.id.tv_desc);
        tvDescToolbar.setText("No. " + idDeliveryOrder);

        constraintLayout = (ConstraintLayout) findViewById(R.id.info_container);
        constraintLayout.setVisibility(View.GONE);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setVisibility(View.GONE);

        FloatingActionButton fabType = (FloatingActionButton) findViewById(R.id.fab_type);
        fabType.setOnClickListener(this);
        FloatingActionButton fabCamera = (FloatingActionButton) findViewById(R.id.fab_camera);
        fabCamera.setOnClickListener(this);
        FloatingActionButton fabUsb = (FloatingActionButton) findViewById(R.id.fab_usb);
        fabUsb.setOnClickListener(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                arrayList.clear();
                int countItem = db.getCountAntaranDeliveryOrder(idDeliveryOrder);

                if (countItem > 0) {
                    constraintLayout.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    arrayList = db.getAllAntaranDeliveryOrder(idDeliveryOrder);
                    adapter = new ItemDeliveryOrderAdapter(DeliveryOrderActivity.this, arrayList, DeliveryOrderActivity.this);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);

                    tvTitleToolbar.setText(getResources().getString(R.string.title_delivery_order) + " | " + arrayList.size() + " ITEM");
                } else {
                    constraintLayout.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
        }, 500);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.fab_type:
                isUsingCamera = false;
                isUsingUsb = false;
                showDialogEntryItem();
                break;
            case R.id.fab_camera:
                isUsingCamera = true;
                isUsingUsb = false;
                showCameraScanner();
                break;
            case R.id.fab_usb:
                isUsingCamera = false;
                isUsingUsb = true;
                showDialogEntryItem();
                break;
        }
    }

    private void showCameraScanner() {
        Log.i(TAG, "showCameraScanner");
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.setCaptureActivity(AnyOrientation.class);
        intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        intentIntegrator.setPrompt("Letakkan barcode tepat pada garis merah");
        intentIntegrator.setOrientationLocked(true);
        intentIntegrator.setBeepEnabled(true);
        intentIntegrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(intentResult != null) {
            if (resultCode == RESULT_OK) {
                idItem = intentResult.getContents();

                if (idItem.isEmpty()) {
                    Toasty.error(this, getResources().getString(R.string.id_item_empty), 3000, false).show();
                } else if (idItem.length() < 11) {
                    if (idItem.length() < 9 || idItem.length() == 10) {
                        Toasty.error(this, getResources().getString(R.string.id_item_min_length), 3000, false)
                                .show();
                    }
                } else if (!idItem.matches(AppConfig.ID_ITEM_PATTERN)) {
                    Toasty.error(this, getString(R.string.id_item_invalid_format), 3000, false).show();
                } else {
                    try {
                        insertItemIntoServer();
                        //getTokenFromServer();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void showDialogEntryItem() {
        Log.i(TAG, "showDialogEntryItem");
        View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_entry_item, null);
        TextViewLatoBold tvTitle = (TextViewLatoBold) view.findViewById(R.id.tv_title);
        tvTitle.setText(getResources().getString(R.string.title_entry_item));

        if (isUsingUsb) {
            Log.i(TAG, "isUsingUSB true");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setView(view);

            final AlertDialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(true);
            //alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            alertDialog.show();

            etIdItem = (TextInputEditTextLatoRegular) view.findViewById(R.id.et_id_item);
            etIdItem.addTextChangedListener(new TextWatcher() {
                Timer timer = new Timer();

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void afterTextChanged(final Editable s) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    idItem = etIdItem.getText().toString().trim();

                                    if (s.toString().equals("0") && s.toString().length() == 1) {
                                        s.replace(0, 1, "");
                                    } else {
                                        if (s.length() == 9 || s.length() >= 11) {
                                            if (!idItem.matches(AppConfig.ID_ITEM_PATTERN)) {
                                                idItem = "";
                                                etIdItem.setText("");
                                                Toasty.error(DeliveryOrderActivity.this, getResources().getString(R.string.id_item_invalid_format), 3000, false).show();
                                            } else {
                                                try {
                                                    alertDialog.dismiss();
                                                    insertItemIntoServer();
                                                    //getTokenFromServer();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }, 500);
                }
            });

            ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
            btnPositive.setVisibility(View.GONE);
        } else {
            Log.i(TAG, "isUsingUSB false");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setView(view);

            final AlertDialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(true);
            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            alertDialog.show();

            etIdItem = (TextInputEditTextLatoRegular) view.findViewById(R.id.et_id_item);
            etIdItem.addTextChangedListener(new PreventZeroOnFirstType(etIdItem));
            etIdItem.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (validateIdItem()) {
                            try {
                                alertDialog.dismiss();
                                insertItemIntoServer();
                                //getTokenFromServer();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    return false;
                }
            });

            ButtonLatoBold btnPositive = (ButtonLatoBold) view.findViewById(R.id.btn_positive);
            btnPositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateIdItem()) {
                        try {
                            alertDialog.dismiss();
                            insertItemIntoServer();
                            //getTokenFromServer();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private boolean validateIdItem() {
        Log.d(TAG, "validateIdItem");
        idItem = etIdItem.getText().toString().trim();

        if (idItem.isEmpty()) {
            Toasty.error(this, getResources().getString(R.string.id_item_empty), 3000, false)
                    .show();
            return false;
        } else if (idItem.length() < 11) {
            if (idItem.length() < 9 || idItem.length() == 10) {
                Toasty.error(this, getResources().getString(R.string.id_item_min_length), 3000, false)
                        .show();
                return false;
            }
        } else if (!idItem.matches(AppConfig.ID_ITEM_PATTERN)) {
            Toasty.error(this, getResources().getString(R.string.id_item_invalid_format), 3000, false)
                    .show();
            return false;
        }

        return true;
    }

    private void insertItemIntoServer() throws IOException {
        Log.i(TAG, "insertItemIntoServer");
        alertDialogLoading.setCancelAble(false).showDialog();

        Date date = new Date(System.currentTimeMillis());
        String currentDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(date);
        final String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);

        final RequestBody formBody = new FormBody.Builder()
                .add("action", "insert")
                .add(AppConfig.ID_ITEM, idItem)
                .add(AppConfig.ID_DELIVERY_ORDER, idDeliveryOrder)
                .add(AppConfig.WAKTU_ENTRI, currentDateTime)
                .add(AppConfig.ID_MANDOR, idMandor)
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .add(AppConfig.ID_KANTOR, idKantor)
                .add(AppConfig.TANGGAL_PERANGKAT, currentDate)
                .build();

        Request request = new Request.Builder()
                //.url("https://httpbin.org/delay/10")
                .url(baseUrl + "antaran.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogLoading.dismissDialog();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogLoading.dismissDialog();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (isError) {
                                        String errorType = jsonObject.getString(AppConfig.ERROR_TYPE);
                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);

                                        if (errorType.equalsIgnoreCase("alert")) {
                                            alertDialogLoading.dismissDialog();
                                            alertDialogInformation
                                                    .setMessage(errorMessage)
                                                    .showDialog();
                                        } else {
                                            alertDialogLoading.dismissDialog();
                                            Toasty.error(DeliveryOrderActivity.this, errorMessage, 3000, false).show();

                                            if (isUsingCamera) {
                                                showCameraScanner();
                                            } else {
                                                showDialogEntryItem();
                                            }
                                        }
                                    } else {
                                        JSONObject antaran = jsonObject.getJSONObject("antaran");
                                        String uid = antaran.getString(AppConfig.UID);
                                        String idItem = antaran.getString(AppConfig.ID_ITEM);
                                        String idDeliveryOrder = antaran.getString(AppConfig.ID_DELIVERY_ORDER);
                                        String idStatus = antaran.getString(AppConfig.ID_STATUS);
                                        String keterangan = antaran.getString(AppConfig.KETERANGAN);
                                        String waktuEntri = antaran.getString(AppConfig.WAKTU_ENTRI);
                                        String waktuUpdate = antaran.getString(AppConfig.WAKTU_UPDATE);
                                        double garisLintang = 0.000000;
                                        double garisBujur = 0.000000;
                                        String tandaTangan = antaran.getString(AppConfig.TANDA_TANGAN);
                                        String foto = antaran.getString(AppConfig.FOTO);
                                        String namaPengirim = antaran.getString(AppConfig.NAMA_PENGIRIM);
                                        String alamatPengirim = antaran.getString(AppConfig.ALAMAT_PENGIRIM);
                                        String hpPengirim = antaran.getString(AppConfig.HP_PENGIRIM);
                                        String namaPenerima = antaran.getString(AppConfig.NAMA_PENERIMA);
                                        String alamatPenerima = antaran.getString(AppConfig.ALAMAT_PENERIMA);
                                        String hpPenerima = antaran.getString(AppConfig.HP_PENERIMA);
                                        String idProduk = antaran.getString(AppConfig.ID_PRODUK);
                                        double berat = Double.parseDouble(antaran.getString(AppConfig.BERAT));
                                        double bsuCod = Double.parseDouble(antaran.getString(AppConfig.BSU_COD));
                                        double bsuBlb = Double.parseDouble(antaran.getString(AppConfig.BSU_BLB));
                                        double bsuTax = Double.parseDouble(antaran.getString(AppConfig.BSU_TAX));
                                        String kantorAsal = antaran.getString(AppConfig.KANTOR_ASAL);
                                        String kantorTujuan = antaran.getString(AppConfig.KANTOR_TUJUAN);
                                        String idCustomer = antaran.getString(AppConfig.ID_CUSTOMER);
                                        String idExternal = antaran.getString(AppConfig.ID_EXTERNAL);
                                        String tglPosting = antaran.getString(AppConfig.TGL_POSTING);
                                        String tipe = antaran.getString(AppConfig.TIPE);
                                        String alamatBaru = antaran.getString(AppConfig.ALAMAT_BARU);
                                        String catatan = antaran.getString(AppConfig.CATATAN);

                                        boolean isInserted = db.insertIntoAntaran(
                                            new Antaran(
                                                uid, idItem, idDeliveryOrder, idStatus, keterangan, waktuEntri,
                                                waktuUpdate, garisLintang, garisBujur, tandaTangan, foto, namaPengirim,
                                                alamatPengirim, hpPengirim, namaPenerima, alamatPenerima, hpPenerima,
                                                idProduk, berat, bsuCod, bsuBlb, bsuTax, kantorAsal, kantorTujuan,
                                                idCustomer, idExternal, tglPosting, tipe, alamatBaru, catatan, "0",
                                                "0", idPengantar
                                            )
                                        );

                                        dateSession.setSession(currentDate);
                                        alertDialogLoading.dismissDialog();
                                        isInsert = isInserted;
                                        addItemIntoList();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void addItemIntoList() {
        Log.i(TAG, "addItemIntoList");
        arrayList = db.getAllAntaranDeliveryOrder(idDeliveryOrder);
        adapter = new ItemDeliveryOrderAdapter(this, arrayList, this);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        if (arrayList.size() == 0) {
            constraintLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

            tvTitleToolbar.setText(getResources().getString(R.string.title_delivery_order));
            Toasty.success(this, getResources().getString(R.string.id_item_delete_success), 3000, false).show();
            isDelete = false;
        } else {
            constraintLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            if (isDelete) {
                tvTitleToolbar.setText(getResources().getString(R.string.title_delivery_order) + " | " + arrayList.size() + " ITEM");

                Toasty.success(this, getResources().getString(R.string.id_item_delete_success), 3000, false).show();
                isDelete = false;
            } else {
                if (isUsingCamera) {
                    idItem = "";
                    tvTitleToolbar.setText(getResources().getString(R.string.title_delivery_order) + " | " + arrayList.size() + " ITEM");

                    /*Toasty.success(this, getResources().getString(R.string.id_item_insert_success), 3000, false).show();
                    showCameraScanner();
                    isInsert = false;*/

                    if (isInsert) {
                        Toasty.success(this, getResources().getString(R.string.id_item_insert_success), 3000, false).show();
                    } else {
                        Toasty.error(this, getResources().getString(R.string.id_item_insert_failed), 3000, false).show();
                    }

                    showCameraScanner();
                    isInsert = false;
                } else {
                    idItem = "";
                    etIdItem.setText("");
                    tvTitleToolbar.setText(getResources().getString(R.string.title_delivery_order) + " | " + arrayList.size() + " ITEM");

                    /*Toasty.success(this, getResources().getString(R.string.id_item_insert_success), 3000, false).show();
                    showDialogEntryItem();
                    isInsert = false;*/

                    if (isInsert) {
                        Toasty.success(this, getResources().getString(R.string.id_item_insert_success), 3000, false).show();
                    } else {
                        Toasty.error(this, getResources().getString(R.string.id_item_insert_failed), 3000, false).show();
                    }

                    showDialogEntryItem();
                    isInsert = false;
                }
            }
        }
    }

    @Override
    public void positiveButtonClickedOnAlertDialogInformation(String action) {
        if (action.equalsIgnoreCase("finish")) {
            startActivity(new Intent(DeliveryOrderActivity.this, MainActivity.class));
            finish();
        }
    }

    @Override
    public void clearButtonClicked(String uid) {
        resetValue();
        strUid = uid;
        alertDialogConfirmation
                .setMessage("Anda yakin akan menghapus item tersebut?")
                .setActionPositive("deleteItem")
                .showDialog();
    }

    private void deleteItemFromServer(final String uid) throws IOException {
        Log.i(TAG, "deleteItemFromServer");
        alertDialogLoading.setCancelAble(false).showDialog();

        RequestBody formBody = new FormBody.Builder()
                .add("action", "delete_by_uid")
                .add(AppConfig.UID, uid)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "antaran.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogLoading.dismissDialog();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogLoading.dismissDialog();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (isError) {
                                        alertDialogLoading.dismissDialog();

                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    } else {
                                        if (db.deleteAntaranByUid(uid)) {
                                            alertDialogLoading.dismissDialog();
                                            strUid = "";
                                            isDelete = true;
                                            addItemIntoList();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public void positiveButtonClickedOnAlertDialogConfirmation(String action) {
        if (action.equalsIgnoreCase("deleteItem")) {
            try {
                deleteItemFromServer(strUid);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (action.equalsIgnoreCase("deleteItems")) {
            try {
                deteleItemsFromServer(idDeliveryOrder);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (action.equalsIgnoreCase("closeDeliveryOrder")) {
            try {
                closeDeliveryOrder();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void negativeButtonClickedOnAlertDialogConfirmation(String action) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delivery_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case  android.R.id.home:
                startActivity(new Intent(DeliveryOrderActivity.this, MainActivity.class));
                finish();
                return true;
            case R.id.nav_close_do:
                resetValue();
                if (arrayList.size() > 0) {
                    String message = "Anda yakin akan melakukan tutup delivery order dengan jumlah " + arrayList.size() + " item?";
                    alertDialogConfirmation
                            .setMessage(message)
                            .setActionPositive("closeDeliveryOrder")
                            .showDialog();
                } else {
                    String message = "Belum ada item pada daftar delivery order";
                    alertDialogInformation
                            .setMessage(message)
                            .showDialog();
                }
                return true;
            case R.id.nav_delete_items:
                resetValue();
                if (arrayList.size() > 0) {
                    String message = "Anda yakin akan menghapus semua item?";
                    alertDialogConfirmation
                            .setMessage(message)
                            .setActionPositive("deleteItems")
                            .showDialog();
                } else {
                    String message = "Belum ada item pada daftar delivery order";
                    alertDialogInformation
                            .setMessage(message)
                            .showDialog();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void closeDeliveryOrder() throws IOException {
        Log.i(TAG, "closeDeliveryOrder");
        alertDialogLoading.setCancelAble(false).showDialog();

        RequestBody formBody = new FormBody.Builder()
                .add("action", "close")
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .add(AppConfig.ID_DELIVERY_ORDER, idDeliveryOrder)
                .add(AppConfig.JUMLAH_ITEM, String.valueOf(arrayList.size()))
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "delivery_order.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogLoading.dismissDialog();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                e.printStackTrace();
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        //throw new IOException("Unexpected code " + response.code() + " : " + response.message());
                        final String errorMessage = response.code() + " " + response.message();

                        DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogLoading.dismissDialog();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (isError) {
                                        alertDialogLoading.dismissDialog();

                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    } else {
                                        boolean isClose;
                                        int size = arrayList.size();
                                        int numRetries = 0;

                                        do {
                                            int numRows = db.updateStatusTutupIntoAntaran(idDeliveryOrder);
                                            if (numRows == size) {
                                                isClose = true;
                                                alertDialogLoading.dismissDialog();

                                                String message = "Berhasil melakukan tutup delivery order dengan jumlah " + numRows + " item";
                                                alertDialogInformation
                                                        .setMessage(message)
                                                        .setActionPositive("finish")
                                                        .showDialog();
                                            } else {
                                                numRetries++;
                                                isClose = false;

                                                if (numRetries == 3) {
                                                    isClose = true;
                                                    alertDialogLoading.dismissDialog();

                                                    String message = "Berhasil melakukan tutup delivery order dengan jumlah " + numRows + " item";
                                                    alertDialogInformation
                                                            .setMessage(message)
                                                            .setActionPositive("finish")
                                                            .showDialog();
                                                }
                                            }
                                        } while (!isClose);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void deteleItemsFromServer(final String idDeliveryOrder) throws IOException {
        Log.i(TAG, "deleteItemsFromServer");
        alertDialogLoading.setCancelAble(false).showDialog();

        RequestBody formBody = new FormBody.Builder()
                .add("action", "delete_by_id_delivery_order")
                .add(AppConfig.ID_DELIVERY_ORDER, idDeliveryOrder)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "antaran.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alertDialogLoading.dismissDialog();
                        alertDialogInformation
                                .setMessage(getResources().getString(R.string.undefined_error))
                                .showDialog();
                    }
                });

                call.cancel();
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        final String errorMessage = response.code() + " " + response.message();

                        DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alertDialogLoading.dismissDialog();
                                alertDialogInformation
                                        .setMessage(errorMessage)
                                        .showDialog();
                            }
                        });
                    } else {
                        final String responseData = responseBody.string();

                        DeliveryOrderActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseData);
                                    boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                                    if (isError) {
                                        alertDialogLoading.dismissDialog();

                                        String errorMessage = jsonObject.getString(AppConfig.ERROR_MESSAGE);
                                        alertDialogInformation
                                                .setMessage(errorMessage)
                                                .showDialog();
                                    } else {
                                        if (db.deleteAntaranByIdDeliveryOrder(idDeliveryOrder)) {
                                            alertDialogLoading.dismissDialog();
                                            isDelete = true;
                                            addItemIntoList();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(DeliveryOrderActivity.this, MainActivity.class));
        finish();
    }

    private void resetValue() {
        Log.i(TAG, "resetValue");
        isUsingCamera = false;
        isUsingUsb =false;
        isInsert = false;
        isDelete = false;
    }
}
