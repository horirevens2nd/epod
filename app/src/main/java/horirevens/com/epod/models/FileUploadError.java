package horirevens.com.epod.models;

public class FileUploadError {
    private String uid;
    private String namaFile;
    private String direktori;
    private String idPengantar;

    public FileUploadError() {

    }

    // Insert
    public FileUploadError(String uid, String namaFile, String direktori, String idPengantar) {
        this.uid = uid;
        this.namaFile = namaFile;
        this.direktori = direktori;
        this.idPengantar = idPengantar;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamaFile() {
        return namaFile;
    }

    public void setNamaFile(String namaFile) {
        this.namaFile = namaFile;
    }

    public String getDirektori() {
        return direktori;
    }

    public void setDirektori(String direktori) {
        this.direktori = direktori;
    }

    public String getIdPengantar() {
        return idPengantar;
    }

    public void setIdPengantar(String idPengantar) {
        this.idPengantar = idPengantar;
    }
}
