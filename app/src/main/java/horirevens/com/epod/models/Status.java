package horirevens.com.epod.models;

/**
 * Created by horirevens on 3/19/18.
 */

public class Status {
    private String id;
    private String keterangan;
    private String tindakan;
    private String kodeLn;
    private String kodeDn;
    private String urutan;
    private String level;
    private String terakhir;
    private String idStatusInduk;

    public Status() {
        // Do nothing
    }

    // Insert & Update
    public Status(
            String id, String keterangan, String tindakan, String kodeLn, String kodeDn, String urutan, String level,
            String terakhir, String idStatusInduk
    ) {
        this.id = id;
        this.keterangan = keterangan;
        this.tindakan = tindakan;
        this.kodeLn = kodeLn;
        this.kodeDn = kodeDn;
        this.urutan = urutan;
        this.level = level;
        this.terakhir = terakhir;
        this.idStatusInduk = idStatusInduk;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getTindakan() {
        return tindakan;
    }

    public void setTindakan(String tindakan) {
        this.tindakan = tindakan;
    }

    public String getKodeLn() {
        return kodeLn;
    }

    public void setKodeLn(String kodeLn) {
        this.kodeLn = kodeLn;
    }

    public String getKodeDn() {
        return kodeDn;
    }

    public void setKodeDn(String kodeDn) {
        this.kodeDn = kodeDn;
    }

    public String getUrutan() {
        return urutan;
    }

    public void setUrutan(String urutan) {
        this.urutan = urutan;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTerakhir() {
        return terakhir;
    }

    public void setTerakhir(String terakhir) {
        this.terakhir = terakhir;
    }

    public String getIdStatusInduk() {
        return idStatusInduk;
    }

    public void setIdStatusInduk(String idStatusInduk) {
        this.idStatusInduk = idStatusInduk;
    }
}
