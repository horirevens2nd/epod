package horirevens.com.epod.models;

public class ItemCollective {
    private String uid;
    private String idItem;
    private String idDeliveryOrder;
    private String waktuEntri;

    public ItemCollective() {
        // Do nothing
    }

    public ItemCollective(String uid, String idItem, String idDeliveryOrder, String waktuEntri) {
        this.uid = uid;
        this.idItem = idItem;
        this.idDeliveryOrder = idDeliveryOrder;
        this.waktuEntri = waktuEntri;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getIdItem() {
        return idItem;
    }

    public void setIdItem(String idItem) {
        this.idItem = idItem;
    }

    public String getIdDeliveryOrder() {
        return idDeliveryOrder;
    }

    public void setIdDeliveryOrder(String idDeliveryOrder) {
        this.idDeliveryOrder = idDeliveryOrder;
    }

    public String getWaktuEntri() {
        return waktuEntri;
    }

    public void setWaktuEntri(String waktuEntri) {
        this.waktuEntri = waktuEntri;
    }
}
