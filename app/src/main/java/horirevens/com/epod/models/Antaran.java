package horirevens.com.epod.models;

/**
 * Created by horirevens on 3/19/18.
 */

public class Antaran {

    private String uid;
    private String idItem;
    private String idDeliveryOrder;
    private String idStatus;
    private String keterangan;
    private String waktuEntri;
    private String waktuUpdate;
    private double garisLintang;
    private double garisBujur;
    private String tandaTangan;
    private String foto;
    private String namaPengirim;
    private String alamatPengirim;
    private String hpPengirim;
    private String namaPenerima;
    private String alamatPenerima;
    private String hpPenerima;
    private String idProduk;
    private double berat;
    private double bsuCod;
    private double bsuBlb;
    private double bsuTax;
    private String kantorAsal;
    private String kantorTujuan;
    private String idCustomer;
    private String idExternal;
    private String tglPosting;
    private String tipe;
    private String alamatBaru;
    private String catatan;
    private String statusTutup;
    private String statusUpdate;
    private boolean isItemChecked = false;
    private String idPengantar;

    public Antaran() {
        // Do nothing
    }

    // Update
    public Antaran(
            String uid, String idStatus, String keterangan, String waktuUpdate, double garisLintang, double garisBujur,
            String tandaTangan, String foto, String alamatBaru, String catatan, String statusUpdate
    ) {
        this.uid = uid;
        this.idStatus = idStatus;
        this.keterangan = keterangan;
        this.waktuUpdate = waktuUpdate;
        this.garisLintang = garisLintang;
        this.garisBujur = garisBujur;
        this.tandaTangan = tandaTangan;
        this.foto = foto;
        this.alamatBaru = alamatBaru;
        this.catatan = catatan;
        this.statusUpdate = statusUpdate;
    }

    // Update collective
    public Antaran(
            String uid, String idStatus, String keterangan, String waktuUpdate, double garisLintang, double garisBujur,
            String tandaTangan, String foto, String alamatBaru, String catatan, String statusUpdate, boolean isItemChecked
    ) {
        this.uid = uid;
        this.idStatus = idStatus;
        this.keterangan = keterangan;
        this.waktuUpdate = waktuUpdate;
        this.garisLintang = garisLintang;
        this.garisBujur = garisBujur;
        this.tandaTangan = tandaTangan;
        this.foto = foto;
        this.alamatBaru = alamatBaru;
        this.catatan = catatan;
        this.statusUpdate = statusUpdate;
        this.isItemChecked = isItemChecked;
    }

    // Insert
    public Antaran(
            String uid, String idItem, String idDeliveryOrder, String idStatus, String keterangan, String waktuEntri,
            String waktuUpdate, double garisLintang, double garisBujur, String tandaTangan, String foto,
            String namaPengirim, String alamatPengirim, String hpPengirim, String namaPenerima, String alamatPenerima
            , String hpPenerima, String idProduk, double berat, double bsuCod, double bsuBlb, double bsuTax, String kantorAsal,
            String kantorTujuan, String idCustomer, String idExternal, String tglPosting, String tipe, String alamatBaru,
            String catatan, String statusTutup, String statusUpdate, String idPengantar
    ) {
        this.uid = uid;
        this.idItem = idItem;
        this.idDeliveryOrder = idDeliveryOrder;
        this.idStatus = idStatus;
        this.keterangan = keterangan;
        this.waktuEntri = waktuEntri;
        this.waktuUpdate = waktuUpdate;
        this.garisLintang = garisLintang;
        this.garisBujur = garisBujur;
        this.tandaTangan = tandaTangan;
        this.foto = foto;
        this.namaPengirim = namaPengirim;
        this.alamatPengirim = alamatPengirim;
        this.hpPengirim = hpPengirim;
        this.namaPenerima = namaPenerima;
        this.alamatPenerima = alamatPenerima;
        this.hpPenerima = hpPenerima;
        this.idProduk = idProduk;
        this.berat = berat;
        this.bsuCod = bsuCod;
        this.bsuBlb = bsuBlb;
        this.bsuTax = bsuTax;
        this.kantorAsal = kantorAsal;
        this.kantorTujuan = kantorTujuan;
        this.idCustomer = idCustomer;
        this.idExternal = idExternal;
        this.tglPosting = tglPosting;
        this.tipe = tipe;
        this.alamatBaru = alamatBaru;
        this.catatan = catatan;
        this.statusTutup = statusTutup;
        this.statusUpdate = statusUpdate;
        this.idPengantar = idPengantar;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getIdItem() {
        return idItem;
    }

    public void setIdItem(String idItem) {
        this.idItem = idItem;
    }

    public String getIdDeliveryOrder() {
        return idDeliveryOrder;
    }

    public void setIdDeliveryOrder(String idDeliveryOrder) {
        this.idDeliveryOrder = idDeliveryOrder;
    }

    public String getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(String idStatus) {
        this.idStatus = idStatus;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getWaktuEntri() {
        return waktuEntri;
    }

    public void setWaktuEntri(String waktuEntri) {
        this.waktuEntri = waktuEntri;
    }

    public String getWaktuUpdate() {
        return waktuUpdate;
    }

    public void setWaktuUpdate(String waktuUpdate) {
        this.waktuUpdate = waktuUpdate;
    }

    public double getGarisLintang() {
        return garisLintang;
    }

    public void setGarisLintang(double garisLintang) {
        this.garisLintang = garisLintang;
    }

    public double getGarisBujur() {
        return garisBujur;
    }

    public void setGarisBujur(double garisBujur) {
        this.garisBujur = garisBujur;
    }

    public String getTandaTangan() {
        return tandaTangan;
    }

    public void setTandaTangan(String tandaTangan) {
        this.tandaTangan = tandaTangan;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNamaPengirim() {
        return namaPengirim;
    }

    public void setNamaPengirim(String namaPengirim) {
        this.namaPengirim = namaPengirim;
    }

    public String getAlamatPengirim() {
        return alamatPengirim;
    }

    public void setAlamatPengirim(String alamatPengirim) {
        this.alamatPengirim = alamatPengirim;
    }

    public String getHpPengirim() {
        return hpPengirim;
    }

    public void setHpPengirim(String hpPengirim) {
        this.hpPengirim = hpPengirim;
    }

    public String getNamaPenerima() {
        return namaPenerima;
    }

    public void setNamaPenerima(String namaPenerima) {
        this.namaPenerima = namaPenerima;
    }

    public String getAlamatPenerima() {
        return alamatPenerima;
    }

    public void setAlamatPenerima(String alamatPenerima) {
        this.alamatPenerima = alamatPenerima;
    }

    public String getHpPenerima() {
        return hpPenerima;
    }

    public void setHpPenerima(String hpPenerima) {
        this.hpPenerima = hpPenerima;
    }

    public String getIdProduk() {
        return idProduk;
    }

    public void setIdProduk(String idProduk) {
        this.idProduk = idProduk;
    }

    public double getBerat() {
        return berat;
    }

    public void setBerat(double berat) {
        this.berat = berat;
    }

    public double getBsuCod() {
        return bsuCod;
    }

    public void setBsuCod(double bsuCod) {
        this.bsuCod = bsuCod;
    }

    public double getBsuBlb() {
        return bsuBlb;
    }

    public void setBsuBlb(double bsuBlb) {
        this.bsuBlb = bsuBlb;
    }

    public double getBsuTax() {
        return bsuTax;
    }

    public void setBsuTax(double bsuTax) {
        this.bsuTax = bsuTax;
    }

    public String getKantorAsal() {
        return kantorAsal;
    }

    public void setKantorAsal(String kantorAsal) {
        this.kantorAsal = kantorAsal;
    }

    public String getKantorTujuan() {
        return kantorTujuan;
    }

    public void setKantorTujuan(String kantorTujuan) {
        this.kantorTujuan = kantorTujuan;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getIdExternal() {
        return idExternal;
    }

    public void setIdExternal(String idExternal) {
        this.idExternal = idExternal;
    }

    public String getTglPosting() {
        return tglPosting;
    }

    public void setTglPosting(String tglPosting) {
        this.tglPosting = tglPosting;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getAlamatBaru() {
        return alamatBaru;
    }

    public void setAlamatBaru(String alamatBaru) {
        this.alamatBaru = alamatBaru;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public String getStatusTutup() {
        return statusTutup;
    }

    public void setStatusTutup(String statusTutup) {
        this.statusTutup = statusTutup;
    }

    public String getStatusUpdate() {
        return statusUpdate;
    }

    public void setStatusUpdate(String statusUpdate) {
        this.statusUpdate = statusUpdate;
    }

    public boolean isChecked() {
        return isItemChecked;
    }

    public void setItemChecked(boolean isItemChecked) {
        this.isItemChecked = isItemChecked;
    }

    public String getIdPengantar() {
        return idPengantar;
    }

    public void setIdPengantar(String idPengantar) {
        this.idPengantar = idPengantar;
    }

}
