package horirevens.com.epod.models;

public class FileUploadUsage {
    private String uid;
    private String tanggal;
    private int jmlBerkas;
    private int jmlBerkasFoto;
    private int jmlBerkasTtd;
    private double jmlUkuran;
    private double jmlUkuranFoto;
    private double jmlUkuranTtd;
    private double transmit;
    private double receive;
    private String statusUpdate;
    private String idPengantar;

    public FileUploadUsage() {

    }

    // Insert without Tx/Rx
    public FileUploadUsage(
            String uid, String tanggal, int jmlBerkas, int jmlBerkasFoto, int jmlBerkasTtd, double jmlUkuran, double jmlUkuranFoto,
            double jmlUkuranTtd, String statusUpdate, String idPengantar
    ) {
        this.uid = uid;
        this.tanggal = tanggal;
        this.jmlBerkas = jmlBerkas;
        this.jmlBerkasFoto = jmlBerkasFoto;
        this.jmlBerkasTtd = jmlBerkasTtd;
        this.jmlUkuran = jmlUkuran;
        this.jmlUkuranFoto = jmlUkuranFoto;
        this.jmlUkuranTtd = jmlUkuranTtd;
        this.statusUpdate = statusUpdate;
        this.idPengantar = idPengantar;
    }

    // Update without Tx/Rx
    public FileUploadUsage(
            String uid, int jmlBerkas, int jmlBerkasFoto, int jmlBerkasTtd, double jmlUkuran, double jmlUkuranFoto,
            double jmlUkuranTtd, String statusUpdate
    ) {
        this.uid = uid;
        this.jmlBerkas = jmlBerkas;
        this.jmlBerkasFoto = jmlBerkasFoto;
        this.jmlBerkasTtd = jmlBerkasTtd;
        this.jmlUkuran = jmlUkuran;
        this.jmlUkuranFoto = jmlUkuranFoto;
        this.jmlUkuranTtd = jmlUkuranTtd;
        this.statusUpdate = statusUpdate;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getJmlBerkas() {
        return jmlBerkas;
    }

    public void setJmlBerkas(int jmlBerkas) {
        this.jmlBerkas = jmlBerkas;
    }

    public int getJmlBerkasFoto() {
        return jmlBerkasFoto;
    }

    public void setJmlBerkasFoto(int jmlBerkasFoto) {
        this.jmlBerkasFoto = jmlBerkasFoto;
    }

    public int getJmlBerkasTtd() {
        return jmlBerkasTtd;
    }

    public void setJmlBerkasTtd(int jmlBerkasTtd) {
        this.jmlBerkasTtd = jmlBerkasTtd;
    }

    public double getJmlUkuran() {
        return jmlUkuran;
    }

    public void setJmlUkuran(double jmlUkuran) {
        this.jmlUkuran = jmlUkuran;
    }

    public double getJmlUkuranFoto() {
        return jmlUkuranFoto;
    }

    public void setJmlUkuranFoto(double jmlUkuranFoto) {
        this.jmlUkuranFoto = jmlUkuranFoto;
    }

    public double getJmlUkuranTtd() {
        return jmlUkuranTtd;
    }

    public void setJmlUkuranTtd(double jmlUkuranTtd) {
        this.jmlUkuranTtd = jmlUkuranTtd;
    }

    public double getTransmit() {
        return transmit;
    }

    public void setTransmit(double transmit) {
        this.transmit = transmit;
    }

    public double getReceive() {
        return receive;
    }

    public void setReceive(double receive) {
        this.receive = receive;
    }

    public String getStatusUpdate() {
        return statusUpdate;
    }

    public void setStatusUpdate(String statusUpdate) {
        this.statusUpdate = statusUpdate;
    }

    public String getIdPengantar() {
        return idPengantar;
    }

    public void setIdPengantar(String idPengantar) {
        this.idPengantar = idPengantar;
    }
}
