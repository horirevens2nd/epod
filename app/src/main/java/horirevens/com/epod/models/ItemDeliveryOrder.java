package horirevens.com.epod.models;

/**
 * Created by horirevens on 3/14/18.
 */

public class ItemDeliveryOrder {
    private String uid;
    private String idItem;

    public ItemDeliveryOrder() {
        // Do nothing
    }

    public ItemDeliveryOrder(String uid, String idItem) {
        this.uid = uid;
        this.idItem = idItem;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getIdItem() {
        return idItem;
    }

    public void setIdItem(String idItem) {
        this.idItem = idItem;
    }
}
