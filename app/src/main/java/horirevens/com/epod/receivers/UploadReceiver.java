package horirevens.com.epod.receivers;

import android.content.Context;
import android.util.Log;

import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import horirevens.com.epod.AppConfig;
import horirevens.com.epod.LoginSession;
import horirevens.com.epod.MainApplication;
import horirevens.com.epod.helpers.DatabaseHelper;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class UploadReceiver extends UploadServiceBroadcastReceiver {

    private static final String TAG = UploadReceiver.class.getName();
    private DatabaseHelper db;
    private LoginSession loginSession;
    private OkHttpClient client;

    @Override
    public void onProgress(Context context, UploadInfo uploadInfo) {
        // your implementation
    }

    @Override
    public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
        client = MainApplication.getUnsafeOkHttpClient();
        db = new DatabaseHelper(context);
        String uploadId = uploadInfo.getUploadId();
        String namaFile = db.getNamaFileFromFileUploadError(uploadId);
        String direktori = db.getDirektoriFromFileUploadError(uploadId);

        loginSession = new LoginSession(context);
        HashMap<String, String> hashMap = loginSession.getSession();
        String idPengantar = hashMap.get(AppConfig.ID);
        String baseUrl = hashMap.get(AppConfig.BASE_URL);

        if (!namaFile.isEmpty() && !direktori.isEmpty()) {
            try {
                submitFileUploadErrorIntoServer(uploadId, idPengantar, namaFile, direktori, baseUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
        client = MainApplication.getUnsafeOkHttpClient();
        db = new DatabaseHelper(context);
        String uploadId = uploadInfo.getUploadId();
        String namaFile = db.getNamaFileFromFileUploadError(uploadId);

        loginSession = new LoginSession(context);
        HashMap<String, String> hashMap = loginSession.getSession();
        String idPengantar = hashMap.get(AppConfig.ID);
        String baseUrl = hashMap.get(AppConfig.BASE_URL);

        if (db.deleteFileUploadErrorByUid(uploadId)) {
            try {
                deleteFileUploadErrorFromServer(idPengantar, namaFile, baseUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCancelled(Context context, UploadInfo uploadInfo) {
        // your implementation
    }

    private void submitFileUploadErrorIntoServer(String uploadId, String idPengantar, String namaFile, String direktori, String baseUrl)
    throws IOException {
        Log.i(TAG, "submitFileUploadErrorIntoServer");
        RequestBody formBody = new FormBody.Builder()
                .add("action", "submit")
                .add(AppConfig.UID, uploadId)
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .add(AppConfig.NAMA_FILE, namaFile)
                .add(AppConfig.DIREKTORI, direktori)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "file_upload_error.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // Do nothing
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        // Do nothing
                    } else {
                        String responseData = responseBody.string();
                        try {
                            JSONObject jsonObject = new JSONObject(responseData);
                            boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                            if (!isError) {
                                JSONObject fileUploadError = jsonObject.getJSONObject("file_upload_error");
                                String action = fileUploadError.getString("action");
                                String uid = fileUploadError.getString(AppConfig.UID);
                                String idPengantar = fileUploadError.getString(AppConfig.ID_PENGANTAR);
                                String namaFile = fileUploadError.getString(AppConfig.NAMA_FILE);
                                String direktori = fileUploadError.getString(AppConfig.DIREKTORI);

                                if (action.equalsIgnoreCase("insert")) {
                                    // Do nothing
                                } else {
                                    // Do nothing
                                }
                            } else {
                                // Do nothing
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private void deleteFileUploadErrorFromServer(String idPengantar, String namaFile, String baseUrl)
            throws IOException {
        Log.i(TAG, "deleteFileUploadErrorFromServer");
        RequestBody formBody = new FormBody.Builder()
                .add("action", "delete")
                .add(AppConfig.ID_PENGANTAR, idPengantar)
                .add(AppConfig.NAMA_FILE, namaFile)
                .build();

        Request request = new Request.Builder()
                .url(baseUrl + "file_upload_error.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // Do nothing
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try(ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) {
                        // Do nothing
                    } else {
                        String responseData = responseBody.string();
                        try {
                            JSONObject jsonObject = new JSONObject(responseData);
                            boolean isError = jsonObject.getBoolean(AppConfig.ERROR);

                            if (!isError) {
                                // Do nothing
                            } else {
                                // Do nothing
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }
}
