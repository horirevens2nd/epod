package horirevens.com.epod;

import okhttp3.MediaType;

/**
 * Created by horirevens on 2/24/18.
 */

public class AppConfig {
    public static final String BASE_URL = "base_url";
    public static final String SERVER = "server";

    // Media Type
    public static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

    // JSON Response
    public static final String ERROR = "error";
    public static final String ERROR_CODE = "error_code";
    public static final String ERROR_MESSAGE = "error_message";
    public static final String ERROR_TYPE = "error_type";
    public static final String MESSAGE = "message";

    // Pattern
    public static final String ID_ITEM_PATTERN = "^[0-9A-Z\\s]+$";
    public static final String NAME_PATTERN = "^[a-zA-Z\\s]+$";
    public static final String NUMBER_PATTERN = "^[0-9\\s]+$";
    public static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-zA-Z])[a-zA-Z0-9]*";

    // Database column
    public static final String ALAMAT_BARU = "alamat_baru";
    public static final String ALAMAT_PENERIMA = "alamat_penerima";
    public static final String ALAMAT_PENGIRIM = "alamat_pengirim";
    public static final String BERAT = "berat";
    public static final String BSU_BLB = "bsu_blb";
    public static final String BSU_COD = "bsu_cod";
    public static final String BSU_TAX = "bsu_tax";
    public static final String CATATAN = "catatan";
    public static final String DIREKTORI = "direktori";
    public static final String FOTO = "foto";
    public static final String GARIS_BUJUR = "garis_bujur";
    public static final String GARIS_LINTANG = "garis_lintang";
    public static final String HP_PENERIMA = "hp_penerima";
    public static final String HP_PENGIRIM = "hp_pengirim";
    public static final String ID = "id";
    public static final String ID_CUSTOMER = "id_customer";
    public static final String ID_DELIVERY_ORDER = "id_delivery_order";
    public static final String ID_EXTERNAL = "id_external";
    public static final String ID_GRUP = "id_grup";
    public static final String ID_ITEM = "id_item";
    public static final String ID_KANTOR = "id_kantor";
    public static final String ID_LOGIN = "id_login";
    public static final String ID_MANDOR = "id_mandor";
    public static final String ID_PENGANTAR = "id_pengantar";
    public static final String ID_PRODUK = "id_produk";
    public static final String ID_STATUS = "id_status";
    public static final String ID_STATUS_INDUK = "id_status_induk";
    public static final String JML_BERKAS = "jml_berkas";
    public static final String JML_BERKAS_FOTO = "jml_berkas_foto";
    public static final String JML_BERKAS_TTD = "jml_berkas_ttd";
    public static final String JML_UKURAN = "jml_ukuran";
    public static final String JML_UKURAN_FOTO = "jml_ukuran_foto";
    public static final String JML_UKURAN_TTD = "jml_ukuran_ttd";
    public static final String JUMLAH_ITEM = "jumlah_item";
    public static final String KANTOR_ASAL = "kantor_asal";
    public static final String KANTOR_TUJUAN = "kantor_tujuan";
    public static final String KETERANGAN = "keterangan";
    public static final String KODE_DN = "kode_dn";
    public static final String KODE_LN = "kode_ln";
    public static final String LEVEL = "level";
    public static final String NAMA = "nama";
    public static final String NAMA_FILE = "nama_file";
    public static final String NAMA_MANDOR = "nama_mandor";
    public static final String NAMA_PENERIMA = "nama_penerima";
    public static final String NAMA_PENGIRIM = "nama_pengirim";
    public static final String STATUS_TUTUP = "status_tutup";
    public static final String STATUS_UPDATE = "status_update";
    public static final String TANDA_TANGAN = "tanda_tangan";
    public static final String TANGGAL = "tanggal";
    public static final String TANGGAL_PERANGKAT = "tanggal_perangkat";
    public static final String TERAKHIR = "terakhir";
    public static final String TGL_POSTING = "tgl_posting";
    public static final String TINDAKAN = "tindakan";
    public static final String TIPE = "tipe";
    public static final String TUTUP = "tutup";
    public static final String UID = "uid";
    public static final String URUTAN = "urutan";
    public static final String WAKTU_ENTRI = "waktu_entri";
    public static final String WAKTU_UPDATE = "waktu_update";

    public static final String FOLDER_SIGNATURE = "ePOD Signatures";
    public static final String FOLDER_IMAGE = "ePOD Images";
}
