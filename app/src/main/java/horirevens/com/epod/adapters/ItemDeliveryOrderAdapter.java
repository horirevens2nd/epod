package horirevens.com.epod.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import java.util.ArrayList;
import java.util.List;

import horirevens.com.epod.R;
import horirevens.com.epod.models.ItemDeliveryOrder;

/**
 * Created by horirevens on 3/14/18.
 */

public class ItemDeliveryOrderAdapter extends RecyclerView.Adapter<ItemDeliveryOrderAdapter.Holder> {
    private Context context;
    private List<ItemDeliveryOrder> list;
    private ArrayList<ItemDeliveryOrder> arrayList = new ArrayList<>();
    private ItemDeliveryOrderListener listener;

    public ItemDeliveryOrderAdapter(Context context, List<ItemDeliveryOrder> list, ItemDeliveryOrderListener listener) {
        this.context = context;
        this. list = list;
        this.listener = listener;
        arrayList.addAll(list);
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextViewLatoRegular tvNumber, tvIdItem;
        private ImageView ivClear;
        private ImageView ivDivider;

        public Holder(View itemView) {
            super(itemView);

            tvNumber = (TextViewLatoRegular) itemView.findViewById(R.id.tv_number);
            tvIdItem = (TextViewLatoRegular) itemView.findViewById(R.id.tv_id_item);
            ivClear = (ImageView) itemView.findViewById(R.id.iv_clear);
            ivDivider = (ImageView) itemView.findViewById(R.id.iv_divider);
        }
    }

    @Override
    public ItemDeliveryOrderAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item_delivery_order, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(ItemDeliveryOrderAdapter.Holder holder, int position) {
        final ItemDeliveryOrder itemDeliveryOrder = list.get(position);

        holder.tvNumber.setText(position + 1 + ". ");
        holder.tvIdItem.setText(itemDeliveryOrder.getIdItem());

        holder.ivClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clearButtonClicked(itemDeliveryOrder.getUid());
            }
        });

        int size = list.size() - 1;
        if (position == size) {
            holder.ivDivider.setVisibility(View.GONE);
        } else {
            holder.ivDivider.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ItemDeliveryOrderListener {

        void clearButtonClicked(String uid);

    }
}
