package horirevens.com.epod.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import horirevens.com.epod.R;
import horirevens.com.epod.helpers.DatabaseHelper;
import horirevens.com.epod.libraries.FlipAnimator;
import horirevens.com.epod.libraries.MyDate;
import horirevens.com.epod.models.Antaran;

/**
 * Created by horirevens on 3/22/18.
 */

public class MainFragmentTab3Adapter extends RecyclerView.Adapter<MainFragmentTab3Adapter.Holder> {
    private List<Antaran> list;
    private ArrayList<Antaran> arrayList = new ArrayList<>();
    private Context context;
    private static int currentSelectedIndex = -1;

    public MainFragmentTab3Adapter(Context context, List<Antaran> list) {
        this.context = context;
        this.list = list;
        arrayList.addAll(list);
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextViewLatoRegular tvNumber, tvIdItem, tvIdDeliveryOrder, tvKeterangan, tvWaktuUpdate, tvIdProduk,
                tvTipe;
        private ConstraintLayout syncContainer, unsyncContainer;
        private ImageView ivDivider;

        public Holder(View itemView) {
            super(itemView);
            tvIdProduk = (TextViewLatoRegular) itemView.findViewById(R.id.tv_id_produk);
            tvNumber = (TextViewLatoRegular) itemView.findViewById(R.id.tv_number);
            tvIdItem = (TextViewLatoRegular) itemView.findViewById(R.id.tv_id_item);
            tvIdDeliveryOrder = (TextViewLatoRegular) itemView.findViewById(R.id.tv_id_delivery_order);
            tvKeterangan = (TextViewLatoRegular) itemView.findViewById(R.id.tv_keterangan);
            tvWaktuUpdate = (TextViewLatoRegular) itemView.findViewById(R.id.tv_waktu_update);
            tvTipe = (TextViewLatoRegular) itemView.findViewById(R.id.tv_tipe);
            syncContainer = (ConstraintLayout) itemView.findViewById(R.id.sync_container);
            unsyncContainer = (ConstraintLayout) itemView.findViewById(R.id.unsync_container);
            ivDivider = (ImageView) itemView.findViewById(R.id.iv_divider);
        }
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_main_fragment3, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Antaran antaran = list.get(position);
        DatabaseHelper db = new DatabaseHelper(context);

        holder.tvNumber.setText(position + 1 + ". ");
        holder.tvIdItem.setText(antaran.getIdItem());
        holder.tvIdDeliveryOrder.setText(antaran.getIdDeliveryOrder());
        holder.tvKeterangan.setText(db.getKeteranganFromStatus(antaran.getIdStatus()));
        holder.tvWaktuUpdate.setText(MyDate.setToAmPm(antaran.getWaktuUpdate()));

        if (antaran.getIdProduk().equalsIgnoreCase("210")) {
            holder.tvIdProduk.setText("Surat Kilat Khusus");
        } else if (antaran.getIdProduk().equalsIgnoreCase("240")) {
            holder.tvIdProduk.setText("Paket Kilat Khusus");
        } else if (antaran.getIdProduk().equalsIgnoreCase("230")) {
            holder.tvIdProduk.setText("Paket Pos Biasa");
        } else if (antaran.getIdProduk().equalsIgnoreCase("417")) {
            holder.tvIdProduk.setText("Surat Express");
        } else if (antaran.getIdProduk().equalsIgnoreCase("447")) {
            holder.tvIdProduk.setText("Paket Express");
        } else if (antaran.getIdProduk().equalsIgnoreCase("PJE")) {
            holder.tvIdProduk.setText("Paket Jumbo Ekonomi");
        } else if (antaran.getIdProduk().equalsIgnoreCase("000")) {
            holder.tvIdProduk.setText("Not Found");
        } else {
            holder.tvIdProduk.setText(antaran.getIdProduk().toUpperCase());
        }

        if (antaran.getTipe().equalsIgnoreCase("NON COD")) {
            holder.tvTipe.setVisibility(View.INVISIBLE);
        } else if (antaran.getTipe().equalsIgnoreCase("COD")) {
            String bsuCod = String.format(Locale.getDefault(), "%,.0f", antaran.getBsuCod());
            holder.tvTipe.setText(antaran.getTipe() + " " + bsuCod);
        } else if (antaran.getTipe().equalsIgnoreCase("BLB")) {
            if (antaran.getBsuBlb() != 0 && antaran.getBsuTax() != 0) {
                double bsuBlbTax = antaran.getBsuBlb() + antaran.getBsuTax();
                String bsuBlb = String.format(Locale.getDefault(), "%,.0f", bsuBlbTax);
                holder.tvTipe.setText(antaran.getTipe() + "+PAJAK " + bsuBlb);
            } else if (antaran.getBsuBlb() != 0 && antaran.getBsuTax() == 0){
                String bsuBlb = String.format(Locale.getDefault(), "%,.0f", antaran.getBsuBlb());
                holder.tvTipe.setText(antaran.getTipe() + " " + bsuBlb);
            }
        }

        int size = list.size() - 1;
        if (position == size) {
            holder.ivDivider.setVisibility(View.GONE);
        } else {
            holder.ivDivider.setVisibility(View.VISIBLE);
        }

        flipIcon(holder, position);
    }

    public void flipIcon(Holder holder, int position) {
        String statusUpdate = list.get(position).getStatusUpdate();

        if (statusUpdate.equalsIgnoreCase("1")) {
            holder.unsyncContainer.setVisibility(View.GONE);

            resetIconYAxis(holder.syncContainer);
            holder.syncContainer.setVisibility(View.VISIBLE);
            holder.syncContainer.setAlpha(1);

            if (currentSelectedIndex == position) {
                FlipAnimator.flipView(context, holder.syncContainer, holder.unsyncContainer, true);
                resetCurrentIndex();
            }
        } else if (statusUpdate.equalsIgnoreCase("0")) {
            holder.syncContainer.setVisibility(View.GONE);

            resetIconYAxis(holder.unsyncContainer);
            holder.unsyncContainer.setVisibility(View.VISIBLE);
            holder.unsyncContainer.setAlpha(1);

            if (currentSelectedIndex == position) {
                FlipAnimator.flipView(context, holder.unsyncContainer, holder.syncContainer, true);
                resetCurrentIndex();
            }
        }
    }

    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    private void resetIconYAxis(View view) {
        if (view.getRotationY() != 0) {
            view.setRotationY(0);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void search(String idItem) {
        list.clear();

        if (idItem.length() == 0) {
            list.addAll(arrayList);
        } else {
            for (Antaran antaran : arrayList) {
                if (idItem.length() != 0 && antaran.getIdItem().contains(idItem)) {
                    list.add(antaran);
                }
            }
        }

        notifyDataSetChanged();
    }
}
