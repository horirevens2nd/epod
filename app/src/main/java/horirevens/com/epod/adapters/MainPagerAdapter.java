package horirevens.com.epod.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import horirevens.com.epod.fragments.MainFragmentTab1;
import horirevens.com.epod.fragments.MainFragmentTab2;
import horirevens.com.epod.fragments.MainFragmentTab3;

/**
 * Created by horirevens on 3/21/18.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {
    private int numOfTabs;

    public MainPagerAdapter(FragmentManager fragmentManager, int numOfTabs) {
        super(fragmentManager);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new MainFragmentTab1();
            case 1:
                return new MainFragmentTab2();
            case 2:
                return new MainFragmentTab3();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
