package horirevens.com.epod.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import java.util.ArrayList;
import java.util.List;

import horirevens.com.epod.R;
import horirevens.com.epod.models.Status;

/**
 * Created by horirevens on 3/22/18.
 */

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.Holder> {
    private Context context;
    private List<Status> list;
    private ArrayList<Status> arrayList = new ArrayList<>();
    private StatusListener listener;

    public StatusAdapter(Context context, List<Status> list, StatusListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        arrayList.addAll(list);
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextViewLatoRegular tvNumber, tvKeterangan, tvTindakan;
        private View ivDivider;

        public Holder(View itemView) {
            super(itemView);
            tvNumber = (TextViewLatoRegular) itemView.findViewById(R.id.tv_number);
            tvKeterangan = (TextViewLatoRegular) itemView.findViewById(R.id.tv_keterangan);
            tvTindakan = (TextViewLatoRegular) itemView.findViewById(R.id.tv_tindakan);
            ivDivider = (View) itemView.findViewById(R.id.iv_divider);
        }

        public void bind(final Status status, final StatusListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(status);
                }
            });
        }
    }

    @Override
    public StatusAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item_status, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(StatusAdapter.Holder holder, int position) {
        Status status = list.get(position);

        holder.bind(status, listener);
        holder.tvNumber.setText(position + 1 + ". ");
        holder.tvKeterangan.setText(status.getKeterangan());

        if ("000".equalsIgnoreCase(status.getIdStatusInduk())) {
            holder.tvTindakan.setVisibility(View.GONE);
        } else {
            holder.tvTindakan.setText(status.getTindakan());
        }

        int size = list.size() - 1;
        if (position == size) {
            holder.ivDivider.setVisibility(View.INVISIBLE);
        } else {
            holder.ivDivider.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public interface StatusListener {

        void onItemClicked(Status status);

    }
}
