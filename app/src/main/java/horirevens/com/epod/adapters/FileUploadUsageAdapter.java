package horirevens.com.epod.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import horirevens.com.epod.R;
import horirevens.com.epod.libraries.MyDate;
import horirevens.com.epod.models.FileUploadUsage;

public class FileUploadUsageAdapter extends RecyclerView.Adapter<FileUploadUsageAdapter.Holder> {
    private Context context;
    private List<FileUploadUsage> list;
    private ArrayList<FileUploadUsage> arrayList = new ArrayList<>();
    private DataUsageListener listener;

    public FileUploadUsageAdapter(Context context, List<FileUploadUsage> list, DataUsageListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        arrayList.addAll(list);
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextViewLatoRegular tvMonth, tvTotalFile, tvTotalImage,tvTotalSign;
        private TextViewLatoRegular tvTotalSize, tvImageSize, tvSignSize;
        private ImageView ivDivider;

        public Holder(View itemView) {
            super(itemView);

            tvMonth = (TextViewLatoRegular) itemView.findViewById(R.id.tv_month);
            tvTotalFile = (TextViewLatoRegular) itemView.findViewById(R.id.tv_total_file);
            tvTotalImage = (TextViewLatoRegular) itemView.findViewById(R.id.tv_total_image);
            tvTotalSign = (TextViewLatoRegular) itemView.findViewById(R.id.tv_total_sign);
            tvTotalSize = (TextViewLatoRegular) itemView.findViewById(R.id.tv_total_size);
            tvImageSize = (TextViewLatoRegular) itemView.findViewById(R.id.tv_image_size);
            tvSignSize = (TextViewLatoRegular) itemView.findViewById(R.id.tv_sign_size);
            ivDivider = (ImageView) itemView.findViewById(R.id.iv_divider);
        }
    }

    @Override
    public FileUploadUsageAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_data_usage, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(FileUploadUsageAdapter.Holder holder, int position) {
        FileUploadUsage fileUploadUsage = list.get(position);
        MyDate myDate = new MyDate();

        holder.tvMonth.setText(MyDate.getMonthNameShort(myDate.getMonthShort(fileUploadUsage.getTanggal())));
        holder.tvTotalFile.setText(String.valueOf(fileUploadUsage.getJmlBerkas()));
        holder.tvTotalSize.setText(convertToFileSize(fileUploadUsage.getJmlUkuran()));
        holder.tvTotalImage.setText(String.valueOf(fileUploadUsage.getJmlBerkasFoto()));
        holder.tvImageSize.setText(convertToFileSize(fileUploadUsage.getJmlUkuranFoto()));
        holder.tvTotalSign.setText(String.valueOf(fileUploadUsage.getJmlBerkasTtd()));
        holder.tvSignSize.setText(convertToFileSize(fileUploadUsage.getJmlUkuranTtd()));

        int size = list.size() - 1;
        if (position == size) {
            holder.ivDivider.setVisibility(View.GONE);
        } else {
            holder.ivDivider.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface DataUsageListener {
        void OnItemClicked();
    }

    private String convertToFileSize(double size) {
        if (size <= 0) {
            return "0";
        }

        final String[] units = new String[] { "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));

        return new DecimalFormat("#,##0.##").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }
}
