package horirevens.com.epod.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import horirevens.com.epod.R;
import horirevens.com.epod.libraries.MyDate;
import horirevens.com.epod.libraries.StringUtils;
import horirevens.com.epod.models.Antaran;

/**
 * Created by horirevens on 3/22/18.
 */

public class MainFragmentTab1Adapter  extends RecyclerView.Adapter<MainFragmentTab1Adapter.Holder> {
    private Context context;
    private List<Antaran> list;
    private ArrayList<Antaran> arrayList = new ArrayList<>();
    private static int currentSelectedIndex = -1;
    private MainFragmentTab1Listener listener;

    public interface MainFragmentTab1Listener {
        void onItemClick(Antaran antaran);
        void onItemLongClick(Antaran antaran);
        void onPhoneIconClick(String phoneNumber);
        void onWhatsappIconClick(String phoneNumber);
    }

    public MainFragmentTab1Adapter(Context context, List<Antaran> list, MainFragmentTab1Listener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        arrayList.addAll(list);
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextViewLatoRegular tvNumber, tvIdItem, tvWaktuEntri, tvIdDeliveryOrder, tvNamaPenerima,
                tvAlamatPenerima, tvIdProduk, tvTipe;
        private AppCompatImageView ivPhone, ivWhatsapp;
        private ConstraintLayout syncContainer;
        private ImageView ivDivider;

        public Holder(View itemView) {
            super(itemView);

            tvNumber = (TextViewLatoRegular) itemView.findViewById(R.id.tv_number);
            tvIdItem = (TextViewLatoRegular) itemView.findViewById(R.id.tv_id_item);
            tvWaktuEntri = (TextViewLatoRegular) itemView.findViewById(R.id.tv_waktu_entri);
            tvIdDeliveryOrder = (TextViewLatoRegular) itemView.findViewById(R.id.tv_id_delivery_order);
            ivPhone = (AppCompatImageView) itemView.findViewById(R.id.iv_phone);
            ivWhatsapp = (AppCompatImageView) itemView.findViewById(R.id.iv_whatsapp);
            tvNamaPenerima = (TextViewLatoRegular) itemView.findViewById(R.id.tv_nama_penerima);
            tvAlamatPenerima = (TextViewLatoRegular) itemView.findViewById(R.id.tv_alamat_penerima);
            tvTipe = (TextViewLatoRegular) itemView.findViewById(R.id.tv_tipe);
            tvIdProduk = (TextViewLatoRegular) itemView.findViewById(R.id.tv_id_produk);
            syncContainer = (ConstraintLayout) itemView.findViewById(R.id.sync_container);
            ivDivider = (ImageView) itemView.findViewById(R.id.iv_divider);
        }

        public void bind(final Antaran antaran, final MainFragmentTab1Listener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(antaran);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onItemLongClick(antaran);
                    return true;
                }
            });

            ivPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String hpPenerima = antaran.getHpPenerima();
                    if (hpPenerima.substring(0, 1).equalsIgnoreCase("6")) { //628140513878
                        hpPenerima = "+" + hpPenerima; // +6282140513878
                    } else if (hpPenerima.substring(0, 1).equalsIgnoreCase("0")) { // 082140513878
                        String countryCode = hpPenerima.substring(0, 1).replace("0", "+62"); // 0 > +62
                        String phoneNumber = hpPenerima.substring(1); // 082140513878 > 821405138787
                        hpPenerima = countryCode + phoneNumber; // +6282140513878
                    }

                    listener.onPhoneIconClick(hpPenerima);
                }
            });

            ivWhatsapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String hpPenerima = antaran.getHpPenerima();
                    if (hpPenerima.substring(0, 1).equalsIgnoreCase("6")) { //628140513878
                        hpPenerima = "+" + hpPenerima; // +6282140513878
                    } else if (hpPenerima.substring(0, 1).equalsIgnoreCase("0")) { // 082140513878
                        String countryCode = hpPenerima.substring(0, 1).replace("0", "+62"); // 0 > +62
                        String phoneNumber = hpPenerima.substring(1); // 082140513878 > 821405138787
                        hpPenerima = countryCode + phoneNumber; // +6282140513878
                    }

                    listener.onWhatsappIconClick(hpPenerima);
                }
            });
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_main_fragment1, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        final Antaran antaran = list.get(position);

        holder.bind(antaran, listener);
        holder.tvNumber.setText(position + 1 + ". ");
        holder.tvIdItem.setText(antaran.getIdItem());
        holder.tvWaktuEntri.setText(MyDate.setToAmPm(antaran.getWaktuEntri()));
        holder.tvIdDeliveryOrder.setText(antaran.getIdDeliveryOrder());
        holder.tvNamaPenerima.setText(antaran.getNamaPenerima().toUpperCase());
        holder.tvAlamatPenerima.setText(StringUtils.uppercaseSetences(antaran.getAlamatPenerima()));
        holder.tvIdProduk.setText(StringUtils.getProductName(antaran.getIdProduk()));

        if (antaran.getHpPenerima().equalsIgnoreCase("0") || antaran.getHpPenerima().equalsIgnoreCase("")
            || antaran.getHpPenerima().length() < 11) {
            holder.ivPhone.setVisibility(View.INVISIBLE);
            holder.ivWhatsapp.setVisibility(View.INVISIBLE);
        } else {
            holder.ivPhone.setVisibility(View.VISIBLE);
            holder.ivWhatsapp.setVisibility(View.VISIBLE);
        }

        if (antaran.getTipe().equalsIgnoreCase("NON COD")) {
            holder.tvTipe.setVisibility(View.INVISIBLE);
        } else if (antaran.getTipe().equalsIgnoreCase("COD")) {
            String bsuCod = String.format(Locale.getDefault(), "%,.0f", antaran.getBsuCod());
            holder.tvTipe.setText(antaran.getTipe() + " " + bsuCod);
        } else if (antaran.getTipe().equalsIgnoreCase("BLB")) {
            if (antaran.getBsuBlb() != 0 && antaran.getBsuTax() != 0) {
                double bsuBlbTax = antaran.getBsuBlb() + antaran.getBsuTax();
                String bsuBlb = String.format(Locale.getDefault(), "%,.0f", bsuBlbTax);
                holder.tvTipe.setText(antaran.getTipe() + "+PAJAK " + bsuBlb);
            } else if (antaran.getBsuBlb() != 0 && antaran.getBsuTax() == 0){
                String bsuBlb = String.format(Locale.getDefault(), "%,.0f", antaran.getBsuBlb());
                holder.tvTipe.setText(antaran.getTipe() + " " + bsuBlb);
            }
        }

        int size = list.size() - 1;
        if (position == size) {
            holder.ivDivider.setVisibility(View.INVISIBLE);
        } else {
            holder.ivDivider.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void search(String idItem) {
        list.clear();

        if (idItem.length() == 0) {
            list.addAll(arrayList);
        } else {
            for (Antaran antaran : arrayList) {
                if (idItem.length() != 0 && antaran.getIdItem().contains(idItem)) {
                    list.add(antaran);
                }
            }
        }

        notifyDataSetChanged();
    }
}
