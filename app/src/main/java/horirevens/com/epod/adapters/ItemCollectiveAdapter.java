package horirevens.com.epod.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.horirevens.mylibrary.fonts.TextViewLatoRegular;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import horirevens.com.epod.R;
import horirevens.com.epod.libraries.MyDate;
import horirevens.com.epod.libraries.StringUtils;
import horirevens.com.epod.models.Antaran;

public class ItemCollectiveAdapter extends RecyclerView.Adapter<ItemCollectiveAdapter.Holder> {
    private Context context;
    private List<Antaran> list;
    private ArrayList<Antaran> arrayList =  new ArrayList<>();
    private ItemCollectiveListener listener;
    //private SparseBooleanArray selectedItems;

    public ItemCollectiveAdapter(Context context, List<Antaran> list, ItemCollectiveListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        arrayList.addAll(list);
        //selectedItems = new SparseBooleanArray();
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextViewLatoRegular tvNumber, tvIdItem, tvWaktuEntri, tvIdDeliveryOrder, tvNamaPenerima,
                tvAlamatPenerima, tvIdProduk, tvTipe;
        private CheckBox checkBox;
        private ImageView ivDivider;

        public Holder(View itemView) {
            super(itemView);

            tvNumber = (TextViewLatoRegular) itemView.findViewById(R.id.tv_number);
            tvIdItem = (TextViewLatoRegular) itemView.findViewById(R.id.tv_id_item);
            tvWaktuEntri = (TextViewLatoRegular) itemView.findViewById(R.id.tv_waktu_entri);
            tvIdDeliveryOrder = (TextViewLatoRegular) itemView.findViewById(R.id.tv_id_delivery_order);
            tvNamaPenerima = (TextViewLatoRegular) itemView.findViewById(R.id.tv_nama_penerima);
            tvAlamatPenerima = (TextViewLatoRegular) itemView.findViewById(R.id.tv_alamat_penerima);
            tvTipe = (TextViewLatoRegular) itemView.findViewById(R.id.tv_tipe);
            tvIdProduk = (TextViewLatoRegular) itemView.findViewById(R.id.tv_id_produk);
            ivDivider = (ImageView) itemView.findViewById(R.id.iv_divider);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item_collective, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        Antaran antaran = list.get(position);

        holder.tvNumber.setText(position + 1 + ". ");
        holder.tvIdItem.setText(antaran.getIdItem());
        holder.tvWaktuEntri.setText(MyDate.setToAmPm(antaran.getWaktuEntri()));
        holder.tvIdDeliveryOrder.setText(antaran.getIdDeliveryOrder());
        holder.tvNamaPenerima.setText(antaran.getNamaPenerima().toUpperCase());
        holder.tvAlamatPenerima.setText(StringUtils.uppercaseSetences(antaran.getAlamatPenerima()));
        holder.tvIdProduk.setText(StringUtils.getProductName(antaran.getIdProduk()));

        holder.checkBox.setTag(antaran);
        holder.checkBox.setChecked(antaran.isChecked());
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                Antaran antaran = (Antaran) cb.getTag();
                antaran.setItemChecked(cb.isChecked());

                if (holder.checkBox.isChecked()) {
                    listener.onCheckboxChecked(position);
                } else {
                    listener.onCheckboxUnchecked(position);
                }
            }
        });

        if (antaran.getTipe().equalsIgnoreCase("NON COD")) {
            holder.tvTipe.setVisibility(View.INVISIBLE);
        } else {
            if (antaran.getTipe().equalsIgnoreCase("COD")) {
                String bsuCod = String.format(Locale.getDefault(), "%,.0f", antaran.getBsuCod());
                holder.tvTipe.setText(antaran.getTipe() + " " + bsuCod);
            } else if (antaran.getTipe().equalsIgnoreCase("BLB")) {
                if (antaran.getBsuBlb() != 0 && antaran.getBsuTax() != 0) {
                    double bsuBlbTax = antaran.getBsuBlb() + antaran.getBsuTax();
                    String bsuBlb = String.format(Locale.getDefault(), "%,.0f", bsuBlbTax);
                    holder.tvTipe.setText(antaran.getTipe() + "+PAJAK " + bsuBlb);
                } else if (antaran.getBsuBlb() != 0 && antaran.getBsuTax() == 0){
                    String bsuBlb = String.format(Locale.getDefault(), "%,.0f", antaran.getBsuBlb());
                    holder.tvTipe.setText(antaran.getTipe() + " " + bsuBlb);
                }
            }
        }

        int size = list.size() - 1;
        if (position == size) {
            holder.ivDivider.setVisibility(View.INVISIBLE);
        } else {
            holder.ivDivider.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void search(String idItem) {
        list.clear();

        if (idItem.length() == 0) {
            list.addAll(arrayList);
        } else {
            for (Antaran antaran : arrayList) {
                if (idItem.length() != 0 && antaran.getIdItem().contains(idItem)) {
                    list.add(antaran);
                }
            }
        }

        notifyDataSetChanged();
    }

    public interface ItemCollectiveListener {
        void onCheckboxChecked(int position);
        void onCheckboxUnchecked(int position);
    }
}
